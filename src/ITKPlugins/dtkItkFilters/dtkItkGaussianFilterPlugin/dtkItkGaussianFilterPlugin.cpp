#include "dtkItkGaussianFilter.h"
#include "dtkItkGaussianFilterPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkGaussianFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkGaussianFilterPlugin::initialize(void)
{
    dtkImaging::filters::gaussian::pluginFactory().record("itkGaussianFilter", dtkItkGaussianFilterCreator);
}

void dtkItkGaussianFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkGaussianFilter)

