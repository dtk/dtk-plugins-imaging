// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkImageWriter.h"
#include "dtkItkImageWriterPlugin.h"

#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// itkImageWriterPlugin
// ///////////////////////////////////////////////////////////////////

void itkImageWriterPlugin::initialize(void)
{
    dtkImaging::writer::pluginFactory().record("dtkItkImageWriter", dtkItkImageWriterCreator);
}

void itkImageWriterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta Writer
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(itkImageWriter)

//
// itkImageWriterPlugin.cpp ends here
