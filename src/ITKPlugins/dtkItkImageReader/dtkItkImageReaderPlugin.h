// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkImageReader.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkImageReaderPlugin : public dtkImageReaderPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkImageReaderPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkImageReaderPlugin" FILE "dtkItkImageReaderPlugin.json")

public:
     dtkItkImageReaderPlugin(void) {}
    ~dtkItkImageReaderPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkImageReaderPlugin.h ends here
