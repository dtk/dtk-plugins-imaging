#include "dtkItkMaskApplicationFilter.h"
#include "dtkItkImageConverter.h"

#include <dtkImage.h>
#include <dtkFilterExecutor.h>

#include <type_traits>

#include <itkImage.h>
#include <itkMaskImageFilter.h>
#include <itkMinimumMaximumImageCalculator.h>

// ///////////////////////////////////////////////////////////////////
//  dtkItkImageSubtractionPrivate
// ///////////////////////////////////////////////////////////////////

class dtkItkMaskApplicationFilterPrivate
{
public:
    dtkImage *m_image;
    dtkImage *m_mask;
    dtkImage *m_res;
    double m_backgroundValue;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkItkMaskApplicationFilter::dtkItkMaskApplicationFilter(void) : d(new dtkItkMaskApplicationFilterPrivate)
{
    d->m_image = NULL;
    d->m_mask = NULL;
    d->m_res = new dtkImage();
    d->m_backgroundValue = 0;
}

dtkItkMaskApplicationFilter::~dtkItkMaskApplicationFilter(void)
{
    delete d;
}

void dtkItkMaskApplicationFilter::setImage(dtkImage *image)
{
    d->m_image = image;
}

void dtkItkMaskApplicationFilter::setMask(dtkImage *image)
{
    d->m_mask = image;
}

void dtkItkMaskApplicationFilter::setBackgroundValue(double value)
{
    d->m_backgroundValue = value;
}

dtkImage *dtkItkMaskApplicationFilter::result(void) const
{
    return d->m_res;
}

void dtkItkMaskApplicationFilter::run(void)
{
    if (!d->m_image || !d->m_mask) {
        dtkWarn() << Q_FUNC_INFO << "input image is missing";
        return;
    }

    dtkFilterExecutor<dtkItkMaskApplicationFilter, INTEGER_ONLY>::run(this, d->m_image);
}

template < typename ImgT, int dim> inline void dtkItkMaskApplicationFilter::exec(void)
{
    typedef dtkItkImageConverter<ImgT,dim> Converter;
    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType,dim> ImageType;
    typedef itk::Image<unsigned char,dim> MaskType;


    typename ImageType::Pointer image = ImageType::New();
    typename MaskType::Pointer mask = MaskType::New();

    Converter::convertToNative(d->m_image, image);
    dtkItkImageConverter<dtkScalarPixel<unsigned char>,dim>::convertToNative(d->m_mask, mask);

    if (image && mask) {

        typedef itk::MaskImageFilter< ImageType,  MaskType, ImageType> MaskFilterType;

        typename MaskFilterType::Pointer maskFilter = MaskFilterType::New();
        mask->SetOrigin(image->GetOrigin());
        mask->SetSpacing(image->GetSpacing());
        maskFilter->SetInput(image);
        maskFilter->SetMaskingValue(d->m_backgroundValue);
        maskFilter->SetMaskImage(mask);

        //Outside values set to the lowest reachable value

        typedef itk::MinimumMaximumImageCalculator <ImageType> ImageCalculatorFilterType;

        typename ImageCalculatorFilterType::Pointer imageCalculatorFilter = ImageCalculatorFilterType::New ();
        imageCalculatorFilter->SetImage(image);

        try {
            imageCalculatorFilter->ComputeMinimum();
            maskFilter->SetOutsideValue(std::min(double(imageCalculatorFilter->GetMinimum()), 0.0));
            maskFilter->Update();
        } catch (itk::ExceptionObject & err) {
            std::cerr << "ExceptionObject caught in medMaskApplication!" << std::endl;
            std::cerr << err << std::endl;
        }

        Converter::convertFromNative(maskFilter->GetOutput(), d->m_res);
    }
}
