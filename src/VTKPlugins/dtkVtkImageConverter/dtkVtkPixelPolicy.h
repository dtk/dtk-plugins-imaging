#pragma once

#include <vtkType.h>
#include <cassert>

#include <vtkImageImport.h>

// /////////////////////////////////////////////////////////////////
// dtkVtkPixelTrait
// /////////////////////////////////////////////////////////////////

template <typename PixelType> class dtkVtkPixelTrait
{
public:
	static const int vtkType = -1;
};

template <> class dtkVtkPixelTrait<unsigned char>
{
public:
	static const int vtkType = VTK_UNSIGNED_CHAR;
};

template <> class dtkVtkPixelTrait<unsigned short>
{
public:
	static const int vtkType = VTK_UNSIGNED_SHORT;
};

template <> class dtkVtkPixelTrait<short>
{
public:
	static const int vtkType = VTK_SHORT;
};

template <> class dtkVtkPixelTrait<int>
{
public:
	static const int vtkType = VTK_INT;
};

template <> class dtkVtkPixelTrait<float>
{
public:
	static const int vtkType = VTK_FLOAT;
};

template <> class dtkVtkPixelTrait<double>
{
public:
	static const int vtkType = VTK_DOUBLE;
};

// /////////////////////////////////////////////////////////////////
// dtkVtkPixelTypeSetter
// /////////////////////////////////////////////////////////////////

template <typename PixelType> class dtkVtkPixelTypeSetter
{
public:
    static void setPixelType(vtkImageImport* imageImport)
    {
	assert(!"VTK doesn't support this type");
    };
};

template <> class dtkVtkPixelTypeSetter<unsigned char>
{
public:
    static void setPixelType(vtkImageImport* imageImport)
    {
        imageImport->SetDataScalarTypeToUnsignedChar();
    }
};

template <> class dtkVtkPixelTypeSetter<unsigned short>
{
public:
    static void setPixelType(vtkImageImport* imageImport)
    {
        imageImport->SetDataScalarTypeToUnsignedShort();
    }
};

template <> class dtkVtkPixelTypeSetter<short>
{
public:
    static void setPixelType(vtkImageImport* imageImport)
    {
        imageImport->SetDataScalarTypeToShort();
    }
};

template <> class dtkVtkPixelTypeSetter<int>
{
public:
    static void setPixelType(vtkImageImport* imageImport)
    {
        imageImport->SetDataScalarTypeToInt();
    }
};

template <> class dtkVtkPixelTypeSetter<float>
{
public:
    static void setPixelType(vtkImageImport* imageImport)
    {
        imageImport->SetDataScalarTypeToFloat();
    }
};

template <> class dtkVtkPixelTypeSetter<double>
{
public:
    static void setPixelType(vtkImageImport* imageImport)
    {
      imageImport->SetDataScalarTypeToDouble();
    }
};
