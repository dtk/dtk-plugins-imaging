// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkVtkImageReaderExport>

#include <dtkImageReader.h>

// /////////////////////////////////////////////////////////////////
// dtkVtkImageReader interface
// /////////////////////////////////////////////////////////////////

class DTKVTKIMAGEREADER_EXPORT dtkVtkImageReader final : public dtkImageReader
{
public:
     dtkVtkImageReader(void);
    ~dtkVtkImageReader(void);

public:
    dtkImage *read(const QString& path);

public:
    QStringList types(void);

private:
    class dtkVtkImageReaderPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkImageReader *dtkVtkImageReaderCreator(void)
{
    return new dtkVtkImageReader();
}

//
// dtkVtkImageReader.h ends here
