// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkConnectivityThresholdPlugin.h"

#include "dtkItkConnectivityThreshold.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkConnectivityThresholdPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkConnectivityThresholdPlugin::initialize(void)
{
    dtkImaging::filters::connectivityThreshold::pluginFactory().record("dtkItkConnectivityThresholdImageFilter", dtkItkConnectivityThresholdCreator);
}

void dtkItkConnectivityThresholdPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkConnectivityThreshold)

//
// dtkItkConnectivityThresholdPlugin.cpp ends here
