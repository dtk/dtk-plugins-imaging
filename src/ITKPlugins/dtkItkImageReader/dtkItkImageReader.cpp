// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <dtkImageReader.h>

#include "dtkItkImageReader.h"

#include <itkImage.h>
#include <itkImageIOBase.h>
#include <itkImageFileReader.h>
#include <itkRGBAPixel.h>

#include <QMutex>

// /////////////////////////////////////////////////////////////////
// dtkItkImageReaderPrivate
// /////////////////////////////////////////////////////////////////

class dtkItkImageReaderPrivate
{
public:
    static QMutex mutex;
};

QMutex dtkItkImageReaderPrivate::mutex;

// /////////////////////////////////////////////////////////////////

template <typename ImageType> void readFile(QString filename, typename ImageType::Pointer image)
{
    typedef itk::ImageFileReader<ImageType> ReaderType;
    typename ReaderType::Pointer reader = ReaderType::New();

    reader->SetFileName(filename.toStdString());
    reader->Update();
    image->Graft(reader->GetOutput());
}

// /////////////////////////////////////////////////////////////////
// dtkItkImageReader implementation
// /////////////////////////////////////////////////////////////////

dtkItkImageReader::dtkItkImageReader(void) : dtkImageReader(), d(new dtkItkImageReaderPrivate)
{

}

dtkItkImageReader::~dtkItkImageReader(void)
{
    delete d;
}

dtkImage *dtkItkImageReader::read(const QString &path)
{
    typedef itk::ImageIOBase::IOComponentType ScalarPixelType;

    // some external libs such as hdf5 are not thread safe, so locking is needed
    d->mutex.lock();
    itk::ImageIOBase::Pointer imageIO = itk::ImageIOFactory::CreateImageIO(qPrintable(path), itk::ImageIOFactory::ReadMode);
    d->mutex.unlock();
    if (!imageIO) {
        dtkError() << Q_FUNC_INFO << "Could not perform CreateImageIO for: " << path;
        return NULL;
    }
    imageIO->SetFileName(qPrintable(path));
    imageIO->ReadImageInformation();

    const ScalarPixelType pixelType = imageIO->GetComponentType();
    const size_t numDimensions =  imageIO->GetNumberOfDimensions();

    dtkImage *target = new dtkImage;

    switch (imageIO->GetComponentType()) {
    case itk::ImageIOBase::UCHAR:
        READ_TYPED_IMAGE_MACRO(unsigned char);
        break;

    case itk::ImageIOBase::CHAR:
        READ_TYPED_IMAGE_MACRO(char);
        break;

    case itk::ImageIOBase::USHORT:
        READ_TYPED_IMAGE_MACRO(unsigned short);
        break;

    case itk::ImageIOBase::SHORT:
        READ_TYPED_IMAGE_MACRO(short);
        break;

    case itk::ImageIOBase::UINT:
        READ_TYPED_IMAGE_MACRO(unsigned int);
       break;

    case itk::ImageIOBase::INT:
        READ_TYPED_IMAGE_MACRO(int);
        break;

    case itk::ImageIOBase::ULONG:
        READ_TYPED_IMAGE_MACRO(unsigned long);
        break;

    case itk::ImageIOBase::LONG:
        READ_TYPED_IMAGE_MACRO(long);
       break;

    case itk::ImageIOBase::FLOAT:
        READ_TYPED_IMAGE_MACRO(float);
       break;

    case itk::ImageIOBase::DOUBLE:
        READ_TYPED_IMAGE_MACRO(double);
        break;

    default:
        dtkError() << Q_FUNC_INFO << "Pixel Type (" << QString::fromStdString(imageIO->GetComponentTypeAsString(pixelType)) << ") not supported. Exiting.";
        return NULL;
    }

    return target;
}

QStringList dtkItkImageReader::types(void)
{
    return QStringList();
}

//
// dtkItkImageReader.cpp ends here
