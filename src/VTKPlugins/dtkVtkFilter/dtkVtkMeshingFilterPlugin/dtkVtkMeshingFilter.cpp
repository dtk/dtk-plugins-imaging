#include "dtkVtkMeshingFilter.h"

#include <dtkImage.h>
#include <dtkImageMesh.h>
#include <dtkFilterExecutor.h>

#include <type_traits>

#include <vtkImageData.h>
#include <dtkVtkImageMeshEngine.h>
#include <dtkVtkImageConverter.h>
#include <vtkContourFilter.h>
#include <vtkDecimatePro.h>
#include <vtkSmoothPolyDataFilter.h>
#include <vtkTriangleFilter.h>

// ///////////////////////////////////////////////////////////////////
//  dtkItkImageSubtractionPrivate
// ///////////////////////////////////////////////////////////////////

class dtkVtkMeshingFilterPrivate
{
public:
    dtkImage *mask;
    dtkImageMesh *mesh;
    dtkVtkImageMeshEngine *engine;

    double isoValue;
    bool decimate;
    double targetReduction;
    bool smooth;
    int iterations;
    double relaxationFactor;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkVtkMeshingFilter::dtkVtkMeshingFilter(void) : d(new dtkVtkMeshingFilterPrivate)
{
    d->mask = nullptr;
    d->mesh = new dtkImageMesh();
    d->engine = new dtkVtkImageMeshEngine();
    d->mesh->setEngine(d->engine);

    //for now we set these parameters
    d->isoValue = 1.0;
    d->decimate = true;
    d->targetReduction = 0.8;
    d->smooth = true;
    d->iterations = 30;
    d->relaxationFactor = 0.2;
}

void dtkVtkMeshingFilter::setDecimate(bool decimate)
{
    d->decimate = decimate;
}

void dtkVtkMeshingFilter::setIsoValue(double isoValue)
{
    d->isoValue = isoValue;
}

void dtkVtkMeshingFilter::setTargetReduction(double targetReduction)
{
    d->targetReduction = targetReduction;
}

void dtkVtkMeshingFilter::setSmooth(bool smooth)
{
    d->smooth = smooth;
}

void dtkVtkMeshingFilter::setIterations(int iterations)
{
    d->iterations = iterations;
}

void dtkVtkMeshingFilter::setRelaxationFactor(double relaxationFactor)
{
    d->relaxationFactor = relaxationFactor;
}

dtkVtkMeshingFilter::~dtkVtkMeshingFilter(void)
{
    delete d;
}

void dtkVtkMeshingFilter::setInput(dtkImage *image)
{
    d->mask = image;
}

dtkImageMesh *dtkVtkMeshingFilter::result(void) const
{
    return d->mesh;
}

void dtkVtkMeshingFilter::run(void)
{
    if (!d->mask) {
        dtkWarn() << Q_FUNC_INFO << "input image is missing";
        return;
    }

    dtkFilterExecutor<dtkVtkMeshingFilter>::run(this, d->mask);
}

template < typename ImgT, int dim> inline void dtkVtkMeshingFilter::exec(void)
{
    typedef dtkVtkImageConverterTemplate<ImgT, dim> Converter;
    vtkImageData *mask = vtkImageData::New();

    Converter::convertToNative(d->mask, mask);

    if (mask) {

        vtkContourFilter *contour = vtkContourFilter::New();
        contour->SetInputData(mask);
        contour->SetValue(0, d->isoValue);
        contour->Update();

        vtkTriangleFilter* contourTrian = vtkTriangleFilter::New();
        contourTrian->SetInputConnection(contour->GetOutputPort());
        contourTrian->PassVertsOn();
        contourTrian->PassLinesOn();
        contourTrian->Update();

        vtkPolyDataAlgorithm *lastAlgo = contourTrian;

        vtkDecimatePro *contourDecimated = nullptr;
        if (d->decimate) {
            // Decimate the mesh if required
            contourDecimated = vtkDecimatePro::New();
            contourDecimated->SetInputConnection(lastAlgo->GetOutputPort());
            contourDecimated->SetTargetReduction(d->targetReduction);
            contourDecimated->SplittingOff();
            contourDecimated->PreserveTopologyOn();
            contourDecimated->Update();
            lastAlgo = contourDecimated;
        }

        vtkSmoothPolyDataFilter *contourSmoothed = nullptr;
        if(d->smooth) {
            // Smooth the mesh if required
            contourSmoothed = vtkSmoothPolyDataFilter::New();
            contourSmoothed->SetInputConnection(lastAlgo->GetOutputPort());
            contourSmoothed->SetNumberOfIterations(d->iterations);
            contourSmoothed->SetRelaxationFactor(d->relaxationFactor);
            contourSmoothed->Update();
            lastAlgo = contourSmoothed;
        }

        vtkPolyData *polydata = vtkPolyData::New();
        polydata->DeepCopy(lastAlgo->GetOutput());

        contour->Delete();
        contourTrian->Delete();

        if (contourDecimated)
            contourDecimated->Delete();

        if (contourSmoothed)
            contourSmoothed->Delete();

        if (polydata) {
            d->engine->setPolyData(polydata);

        } else {
            dtkError() << "No polydata has been generated. Null pointer is set.";
            d->engine->setPolyData(nullptr);
        }
    }
}
