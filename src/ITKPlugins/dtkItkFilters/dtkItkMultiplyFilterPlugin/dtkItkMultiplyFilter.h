#pragma once

#include <QObject>
#include <dtkAbstractMultiplyFilter.h>

class dtkImage;
class dtkItkMultiplyFilterPrivate;

class dtkItkMultiplyFilter final : public dtkAbstractMultiplyFilter
{
public:
     dtkItkMultiplyFilter(void);
    ~dtkItkMultiplyFilter(void);

public:
    void setValue(double value);
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);
    template < typename ImgT, int dim > void exec(void);

private:
    dtkItkMultiplyFilterPrivate *d;
};


inline dtkAbstractMultiplyFilter *dtkItkMultiplyFilterCreator(void)
{
    return new dtkItkMultiplyFilter();
}
