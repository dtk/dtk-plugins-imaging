#include "dtkItkMultiplyFilter.h"
#include "dtkItkImageConverter.h"

#include <dtkFilterExecutor.h>

#include <itkImage.h>
#include <itkMultiplyImageFilter.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkMultiplyFilterPrivate
{
public:
    dtkImage *m_imageIn;
    dtkImage *m_imageOut;

    double m_value;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkItkMultiplyFilter::dtkItkMultiplyFilter(void): d(new dtkItkMultiplyFilterPrivate)
{
    d->m_imageIn=NULL;
    d->m_imageOut=new dtkImage();

    d->m_value=0;
}

dtkItkMultiplyFilter::~dtkItkMultiplyFilter(void)
{
    if(d)
        delete d;
}

void dtkItkMultiplyFilter::setValue(double value)
{
    d->m_value=value;
}

void dtkItkMultiplyFilter::setImage(dtkImage *image)
{
    d->m_imageIn=image;
}

dtkImage *dtkItkMultiplyFilter::filteredImage(void) const
{
    return d->m_imageOut;
}

void dtkItkMultiplyFilter::run(void)
{
    if (!d->m_imageIn) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkItkMultiplyFilter>::run(this, d->m_imageIn);
}

template < typename ImgT, int dim> inline void dtkItkMultiplyFilter::exec(void)
{
    qDebug()<< Q_FUNC_INFO;

    typedef dtkItkImageConverter<ImgT,dim> Converter;
    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType,dim> ImageType;
    typename ImageType::Pointer image=ImageType::New();
    Converter::convertToNative(d->m_imageIn,image);

    if(image)
    {
        typedef itk::MultiplyImageFilter <ImageType, ImageType, ImageType> MultiplyImageFilterType;
        typename MultiplyImageFilterType::Pointer filter = MultiplyImageFilterType::New();
        filter->SetInput1(image);
        filter->SetConstant2(d->m_value);
        filter->Update();
        Converter::convertFromNative(filter->GetOutput(),d->m_imageOut);
    }
}
