// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractErodeFilter.h>

class dtkImage;
class dtkVtErodeWithPseudoBallFilterPrivate;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtErodeWithPseudoBallFilter final : public dtkAbstractErodeFilter
{
public:
     dtkVtErodeWithPseudoBallFilter(void);
    ~dtkVtErodeWithPseudoBallFilter(void);

public:
    void setRadius(double radius);
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);

private:
    dtkVtErodeWithPseudoBallFilterPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkAbstractErodeFilter *dtkVtErodeWithPseudoBallFilterCreator(void)
{
    return new dtkVtErodeWithPseudoBallFilter();
}

//
// dtkVtErodeWithPseudoBallFilter.h ends here
