#include "dtkItkNormalizeFilter.h"
#include "dtkItkImageConverter.h"

#include <dtkFilterExecutor.h>

#include <itkImage.h>
#include <itkNormalizeImageFilter.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkNormalizeFilterPrivate
{
public:
    dtkImage *m_imageIn;
    dtkImage *m_imageOut;

};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkItkNormalizeFilter::dtkItkNormalizeFilter(void): d(new dtkItkNormalizeFilterPrivate)
{
    d->m_imageIn=NULL;
    d->m_imageOut=new dtkImage();
}

dtkItkNormalizeFilter::~dtkItkNormalizeFilter(void)
{
    if(d)
        delete d;
}

void dtkItkNormalizeFilter::setImage(dtkImage *image)
{
    d->m_imageIn=image;
}

dtkImage *dtkItkNormalizeFilter::filteredImage(void) const
{
    return d->m_imageOut;
}

void dtkItkNormalizeFilter::run(void)
{
    if (!d->m_imageIn) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkItkNormalizeFilter>::run(this, d->m_imageIn);
}

template < typename ImgT, int dim> inline void dtkItkNormalizeFilter::exec(void)
{
    qDebug()<< Q_FUNC_INFO;

    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType,dim> ImageType;
    typedef dtkItkImageConverter<ImgT,dim> Converter;
    typename ImageType::Pointer image=ImageType::New();
    Converter::convertToNative(d->m_imageIn,image);

    if(image)
    {
        typedef itk::NormalizeImageFilter <ImageType, ImageType> NormalizeImageFilterType;
        typename NormalizeImageFilterType::Pointer filter = NormalizeImageFilterType::New();
        filter->SetInput(image);
        filter->Update();
        Converter::convertFromNative(filter->GetOutput(),d->m_imageOut);
    }
}
