// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractImageMeshMapping.h>

class dtkImage;
class dtkImageMesh;

// /////////////////////////////////////////////////////////////////
// dtkVtkImageMeshMapping
// /////////////////////////////////////////////////////////////////

class dtkVtkImageMeshMapping final : public dtkAbstractImageMeshMapping
{
public:
     dtkVtkImageMeshMapping(void);
    ~dtkVtkImageMeshMapping(void);

public:
    void setImage(dtkImage *image);
    void setMesh(dtkImageMesh *mesh);

public:
    dtkImageMesh* resMesh(void) const;

public:
    void run(void);

private:
    class dtkVtkImageMeshMappingPrivate* d;
};

// /////////////////////////////////////////////////////////////////

inline dtkAbstractImageMeshMapping *dtkVtkImageMeshMappingCreator(void)
{
    return new dtkVtkImageMeshMapping();
}

//
// dtkVtkImageMeshMapping.h ends here
