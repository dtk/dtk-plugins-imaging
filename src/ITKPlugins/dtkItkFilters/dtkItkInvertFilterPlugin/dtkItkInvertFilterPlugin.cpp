#include "dtkItkInvertFilter.h"
#include "dtkItkInvertFilterPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkInvertFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkInvertFilterPlugin::initialize(void)
{
    dtkImaging::filters::invert::pluginFactory().record("itkInvertFilter", dtkItkInvertFilterCreator);
}

void dtkItkInvertFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkInvertFilter)

