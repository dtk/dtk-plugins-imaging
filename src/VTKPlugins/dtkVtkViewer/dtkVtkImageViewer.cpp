// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtkImageViewer.h"

#include <dtkVtkImageConverter.h>
#include <dtkVtkImageMeshEngine.h>

#include <dtkImagingCore/dtkFilterExecutor>
#include <dtkImagingCore/dtkImageMesh>
#include <dtkImagingCore/dtkImageMeshEngine>

#include <vtkActor.h>
#include <vtkAppendFilter.h>
#include <vtkAssembly.h>
#include <vtkCellArray.h>
#include <vtkPlane.h>
#include <vtkPlaneWidget.h>
#include <vtkCamera.h>
#include <vtkCellPicker.h>
#include <vtkCommand.h>
#include <vtkDataSetMapper.h>
#include <vtkDICOMImageReader.h>
#include <vtkExtractGeometry.h>
#include <vtkImageData.h>
#include <vtkImageViewer.h>
#include <vtkImageActor.h>
#include <vtkImagePlaneWidget.h>
#include <vtkImageViewer2.h>
#include <vtkImageMapToColors.h>
#include <vtkInteractorStyleImage.h>
#include <vtkLightKit.h>
#include <vtkMetaImageReader.h>
#include <vtkPlaneSource.h>
#include <vtkPlanes.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolyDataMapper.h>
#include <vtkPolyDataAlgorithm.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkResliceCursorActor.h>
#include <vtkResliceCursorWidget.h>
#include <vtkResliceCursorPolyDataAlgorithm.h>
#include <vtkResliceCursorLineRepresentation.h>
#include <vtkResliceImageViewer.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkShrinkFilter.h>
#include <vtkVersion.h>

#include <QVTKWidget.h>
#include <QVTKInteractor.h>

template <> class dtkExecutor<dtkVtkImageViewer>
{
public:
    template <typename ImgT, int dim> static typename std::enable_if<dim == 2>::type exec(dtkVtkImageViewer* d) {
        d->execDim2<ImgT, dim>();
    }

    template <typename ImgT, int dim> static typename std::enable_if<dim == 3>::type exec(dtkVtkImageViewer* d) {
        d->execDim3<ImgT, dim>();
    }

    template <typename ImgT, int dim> static typename std::enable_if<dim!=2 && dim!=3>::type exec(dtkVtkImageViewer* d) {
        qDebug() << "not implemented";
    }
};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class vtkResliceCursorCallback : public vtkCommand
{
public:
    static vtkResliceCursorCallback *New(void) {
        return new vtkResliceCursorCallback;
    }

    void Execute(vtkObject *caller, unsigned long event, void *) {

        if (event != vtkCommand::WindowLevelEvent)
            return;

        if(vtkInteractorStyleImage *style = dynamic_cast<vtkInteractorStyleImage *>(caller)) {

            if(style == this->RIV[0]->GetInteractorStyle()) {
                this->IPW[2]->SetWindowLevel(this->RIV[0]->GetColorWindow(),this->RIV[0]->GetColorLevel(),0);
                this->IPW[2]->GetInteractor()->GetRenderWindow()->Render();
            }

            if(style == this->RIV[1]->GetInteractorStyle()) {
                this->IPW[1]->SetWindowLevel(this->RIV[1]->GetColorWindow(),this->RIV[0]->GetColorLevel(),0);
                this->IPW[1]->GetInteractor()->GetRenderWindow()->Render();
            }

            if(style == this->RIV[2]->GetInteractorStyle()) {
                this->IPW[0]->SetWindowLevel(this->RIV[2]->GetColorWindow(),this->RIV[0]->GetColorLevel(),0);
                this->IPW[0]->GetInteractor()->GetRenderWindow()->Render();
            }
        }
    }

    vtkResliceCursorCallback(void) {}

public:
    vtkImagePlaneWidget* IPW[3];
    vtkResliceImageViewer *RIV[3];
};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtkImageViewer2 : public vtkResliceImageViewer
{
public:
    enum Orientation {
        SLICE_ORIENTATION_XY = 2,
        SLICE_ORIENTATION_XZ = 1,
        SLICE_ORIENTATION_YZ = 0
    };

public:
    Orientation orientation(void);

public:
    void setSliceOrientation(Orientation orientation);
};

dtkVtkImageViewer2::Orientation dtkVtkImageViewer2::orientation(void)
{
    switch(this->GetSliceOrientation()) {
    case 2:
        return SLICE_ORIENTATION_XY;
    case 1:
        return SLICE_ORIENTATION_XZ;
    case 0:
        return SLICE_ORIENTATION_YZ;
    default:
        return SLICE_ORIENTATION_YZ;
    }
}

void dtkVtkImageViewer2::setSliceOrientation(Orientation orientation)
{
    switch(orientation) {
    case SLICE_ORIENTATION_YZ:
        this->SetSliceOrientation(vtkImageViewer2::SLICE_ORIENTATION_YZ);
        break;
    case SLICE_ORIENTATION_XZ:
        this->SetSliceOrientation(vtkImageViewer2::SLICE_ORIENTATION_XZ);
        break;
    case SLICE_ORIENTATION_XY:
        this->SetSliceOrientation(vtkImageViewer2::SLICE_ORIENTATION_XY);
        break;
    default:
        break;
    };
}

// /////////////////////////////////////////////////////////////////
// dtkVtkImageView2
// /////////////////////////////////////////////////////////////////

class dtkVtkImageView2 : public QVTKWidget
{
    Q_OBJECT

public:
     dtkVtkImageView2(QWidget *parent = 0);
    ~dtkVtkImageView2(void);

public:
    dtkVtkImageViewer2::Orientation orientation(void);

public slots:
    void setInput(vtkImageData *image);

public slots:
    void setSlice(int slice);
    void setSliceOrientation(dtkVtkImageViewer2::Orientation orientation);

public:
    vtkSmartPointer<vtkRenderer> renderer;
    vtkSmartPointer<vtkRenderWindow> window;
    vtkSmartPointer<dtkVtkImageViewer2> viewer;
};

dtkVtkImageView2::dtkVtkImageView2(QWidget *parent) : QVTKWidget(parent)
{
    this->renderer = vtkSmartPointer<vtkRenderer>::New();

    this->window = vtkSmartPointer<vtkRenderWindow>::New();
    this->window->AddRenderer(renderer);

    vtkImageData *dummy = vtkImageData::New();
    dummy->SetDimensions(1, 1, 1);
    dummy->SetSpacing(1, 1, 1);
#if VTK_MAJOR_VERSION <= 5
    dummy->SetNumberOfScalarComponents(1);
    dummy->SetScalarTypeToUnsignedChar();
#else
    dummy->AllocateScalars(VTK_UNSIGNED_CHAR,1);
#endif

    this->viewer = new dtkVtkImageViewer2;
    this->viewer->SetRenderer(renderer);
    this->viewer->SetRenderWindow(window);
    this->viewer->SetupInteractor(this->GetInteractor());
    this->viewer->SetRenderWindow(this->GetRenderWindow());
    this->viewer->SetInputData(dummy);
    this->viewer->SetResliceModeToAxisAligned();
}

dtkVtkImageView2::~dtkVtkImageView2(void)
{

}

dtkVtkImageViewer2::Orientation dtkVtkImageView2::orientation(void)
{
    return this->viewer->orientation();
}

void dtkVtkImageView2::setInput(vtkImageData *image)
{
    this->viewer->SetInputData(image);

    if(this->viewer->orientation() == dtkVtkImageViewer2::SLICE_ORIENTATION_XY)
        this->setSlice(image->GetDimensions()[2]/2);

    if(this->viewer->orientation() == dtkVtkImageViewer2::SLICE_ORIENTATION_XZ)
        this->setSlice(image->GetDimensions()[1]/2);

    if(this->viewer->orientation() == dtkVtkImageViewer2::SLICE_ORIENTATION_YZ)
        this->setSlice(image->GetDimensions()[0]/2);

    this->renderer->ResetCamera();
    this->update();
}

void dtkVtkImageView2::setSlice(int slice)
{
    this->viewer->SetSlice(slice);
}

void dtkVtkImageView2::setSliceOrientation(dtkVtkImageViewer2::Orientation orientation)
{
    this->viewer->setSliceOrientation(orientation);
}

// /////////////////////////////////////////////////////////////////
// dtkVtkImageWidget2
// /////////////////////////////////////////////////////////////////

class dtkVtkImageWidget2 : public QFrame
{
    Q_OBJECT

public:
     dtkVtkImageWidget2(QWidget *parent = 0);
    ~dtkVtkImageWidget2(void);

signals:
    void sliceChanged(int);

public slots:
    void setInput(vtkImageData *image);

public slots:
    void setSlice(int slice);
    void setSliceOrientation(dtkVtkImageViewer2::Orientation orientation);

public:
    dtkVtkImageView2 *view;

private:
    QSlider *slider;
};

dtkVtkImageWidget2::dtkVtkImageWidget2(QWidget *parent) : QFrame(parent)
{
    this->view = new dtkVtkImageView2(this);

    this->slider = new QSlider(Qt::Vertical, this);
    this->slider->setEnabled(false);

    QHBoxLayout *layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    layout->addWidget(this->slider);
    layout->addWidget(this->view);

    connect(this->slider, SIGNAL(valueChanged(int)), this, SLOT(setSlice(int)));
}

dtkVtkImageWidget2::~dtkVtkImageWidget2(void)
{
    delete this->view;
}

void dtkVtkImageWidget2::setInput(vtkImageData *image)
{
    this->view->setInput(image);

    switch(this->view->orientation()) {
    case dtkVtkImageViewer2::SLICE_ORIENTATION_XY:
        this->slider->setRange(0, image->GetDimensions()[2]);
        this->slider->setValue(image->GetDimensions()[2]/2);
        break;
    case dtkVtkImageViewer2::SLICE_ORIENTATION_XZ:
        this->slider->setRange(0, image->GetDimensions()[1]);
        this->slider->setValue(image->GetDimensions()[1]/2);
        break;
    case dtkVtkImageViewer2::SLICE_ORIENTATION_YZ:
        this->slider->setRange(0, image->GetDimensions()[0]);
        this->slider->setValue(image->GetDimensions()[0]/2);
        break;
    default:
        break;
    };

    this->slider->setEnabled(true);
}

void dtkVtkImageWidget2::setSlice(int slice)
{
    this->view->setSlice(slice);

    emit sliceChanged(slice);
}

void dtkVtkImageWidget2::setSliceOrientation(dtkVtkImageViewer2::Orientation orientation)
{
    this->view->setSliceOrientation(orientation);
}

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtkImageWidget3PlaneCallback : public vtkCommand
{
public:
    static dtkVtkImageWidget3PlaneCallback *New(void) {
        return new dtkVtkImageWidget3PlaneCallback;
    }

    virtual void Execute(vtkObject *caller, unsigned long event, void *) {

        vtkPlaneWidget *planeWidget = reinterpret_cast<vtkPlaneWidget*>(caller);

        if(event == vtkCommand::InteractionEvent) {

            vtkPlane *plane = vtkPlane::New(); planeWidget->GetPlane(plane);

            foreach(vtkActor *actor, *(this->actors)) {
                actor->GetMapper()->RemoveAllClippingPlanes();
                actor->GetMapper()->AddClippingPlane(plane);
            }

            plane->Delete();
        }
    }

    dtkVtkImageWidget3PlaneCallback(void) {

    }

    QMap<void *, vtkActor *> *actors;
};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtkImageWidget3 : public QFrame
{
    Q_OBJECT

public:
     dtkVtkImageWidget3(QWidget *parent = 0);
    ~dtkVtkImageWidget3(void);

public slots:
    void addPolyData(vtkPolyData *pdata);
    void remPolyData(vtkPolyData *pdata);

public slots:
    void addGrid(vtkUnstructuredGrid *grid);
    void remGrid(vtkUnstructuredGrid *grid);

public slots:
    void setInput(vtkImageData *image);

public slots:
    void setPlanesShown(bool shown);
    void setClippingShown(bool shown);

public slots:
    void update(void);

public:
    vtkSmartPointer<vtkRenderer> renderer;
    vtkSmartPointer<vtkRenderWindow> window;
    vtkSmartPointer<vtkImagePlaneWidget> planeWidget[3];
    vtkSmartPointer<vtkPlaneWidget> clippingWidget;
    vtkSmartPointer<dtkVtkImageWidget3PlaneCallback> callback;

    vtkImageData *image;

    vtkAssembly *assembly;

    QMap<void *, vtkActor *> actors;

public:
    QVTKWidget *widget;
    static int count;
};

int dtkVtkImageWidget3::count = 0;

dtkVtkImageWidget3::dtkVtkImageWidget3(QWidget *parent) : QFrame(parent)
{
    this->image = NULL;

    this->assembly = vtkAssembly::New();

    this->renderer = vtkSmartPointer<vtkRenderer>::New();
    this->renderer->AddActor(this->assembly);
    this->renderer->SetBackground(0.0, 0.0, 0.0);

    this->window = vtkSmartPointer<vtkRenderWindow>::New();
    this->window->SetPointSmoothing(1);
    this->window->SetLineSmoothing(1);
    this->window->SetPolygonSmoothing(1);
    this->window->AddRenderer(renderer);

    this->widget = new QVTKWidget(this);
    this->widget->SetRenderWindow(this->window);

    this->callback = vtkSmartPointer<dtkVtkImageWidget3PlaneCallback>::New();
    this->callback->actors = &this->actors;

    this->clippingWidget = vtkSmartPointer<vtkPlaneWidget>::New();
    this->clippingWidget->SetInteractor(this->widget->GetInteractor());
    this->clippingWidget->AddObserver(vtkCommand::InteractionEvent, callback);
    this->clippingWidget->Off();

    vtkImageData *dummy = vtkImageData::New();
    dummy->SetDimensions(1, 1, 1);
    dummy->SetSpacing(1, 1, 1);

#if VTK_MAJOR_VERSION <= 5
    dummy->SetNumberOfScalarComponents(1);
    dummy->SetScalarTypeToUnsignedChar();
#else
    dummy->AllocateScalars(VTK_UNSIGNED_CHAR, 1);
#endif

    vtkRenderWindowInteractor *iren = this->widget->GetInteractor();

    for (int i = 0; i < 3; i++) {

        double color[3] = { 0, 0, 0 }; color[i] = 1;

        planeWidget[i] = vtkSmartPointer<vtkImagePlaneWidget>::New();
        planeWidget[i]->SetInteractor(iren);
        planeWidget[i]->SetInputData(dummy);
        planeWidget[i]->SetPlaneOrientation(i);
        planeWidget[i]->RestrictPlaneToVolumeOn();
        planeWidget[i]->GetPlaneProperty()->SetColor(color);
    }

    vtkSmartPointer<vtkLightKit> lightKit = vtkLightKit::New();
    lightKit->AddLightsToRenderer(this->renderer);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    layout->addWidget(this->widget);
}

dtkVtkImageWidget3::~dtkVtkImageWidget3(void)
{
    delete this->widget;
}

void dtkVtkImageWidget3::setInput(vtkImageData *image)
{
    this->image = image;

    int imageDims[3]; image->GetDimensions(imageDims);

    for(int i = 0; i < 3; i++) {
        planeWidget[i]->SetInputData(image);
        planeWidget[i]->SetSliceIndex(imageDims[i]/2);
        planeWidget[i]->On();
        planeWidget[i]->DisplayTextOn();
        planeWidget[i]->InteractionOn();
    }

    this->renderer->ResetCamera();
    this->update();
}

void dtkVtkImageWidget3::addPolyData(vtkPolyData *pdata)
{
    vtkPolyDataMapper *mapper = vtkPolyDataMapper::New();
    mapper->SetInputData(pdata);

    vtkActor *actor = vtkActor::New();
    actor->SetMapper(mapper);

    this->actors.insert(pdata, actor);
    this->assembly->AddPart(actor);

    this->update();
}

void dtkVtkImageWidget3::remPolyData(vtkPolyData *pdata)
{
    if(!this->actors.contains(pdata))
        return;

    vtkActor *actor = this->actors.value(pdata);

    this->assembly->RemovePart(actor);

    this->actors.remove(pdata);

    actor->Delete();

    this->update();
}

void dtkVtkImageWidget3::addGrid(vtkUnstructuredGrid *grid)
{
    count+=4;

    QColor color(QColor::colorNames().at(count % QColor::colorNames().count()));

    vtkSmartPointer<vtkDataSetMapper> mapper = vtkSmartPointer<vtkDataSetMapper>::New();
    mapper->SetInputData(grid);
    mapper->Update();

    vtkActor *actor = vtkActor::New();
    actor->SetMapper(mapper);
    actor->GetProperty()->SetColor(color.redF(), color.greenF(), color.blueF());

    this->actors.insert(grid, actor);

    this->assembly->AddPart(actor);

    this->update();
}

void dtkVtkImageWidget3::remGrid(vtkUnstructuredGrid *grid)
{
    if(!this->actors.contains(grid))
        return;

    vtkActor *actor = this->actors.value(grid);

    this->assembly->RemovePart(actor);

    this->actors.remove(grid);

    actor->Delete();

    this->update();
}

void dtkVtkImageWidget3::setPlanesShown(bool shown)
{
    for(int i = 0; i < 3; i++)
        if(shown)
            planeWidget[i]->On();
        else
            planeWidget[i]->Off();
}

void dtkVtkImageWidget3::setClippingShown(bool shown)
{
    if(shown)
        this->clippingWidget->On();
    else
        this->clippingWidget->Off();

    if(shown) {

        if(this->image) {
            double bounds[6];
            double spcing[3];

            bounds[0] = 0;
            bounds[1] = this->image->GetDimensions()[0];
            bounds[2] = 0;
            bounds[3] = this->image->GetDimensions()[1];
            bounds[4] = 0;
            bounds[5] = this->image->GetDimensions()[2];

            spcing[0] = this->image->GetSpacing()[0];
            spcing[1] = this->image->GetSpacing()[1];
            spcing[2] = this->image->GetSpacing()[2];

            this->clippingWidget->SetOrigin(0, 0, 0);
            this->clippingWidget->SetNormal(1, 0, 0);
            this->clippingWidget->SetPoint1(bounds[1]*spcing[0], 0, 0);
            this->clippingWidget->SetPoint2(0, bounds[3]*spcing[0], 0);
        }

        this->callback->Execute(this->clippingWidget, vtkCommand::InteractionEvent, NULL);

    } else {

        foreach(vtkActor *actor, this->actors)
            actor->GetMapper()->RemoveAllClippingPlanes();
    }

    this->update();
}

void dtkVtkImageWidget3::update(void)
{
    this->window->Render();

    this->widget->update();

    QFrame::update();
}

// /////////////////////////////////////////////////////////////////
// dtkVtkImageMainWindow
// /////////////////////////////////////////////////////////////////

class dtkVtkImageMainWindow : public QMainWindow
{
    Q_OBJECT

public:
     dtkVtkImageMainWindow(QWidget *parent = 0);
    ~dtkVtkImageMainWindow(void);

public slots:
    void addPolyData(vtkPolyData *pdata);
    void addGrid(vtkUnstructuredGrid *grid);
    void setInput(vtkImageData *data);

protected slots:
    void onSliceChanged(int);

public:
    dtkVtkImageWidget2 *view_a;
    dtkVtkImageWidget2 *view_b;
    dtkVtkImageWidget2 *view_c;
    dtkVtkImageWidget3 *view_d;

public:
    vtkSmartPointer<vtkImageData> data;
};

dtkVtkImageMainWindow::dtkVtkImageMainWindow(QWidget *parent) : QMainWindow(parent)
{
    // Setup visualization

    this->view_a = new dtkVtkImageWidget2(this);
    this->view_b = new dtkVtkImageWidget2(this);
    this->view_c = new dtkVtkImageWidget2(this);
    this->view_d = new dtkVtkImageWidget3(this);

    connect(this->view_a, SIGNAL(sliceChanged(int)), this, SLOT(onSliceChanged(int)));
    connect(this->view_b, SIGNAL(sliceChanged(int)), this, SLOT(onSliceChanged(int)));
    connect(this->view_c, SIGNAL(sliceChanged(int)), this, SLOT(onSliceChanged(int)));

    // --

    vtkResliceImageViewer *riw[3];
    riw[0] = static_cast<vtkResliceImageViewer *>(this->view_a->view->viewer);
    riw[1] = static_cast<vtkResliceImageViewer *>(this->view_b->view->viewer);
    riw[2] = static_cast<vtkResliceImageViewer *>(this->view_c->view->viewer);

    vtkImagePlaneWidget *ipw[3];
    ipw[0] = this->view_d->planeWidget[0];
    ipw[1] = this->view_d->planeWidget[1];
    ipw[2] = this->view_d->planeWidget[2];

    vtkSmartPointer<vtkResliceCursorCallback> cbk = vtkSmartPointer<vtkResliceCursorCallback>::New();

    for (int i = 0; i < 3; i++) {
        cbk->IPW[i] = ipw[i];
        cbk->RIV[i] = riw[i];

        riw[i]->GetInteractorStyle()->AddObserver(vtkCommand::WindowLevelEvent, cbk);
    }

    // Laying out

    QVBoxLayout *v_layout = new QVBoxLayout;
    v_layout->setContentsMargins(0, 0, 0, 0);
    v_layout->setSpacing(0);
    v_layout->addWidget(view_a);
    v_layout->addWidget(view_b);
    v_layout->addWidget(view_c);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setSpacing(0);
    layout->addLayout(v_layout, 1);
    layout->addWidget(view_d, 2);

    QWidget *central = new QWidget(this);
    central->setLayout(layout);

    this->setCentralWidget(central);

    connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(close()));
}

dtkVtkImageMainWindow::~dtkVtkImageMainWindow(void)
{

}

void dtkVtkImageMainWindow::onSliceChanged(int slice)
{
    if(this->sender() == this->view_a)
        this->view_d->planeWidget[2]->SetSliceIndex(slice);

    if(this->sender() == this->view_b)
        this->view_d->planeWidget[1]->SetSliceIndex(slice);

    if(this->sender() == this->view_c)
        this->view_d->planeWidget[0]->SetSliceIndex(slice);

    this->view_d->update();
}

void dtkVtkImageMainWindow::addPolyData(vtkPolyData *pdata)
{
    this->view_d->addPolyData(pdata);
    this->view_d->update();
}

void dtkVtkImageMainWindow::addGrid(vtkUnstructuredGrid *grid)
{
    this->view_d->addGrid(grid);
    this->view_d->update();
}

void dtkVtkImageMainWindow::setInput(vtkImageData *data)
{
    this->view_a->setSliceOrientation(dtkVtkImageViewer2::SLICE_ORIENTATION_XY);
    this->view_a->setInput(data);

    this->view_b->setSliceOrientation(dtkVtkImageViewer2::SLICE_ORIENTATION_XZ);
    this->view_b->setInput(data);

    this->view_c->setSliceOrientation(dtkVtkImageViewer2::SLICE_ORIENTATION_YZ);
    this->view_c->setInput(data);

    this->view_d->setInput(data);
}

// /////////////////////////////////////////////////////////////////
// dtkVtkImageViewerPrivate
// /////////////////////////////////////////////////////////////////

class dtkVtkImageViewerPrivate
{
public:
    dtkImage *image_in;
    dtkVtkImageMeshEngine *engine;

public:
    dtkVtkImageWidget2 *m_widget2;
    dtkVtkImageMainWindow *m_window;
};

// ///////////////////////////////////////////////////////////////////
// dtkVtkImageViewer
// ///////////////////////////////////////////////////////////////////

dtkVtkImageViewer::dtkVtkImageViewer(void) : d(new dtkVtkImageViewerPrivate)
{
    d->image_in = NULL;
    d->engine   = NULL;

    d->m_widget2 = NULL;
    d->m_window = NULL;
}

dtkVtkImageViewer::~dtkVtkImageViewer(void)
{
    if (d) {
        if (d->m_widget2) {
            delete d->m_widget2;
        }
        if (d->m_window) {
            delete d->m_window;
        }
        delete d;
    }
}

dtkImage *dtkVtkImageViewer::getInput(void) const
{
    return d->image_in;
}

void dtkVtkImageViewer::display(dtkImage *image)
{
    if (!image) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    d->image_in = image;

    dtkFilterExecutor<dtkVtkImageViewer>::run(this, d->image_in);
}

template <typename ImgT, int dim> inline void dtkVtkImageViewer::execDim2(void)
{
    typedef dtkVtkImageConverterTemplate<ImgT,dim> Converter;

    vtkSmartPointer<vtkImageData> image = vtkSmartPointer<vtkImageData>::New();

    Converter::convertToNative(d->image_in, image);

    if (image) {
        if (!d->m_widget2) {
            d->m_widget2 = new dtkVtkImageWidget2;
        }
        d->m_widget2->setAttribute(Qt::WA_DeleteOnClose);
        d->m_widget2->setInput(image);
        d->m_widget2->resize(800, 600);
        d->m_widget2->show();
    }
}

template <typename ImgT, int dim> inline void dtkVtkImageViewer::execDim3(void)
{
    typedef dtkVtkImageConverterTemplate<ImgT, dim> Converter;

    vtkSmartPointer<vtkImageData> image = vtkSmartPointer<vtkImageData>::New();

    Converter::convertToNative(d->image_in, image);

    if(image) {
        if (!d->m_window) {
            d->m_window = new dtkVtkImageMainWindow;
            d->m_window->setAttribute(Qt::WA_DeleteOnClose);
            d->m_window->resize(800, 600);
        }
        d->m_window->setInput(image);
        d->m_window->show();
    }
}

void dtkVtkImageViewer::display(dtkImageMesh *mesh)
{
    if (!mesh) {
        dtkWarn() << Q_FUNC_INFO << "no mesh input";
        return;
    }

    d->engine = dynamic_cast<dtkVtkImageMeshEngine *>(mesh->engine());
    if (d->engine) {
        if (!d->m_window) {
            d->m_window = new dtkVtkImageMainWindow;
            d->m_window->setAttribute(Qt::WA_DeleteOnClose);
            d->m_window->resize(800, 600);
        }
        d->m_window->addPolyData(d->engine->polyData());
        d->m_window->show();
    }
}

QWidget *dtkVtkImageViewer::widget(void)
{
    return d->m_window;
}

// /////////////////////////////////////////////////////////////////
// MOCing cpp-side classes
// /////////////////////////////////////////////////////////////////

#include "dtkVtkImageViewer.moc"

//
// dtkVtkImageViewer.cpp ends here
