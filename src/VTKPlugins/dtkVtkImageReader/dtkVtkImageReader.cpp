// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtkImageReader.h"
#include "dtkVtkImageConverter.h"

#include <dtkImage.h>

#include <dtkLog>

#include <dtkImageReader.h>

#include <vtkPointData.h>
#include <vtkImageData.h>
#include <vtkImageReader2Factory.h>
#include <vtkImageReader2.h>
#include <vtkSmartPointer.h>
#include <vtkXMLImageDataReader.h>


class dtkVtkImageReaderPrivate
{
public:
    vtkSmartPointer<vtkXMLImageDataReader> xml_reader;

public:
    vtkSmartPointer<vtkImageReader2Factory> factory;
    vtkImageReader2 *reader = nullptr;

    vtkSmartPointer<vtkImageData> vtk_image;
};

// /////////////////////////////////////////////////////////////////
// dtkVtkImageReader implementation
// /////////////////////////////////////////////////////////////////

dtkVtkImageReader::dtkVtkImageReader(void) : dtkImageReader(), d(new dtkVtkImageReaderPrivate)
{
    d->xml_reader = vtkSmartPointer<vtkXMLImageDataReader>::New();
    d->factory = vtkSmartPointer<vtkImageReader2Factory>::New();

}

dtkVtkImageReader::~dtkVtkImageReader(void)
{
    delete d;
}

dtkImage *dtkVtkImageReader::read(const QString& path)
{
    if (path.endsWith(".vti")) {
        d->vtk_image = vtkSmartPointer<vtkImageData>::New();
        d->xml_reader->SetFileName(qPrintable(path));
        d->xml_reader->Update();
        d->vtk_image = d->xml_reader->GetOutput();

    } else {
        d->reader = d->factory->CreateImageReader2(qPrintable(path));

        if (!d->reader) {
            dtkError() << Q_FUNC_INFO << "None vtk image reader is able to read file " << path << ". Aborting.";
            d->reader->Delete();
            d->reader = nullptr;
            return nullptr;
        }

        d->reader->SetFileName(qPrintable(path));
        d->reader->Update();
        d->vtk_image = d->reader->GetOutput();
    }

    dtkImage *image = new dtkImage;

    dtkVtkImageConverter::convertFromNative(d->vtk_image, image);

    //vtk_image->SetReferenceCount(2); // Prevents the reader to delete the underlying pointer of scalars
    if (d->reader) {
        d->reader->Delete();
        d->reader = nullptr;
    }

    return image;
}

QStringList dtkVtkImageReader::types(void)
{
    return QStringList();
}

//
// dtkVtkImageReader.cpp ends here
