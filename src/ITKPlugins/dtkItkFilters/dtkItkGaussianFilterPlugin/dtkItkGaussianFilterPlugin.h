#pragma once

#include <dtkCore>
#include <dtkAbstractGaussianFilter.h>

class dtkItkGaussianFilterPlugin: public dtkAbstractGaussianFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractGaussianFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkGaussianFilterPlugin" FILE "dtkItkGaussianFilterPlugin.json")

public:
     dtkItkGaussianFilterPlugin(void) {}
    ~dtkItkGaussianFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
