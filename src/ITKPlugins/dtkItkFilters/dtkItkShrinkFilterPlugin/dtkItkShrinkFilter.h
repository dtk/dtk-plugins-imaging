#pragma once

#include <QObject>
#include <dtkAbstractShrinkFilter.h>

class dtkImage;
class dtkItkShrinkFilterPrivate;

class dtkItkShrinkFilter final : public dtkAbstractShrinkFilter
{
    

public:
     dtkItkShrinkFilter(void);
    ~dtkItkShrinkFilter(void);

public:
    void setXShrink(double xShrink);
    void setYShrink(double yShrink);
    void setZShrink(double zShrink);

    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);
    template < typename ImgT, int dim > void exec(void);

private:
    dtkItkShrinkFilterPrivate *d;
};


inline dtkAbstractShrinkFilter *dtkItkShrinkFilterCreator(void)
{
    return new dtkItkShrinkFilter();
}
