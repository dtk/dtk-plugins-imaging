// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractErodeFilter.h>

class dtkImage;
class dtkItkErodeFilterPrivate;

// /////////////////////////////////////////////////////////////////
// dtkItkErodeFilter
// /////////////////////////////////////////////////////////////////

class dtkItkErodeFilter final : public dtkAbstractErodeFilter
{
public:
     dtkItkErodeFilter(void);
    ~dtkItkErodeFilter(void);

public:
    void setRadius(double radius);
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);
    template < typename ImgT, int dim > void exec(void);

private:
    dtkItkErodeFilterPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkAbstractErodeFilter *dtkItkErodeFilterCreator(void)
{
    return new dtkItkErodeFilter();
}

//
// dtkItkErodeFilter.h ends here
