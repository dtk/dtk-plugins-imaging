#include "dtkVtGaussianFilter.h"
#include "dtkVtImageConverter.h"

#include <dtkFilterExecutor.h>

#include <vt_image.h>

#include <api-linearFilter.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtGaussianFilterPrivate
{
public:
    dtkImage *image_in;
    dtkImage *image_out;

    double sigma;
    double *sigma_values;

public:
    template < typename ImgT, int dim > void exec(void);
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkVtGaussianFilter::dtkVtGaussianFilter(void): dtkAbstractGaussianFilter(), d(new dtkVtGaussianFilterPrivate)
{
    d->image_in = nullptr;
    d->image_out = new dtkImage();

    d->sigma = 0;
    d->sigma_values = nullptr;
}

dtkVtGaussianFilter::~dtkVtGaussianFilter(void)
{
    delete d;
}

void dtkVtGaussianFilter::setSigma(double sigma)
{
    d->sigma = sigma;
}

void dtkVtGaussianFilter::setSigmaValues(double *sigma_values)
{
    d->sigma_values = sigma_values;
}

void dtkVtGaussianFilter::setImage(dtkImage *image)
{
    d->image_in = image;
}

dtkImage *dtkVtGaussianFilter::filteredImage(void) const
{
    return d->image_out;
}

void dtkVtGaussianFilter::run(void)
{
    if (!d->image_in) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkVtGaussianFilterPrivate>::run(d, d->image_in);
}

template < typename ImgT, int dim> inline void dtkVtGaussianFilterPrivate::exec(void)
{
    vt_image image;
    char str[100];
    if (!sigma_values) {
        if ( dim == 2 )
            sprintf( str, "-x 0 -y 0 -sigma %f", sigma/image_in->spacing()[0] );
        else
            sprintf( str, "-x 0 -y 0 -z 0 -sigma %f", sigma/image_in->spacing()[0] );
    } else {
        if ( dim == 2 )
            sprintf( str, "-x 0 -y 0 -sigma %f %f", sigma_values[0]/image_in->spacing()[0], sigma_values[1]/image_in->spacing()[1] );
        else
            sprintf( str, "-x 0 -y 0 -z 0 -sigma %f %f %f", sigma_values[0]/image_in->spacing()[0], sigma_values[1]/image_in->spacing()[1], sigma_values[2]/image_in->spacing()[2] );
    }

    if (dtkVtImageConverter::convertToNative(this->image_in, &image) != EXIT_FAILURE) {
        if ( API_linearFilter( &image, &image, str, (char*)NULL ) != 1 ) {
            free( image.array );
            dtkError() << Q_FUNC_INFO << "Computation failed. Aborting.";
            return;
        }

        dtkVtImageConverter::convertFromNative(&image, this->image_out);

        free( image.array );
        image.array = NULL;

    } else {
        dtkError() << Q_FUNC_INFO << "Conversion to VT format failed. Aborting.";
    }
}
