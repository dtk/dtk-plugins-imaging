// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractConnectedComponentsFilter.h>

class dtkImage;
class dtkItkConnectedComponentsFilterPrivate;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkConnectedComponentsFilter final : public dtkAbstractConnectedComponentsFilter
{
public:
     dtkItkConnectedComponentsFilter(void);
    ~dtkItkConnectedComponentsFilter(void);

public:
    void setValue(double value);
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);

private:
    dtkItkConnectedComponentsFilterPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkAbstractConnectedComponentsFilter *dtkItkConnectedComponentsFilterCreator(void)
{
    return new dtkItkConnectedComponentsFilter();
}
//
// dtkItkConnectedComponentsFilter.h ends here
