#pragma once

#include <dtkCore>
#include <dtkAbstractDivideFilter.h>

class dtkItkDivideFilterPlugin: public dtkAbstractDivideFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractDivideFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkDivideFilterPlugin" FILE "dtkItkDivideFilterPlugin.json")

public:
     dtkItkDivideFilterPlugin(void) {}
    ~dtkItkDivideFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
