// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImageMeshEngine.h>

class vtkPolyData;

dtkVtkImageMeshEnginePrivate;

// /////////////////////////////////////////////////////////////////
// dtkVtkImageMeshEngine interface
// /////////////////////////////////////////////////////////////////

class dtkVtkImageMeshEngine : public dtkImageMeshEngine
{
public:
     dtkVtkImageMeshEngine(void);
     dtkVtkImageMeshEngine(const dtkVtkImageMeshEngine& o);
    ~dtkVtkImageMeshEngine(void);

public:
    dtkVtkImageMeshEngine *clone(void) const final;

public:
    void setPolyData(vtkPolyData *poly_data);

public:
    vtkPolyData *polyData(void) const;

private:
    struct dtkVtkImageMeshEnginePrivate *d;
};

// /////////////////////////////////////////////////////////////////
// Creator function
// /////////////////////////////////////////////////////////////////

inline dtkImageMeshEngine *dtkVtkImageMeshEngineCreator(void)
{
    return new dtkVtkImageMeshEngine();
}

//
// dtkVtkImageMeshEngine.h ends here
