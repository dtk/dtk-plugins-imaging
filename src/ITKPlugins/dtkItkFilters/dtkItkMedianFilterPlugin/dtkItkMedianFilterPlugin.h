#pragma once

#include <dtkCore>
#include <dtkAbstractMedianFilter.h>

class dtkItkMedianFilterPlugin: public dtkAbstractMedianFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractMedianFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkMedianFilterPlugin" FILE "dtkItkMedianFilterPlugin.json")

public:
     dtkItkMedianFilterPlugin(void) {}
    ~dtkItkMedianFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
