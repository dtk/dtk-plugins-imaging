#include "dtkItkImageSubtraction.h"
#include "dtkItkImageConverter.h"

#include <dtkFilterExecutor.h>

#include <itkImage.h>
#include <itkSubtractImageFilter.h>

// ///////////////////////////////////////////////////////////////////
//  dtkItkImageSubtractionPrivate
// ///////////////////////////////////////////////////////////////////

class dtkItkImageSubtractionPrivate
{
    public:
        dtkImage *m_imageIn1;
        dtkImage *m_imageIn2;
        dtkImage *m_imageOut;
};

// ///////////////////////////////////////////////////////////////////
//  dtkItkImageSubtraction
// ///////////////////////////////////////////////////////////////////

dtkItkImageSubtraction::dtkItkImageSubtraction(void): d(new dtkItkImageSubtractionPrivate)
{
    d->m_imageIn1=NULL;
    d->m_imageIn2=NULL;
    d->m_imageOut=new dtkImage();
}

dtkItkImageSubtraction::~dtkItkImageSubtraction(void)
{
    if(d)
        delete d;
}

void dtkItkImageSubtraction::setLhs(dtkImage *image)
{
    d->m_imageIn1=image;
}

void dtkItkImageSubtraction::setRhs(dtkImage *image)
{
    d->m_imageIn2=image;
}

dtkImage *dtkItkImageSubtraction::result(void) const
{
    return d->m_imageOut;
}

void dtkItkImageSubtraction::run(void)
{
    if (!d->m_imageIn1) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkItkImageSubtraction>::run(this,d->m_imageIn1);
}

template < typename ImgT, int dim> inline void dtkItkImageSubtraction::exec(void)
{
    qDebug()<< Q_FUNC_INFO;

    typedef dtkItkImageConverter<ImgT,dim> Converter;
    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType,dim> ImageType;
    typename ImageType::Pointer image1=ImageType::New();
    typename ImageType::Pointer image2=ImageType::New();
    
    Converter::convertToNative(d->m_imageIn1, image1);
    Converter::convertToNative(d->m_imageIn2, image2);

    if(image1 && image2)
    {
    
        typedef itk::SubtractImageFilter<ImageType> SubtractType;
        typename SubtractType::Pointer diff = SubtractType::New();
        diff->SetInput1(image1);
        diff->SetInput2(image2);
        diff->Update();
        Converter::convertFromNative(diff->GetOutput(),d->m_imageOut);
    }
}
