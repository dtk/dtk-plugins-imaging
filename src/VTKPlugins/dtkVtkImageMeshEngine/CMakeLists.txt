## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

project(dtkVtkImageMeshEnginePlugin)

## ###################################################################
## Build rules
## ###################################################################

add_definitions(-DQT_PLUGIN)

add_library(${PROJECT_NAME} SHARED
  dtkVtkImageMeshEngine.h
  dtkVtkImageMeshEngine.cpp
  dtkVtkImageMeshEnginePlugin.h
  dtkVtkImageMeshEnginePlugin.cpp
  dtkVtkMeshReader.h
  dtkVtkMeshReader.cpp
  dtkVtkMeshWriter.h
  dtkVtkMeshWriter.cpp)

## ###################################################################
## Link rules
## ###################################################################

target_link_libraries(${PROJECT_NAME} PUBLIC Qt5::Core)

target_link_libraries(${PROJECT_NAME} PUBLIC dtkCore)
target_link_libraries(${PROJECT_NAME} PRIVATE dtkLog)
target_link_libraries(${PROJECT_NAME} PUBLIC dtkImagingCore)

target_link_libraries(${PROJECT_NAME} PRIVATE ${VTK_LIBRARIES})

## #################################################################
## Install rules
## #################################################################

install(TARGETS ${PROJECT_NAME}
  RUNTIME DESTINATION plugins/${DTK_CURRENT_LAYER}
  LIBRARY DESTINATION plugins/${DTK_CURRENT_LAYER}
  ARCHIVE DESTINATION plugins/${DTK_CURRENT_LAYER})

######################################################################
### CMakeLists.txt ends here
