## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

project(dtkVtHTransformFilterPlugin)

## ###################################################################
## Build rules
## ###################################################################

add_definitions(-DQT_PLUGIN)

add_library(${PROJECT_NAME} SHARED
  dtkVtHTransformFilter.h
  dtkVtHTransformFilter.cpp
  dtkVtHTransformFilterPlugin.h
  dtkVtHTransformFilterPlugin.cpp)

## ###################################################################
## Link rules
## ###################################################################

target_link_libraries(${PROJECT_NAME} Qt5::Core)

target_link_libraries(${PROJECT_NAME} dtkCore)
target_link_libraries(${PROJECT_NAME} dtkLog)

target_link_libraries(${PROJECT_NAME} dtkImagingCore)
target_link_libraries(${PROJECT_NAME} dtkImagingFilters)

target_link_libraries(${PROJECT_NAME} dtkVtImageConverter)

target_link_libraries(${PROJECT_NAME} vt)
target_link_libraries(${PROJECT_NAME} vp)
target_link_libraries(${PROJECT_NAME} exec)
target_link_libraries(${PROJECT_NAME} io)
target_link_libraries(${PROJECT_NAME} basic)

## #################################################################
## Install rules
## #################################################################

install(TARGETS ${PROJECT_NAME}
  RUNTIME DESTINATION plugins/${DTK_CURRENT_LAYER}
  LIBRARY DESTINATION plugins/${DTK_CURRENT_LAYER}
  ARCHIVE DESTINATION plugins/${DTK_CURRENT_LAYER})

######################################################################
### CMakeLists.txt ends here
