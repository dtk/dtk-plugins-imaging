#pragma once

#include <dtkCore>
#include <dtkAbstractSubtractFilter.h>

class dtkItkSubtractFilterPlugin: public dtkAbstractSubtractFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractSubtractFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkSubtractFilterPlugin" FILE "dtkItkSubtractFilterPlugin.json")

public:
     dtkItkSubtractFilterPlugin(void) {}
    ~dtkItkSubtractFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
