// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkVtkImageConverterExport>

#include <dtkImagingCore/dtkImageConverter>

class dtkImage;
class dtkImageData;
class vtkImageData;

// ///////////////////////////////////////////////////////////////////
// dtkVtkImageConverter
// ///////////////////////////////////////////////////////////////////

class DTKVTKIMAGECONVERTER_EXPORT dtkVtkImageConverter final : public dtkImageConverter
{
public:
     dtkVtkImageConverter(void);
    ~dtkVtkImageConverter(void);

public:
    void setInput(dtkImage *) override;

public:
    void *output(void) override;

public:
    int convert(void) override;
    static void convertFromNative(vtkImageData *source, dtkImage *target);
    static void convertToNative(dtkImage *source, vtkImageData *target);

private:
    class dtkVtkImageConverterPrivate *d;
};

inline dtkImageConverter *dtkVtkImageConverterCreator(void)
{
    return new dtkVtkImageConverter;
}

//
// dtkVtkImageConverter.h ends here
