// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkImageWriter.h"

#include "dtkItkImageConverter.h"

#include <dtkImage.h>
#include <dtkFilterExecutor.h>

#include <itkImage.h>
#include <itkImageFileWriter.h>

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkItkImageWriterPrivate
// /////////////////////////////////////////////////////////////////

class dtkItkImageWriterPrivate
{
public:
    QString path;
    dtkImage *image;

public:
    template < typename ImgT, int dim > void exec(void);
};

// /////////////////////////////////////////////////////////////////
// dtkItkImageWriter
// /////////////////////////////////////////////////////////////////

dtkItkImageWriter::dtkItkImageWriter(void) : dtkImageWriter(), d(new dtkItkImageWriterPrivate)
{
    d->image = nullptr;
}

dtkItkImageWriter::~dtkItkImageWriter(void)
{
    d->image = nullptr;
    delete d;
}
void dtkItkImageWriter::setPath(const QString& path)
{
    d->path = path;
}

void dtkItkImageWriter::setImage(dtkImage *image)
{
    if (!image) {
        dtkWarn() << Q_FUNC_INFO << "Input image is null. Nothing is done";
        return;
    }
    d->image = image;
}

void dtkItkImageWriter::write(void)
{
    if (!d->image) {
        dtkWarn() << Q_FUNC_INFO << "No image to write. Nothing is done.";
        return;
    }

    if (d->path.isEmpty()) {
        dtkWarn() << Q_FUNC_INFO << "No path has been set. Nothing is done.";
        return;
    }

    dtkFilterExecutor<dtkItkImageWriterPrivate>::run(d, d->image);
}

template < typename ImgT, int dim> inline void dtkItkImageWriterPrivate::exec(void)
{
    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType, dim> ImageType;
    typedef dtkItkImageConverter< ImgT, dim> Converter;

    typename ImageType::Pointer image = ImageType::New();

    Converter::convertToNative(this->image, image);

    if (image) {
        typedef  itk::ImageFileWriter<ImageType> WriterType;

        typename WriterType::Pointer writer = WriterType::New();
        writer->SetFileName(qPrintable(this->path));
        writer->SetInput(image);
        writer->Update();
    }
}

//
// executive_visitor.cpp ends here
