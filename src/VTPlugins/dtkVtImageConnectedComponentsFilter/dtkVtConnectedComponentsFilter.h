// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractConnectedComponentsFilter.h>

class dtkImage;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtConnectedComponentsFilter final : public dtkAbstractConnectedComponentsFilter
{
public:
     dtkVtConnectedComponentsFilter(void);
    ~dtkVtConnectedComponentsFilter(void);

public:
    void setImage(dtkImage *image);
    void setValue(double value);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);

private:
    class dtkVtConnectedComponentsFilterPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkAbstractConnectedComponentsFilter *dtkVtConnectedComponentsFilterCreator(void)
{
    return new dtkVtConnectedComponentsFilter();
}

//
// dtkVtConnectedComponentsFilter.h ends here
