#pragma once

#include <dtkCore>
#include <dtkAbstractInvertFilter.h>

class dtkItkInvertFilterPlugin: public dtkAbstractInvertFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractInvertFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkInvertFilterPlugin" FILE "dtkItkInvertFilterPlugin.json")

public:
     dtkItkInvertFilterPlugin(void) {}
    ~dtkItkInvertFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
