// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>

class itkImageTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testItkConverter(void);
    void testItkConverter2(void);
    
private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);
};


// 
// csrSparseMatrixHandlerTest.h ends here
