// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImageMeshEngine.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtkImageMeshEnginePlugin : public dtkImageMeshEnginePlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkImageMeshEnginePlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtkImageMeshEnginePlugin" FILE "dtkVtkImageMeshEnginePlugin.json")

public:
     dtkVtkImageMeshEnginePlugin(void) {}
    ~dtkVtkImageMeshEnginePlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkImageMeshEnginePlugin.h ends here
