// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkAddFilter.h"

#include "dtkItkImageConverter.h"

#include <dtkImage.h>
#include <dtkFilterExecutor.h>

#include <dtkLog>

#include <itkImage.h>
#include <itkAddImageFilter.h>

// ///////////////////////////////////////////////////////////////////
//  dtkItkAddFilterPrivate
// ///////////////////////////////////////////////////////////////////

class dtkItkAddFilterPrivate
{
public:
    dtkImage *image_in;
    dtkImage *image_out;

    double value;

public:
    template < typename ImgT, int dim > void exec(void);
};

// ///////////////////////////////////////////////////////////////////
//  dtkItkAddFilter
// ///////////////////////////////////////////////////////////////////

dtkItkAddFilter::dtkItkAddFilter(void): dtkAbstractAddFilter(), d(new dtkItkAddFilterPrivate)
{
    d->image_in = nullptr;
    d->image_out = new dtkImage();

    d->value = 0;
}

dtkItkAddFilter::~dtkItkAddFilter(void)
{
    delete d;
}

void dtkItkAddFilter::setValue(double value)
{
    d->value = value;
}

void dtkItkAddFilter::setImage(dtkImage *image)
{
    d->image_in = image;
}

dtkImage *dtkItkAddFilter::filteredImage(void) const
{
    return d->image_out;
}

void dtkItkAddFilter::run(void)
{
    if (!d->image_in) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkItkAddFilterPrivate>::run(d, d->image_in);
}

template < typename ImgT, int dim> inline void dtkItkAddFilterPrivate::exec(void)
{
    typedef dtkItkImageConverter<ImgT, dim> Converter;
    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType, dim> ImageType;

    typename ImageType::Pointer image = ImageType::New();

    Converter::convertToNative(this->image_in, image);

    if (image) {
        typedef itk::AddImageFilter <ImageType, ImageType, ImageType> AddImageFilterType;

        typename AddImageFilterType::Pointer filter = AddImageFilterType::New();
        filter->SetInput1(image);
        filter->SetConstant2(this->value);
        filter->Update();

        Converter::convertFromNative(filter->GetOutput(), this->image_out);
    }
}

//
// dtkItkAddFilter.cpp ends here
