#pragma once

#include <dtkCore>
#include <dtkAbstractGaussianFilter.h>

class dtkVtGaussianFilterPlugin: public dtkAbstractGaussianFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractGaussianFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtGaussianFilterPlugin" FILE "dtkVtGaussianFilterPlugin.json")

public:
     dtkVtGaussianFilterPlugin(void) {}
    ~dtkVtGaussianFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
