// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtkMeshingFilterWidget.h"
#include "dtkVtkMeshingFilter.h"
#include "ui_dtkVtkMeshingFilterWidget.h"

#include <QtGlobal>
#include <dtkLog>

// ///////////////////////////////////////////////////////////////////

class dtkVtkMeshingFilterWidgetPrivate
{
public:
    Ui::dtkVtkMeshingFilterWidget *ui;
    QMetaObject::Connection connection;
    dtkVtkMeshingFilter *filter = nullptr;

public:
    void connect(void);
};

void dtkVtkMeshingFilterWidgetPrivate::connect(void)
{
    QObject::connect(this->ui->m_decimate, &QCheckBox::toggled, [=] (bool b) {
            this->filter->setDecimate(b);
        });
    QObject::connect(this->ui->m_smooth, &QCheckBox::toggled, [=] (bool b) {
            this->filter->setSmooth(b);        });

#if QT_VERSION < QT_VERSION_CHECK(5, 7, 0)
    // QOverload only available in Qt 5.7
    qWarning() << "Can't connect signals, this plugin's widget does not work with Qt < 5.7";
#else
    QObject::connect(this->ui->m_isoValue, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=] (double v) {
            this->filter->setIsoValue(v);
        });
    QObject::connect(this->ui->m_iterations, QOverload<int>::of(&QSpinBox::valueChanged), [=] (int v) {
            this->filter->setIterations(v);
        });
    QObject::connect(this->ui->m_relaxationFactor, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=] (double v) {
            this->filter->setRelaxationFactor(v);
        });
    QObject::connect(this->ui->m_targetReduction, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=] (double v) {
            this->filter->setTargetReduction(v);
        });
#endif
}

// ///////////////////////////////////////////////////////////////////


dtkVtkMeshingFilterWidget::dtkVtkMeshingFilterWidget(QWidget *parent) : QWidget(parent), d(new dtkVtkMeshingFilterWidgetPrivate)
{
    d->ui = new Ui::dtkVtkMeshingFilterWidget;
    d->ui->setupUi(this);

    d->connection = connect(&dtkCorePluginWidgetManager::instance(), &dtkCorePluginWidgetManager::added, [=] (const QVariant& v, QWidget *w) {

            if (w != this) {
                dtkTrace() << Q_FUNC_INFO << "Mismatch widget";
                return;
            }

            dtkAbstractMeshingFilter *f = v.value<dtkAbstractMeshingFilter *>();
            if (!f) {
                dtkTrace() << Q_FUNC_INFO << "Wrong concept";
                return;
            }

            d->filter = dynamic_cast<dtkVtkMeshingFilter *>(f);
            if (!d->filter) {
                dtkTrace() << Q_FUNC_INFO << "Wrong implementation";
                return;
            }
            d->connect();

            dtkTrace() << Q_FUNC_INFO << "Connection done";
        });
}

dtkVtkMeshingFilterWidget::~dtkVtkMeshingFilterWidget()
{
    disconnect(d->connection);
    delete d->ui;
    delete d;
}

//
// dtkVtkMeshingFilterWidget.cpp ends here
