#pragma once

#include <dtkCore>
#include <dtkAbstractMultiplyFilter.h>

class dtkItkMultiplyFilterPlugin: public dtkAbstractMultiplyFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractMultiplyFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkMultiplyFilterPlugin" FILE "dtkItkMultiplyFilterPlugin.json")

public:
     dtkItkMultiplyFilterPlugin(void) {}
    ~dtkItkMultiplyFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
