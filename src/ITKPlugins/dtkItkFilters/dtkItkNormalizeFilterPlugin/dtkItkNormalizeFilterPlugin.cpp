#include "dtkItkNormalizeFilter.h"
#include "dtkItkNormalizeFilterPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkNormalizeFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkNormalizeFilterPlugin::initialize(void)
{
    dtkImaging::filters::normalize::pluginFactory().record("itkNormalizeFilter", dtkItkNormalizeFilterCreator);
}

void dtkItkNormalizeFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkNormalizeFilter)

