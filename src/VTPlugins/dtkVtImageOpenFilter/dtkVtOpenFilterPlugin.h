#pragma once

#include <dtkCore>
#include <dtkAbstractOpenFilter.h>

class dtkVtOpenFilterPlugin: public dtkAbstractOpenFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractOpenFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtOpenFilterPlugin" FILE "dtkVtOpenFilterPlugin.json")

public:
     dtkVtOpenFilterPlugin(void) {}
    ~dtkVtOpenFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
