// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkImage>

#include <dtkImageReader.h>

class dtkVtkImageReaderPlugin : public dtkImageReaderPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkImageReaderPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtkImageReaderPlugin" FILE "dtkVtkImageReaderPlugin.json")

public:
     dtkVtkImageReaderPlugin(void) {}
    ~dtkVtkImageReaderPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkImageReaderPlugin.h ends here
