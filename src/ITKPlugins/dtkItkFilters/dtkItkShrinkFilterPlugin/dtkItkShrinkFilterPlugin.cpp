#include "dtkItkShrinkFilter.h"
#include "dtkItkShrinkFilterPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkShrinkFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkShrinkFilterPlugin::initialize(void)
{
    dtkImaging::filters::shrink::pluginFactory().record("itkShrinkFilter", dtkItkShrinkFilterCreator);
}

void dtkItkShrinkFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkShrinkFilter)

