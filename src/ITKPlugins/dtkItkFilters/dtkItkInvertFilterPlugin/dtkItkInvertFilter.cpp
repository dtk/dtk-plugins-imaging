#include "dtkItkInvertFilter.h"
#include "dtkItkImageConverter.h"

#include <dtkFilterExecutor.h>

#include <itkImage.h>
#include <itkInvertIntensityImageFilter.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkInvertFilterPrivate
{
    public:
        dtkImage *m_imageIn;
        dtkImage *m_imageOut;
};
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkItkInvertFilter::dtkItkInvertFilter(void): d(new dtkItkInvertFilterPrivate)
{
    d->m_imageIn=NULL;
    d->m_imageOut=new dtkImage();
}

dtkItkInvertFilter::~dtkItkInvertFilter(void)
{
    if(d)
        delete d;
}

void dtkItkInvertFilter::setImage(dtkImage *image)
{
    d->m_imageIn=image;
}

dtkImage *dtkItkInvertFilter::filteredImage(void) const
{
    return d->m_imageOut;
}

void dtkItkInvertFilter::run(void)
{
    if (!d->m_imageIn) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkItkInvertFilter>::run(this, d->m_imageIn);
}

template < typename ImgT, int dim> inline void dtkItkInvertFilter::exec(void)
{
    qDebug()<< Q_FUNC_INFO;

    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType,dim> ImageType;
    typedef dtkItkImageConverter<ImgT,dim> Converter;
    typename ImageType::Pointer image=ImageType::New();
    Converter::convertToNative(d->m_imageIn, image);

    if(image)
    {
        typedef itk::InvertIntensityImageFilter <ImageType, ImageType> InvertImageFilterType;
        typename InvertImageFilterType::Pointer filter = InvertImageFilterType::New();
        filter->SetInput(image);
        filter->Update();
        Converter::convertFromNative(filter->GetOutput(),d->m_imageOut);
    }
}
