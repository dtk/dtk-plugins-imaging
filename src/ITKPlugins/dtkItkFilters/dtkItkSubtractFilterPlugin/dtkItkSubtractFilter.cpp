#include "dtkItkSubtractFilter.h"
#include "dtkItkImageConverter.h"

#include <dtkFilterExecutor.h>

#include <itkImage.h>
#include <itkSubtractImageFilter.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkSubtractFilterPrivate
{
public:
    dtkImage *m_imageIn;
    dtkImage *m_imageOut;

    double m_value;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkItkSubtractFilter::dtkItkSubtractFilter(void): d(new dtkItkSubtractFilterPrivate)
{
    d->m_imageIn=NULL;
    d->m_imageOut=new dtkImage();

    d->m_value=0;
}

dtkItkSubtractFilter::~dtkItkSubtractFilter(void)
{
    if(d)
        delete d;
}

void dtkItkSubtractFilter::setValue(double value)
{
    d->m_value=value;
}

void dtkItkSubtractFilter::setImage(dtkImage *image)
{
    d->m_imageIn=image;
}

dtkImage *dtkItkSubtractFilter::filteredImage(void) const
{
    return d->m_imageOut;
}

void dtkItkSubtractFilter::run(void)
{
    if (!d->m_imageIn) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkItkSubtractFilter>::run(this, d->m_imageIn);
}

template < typename ImgT, int dim> inline void dtkItkSubtractFilter::exec(void)
{
    qDebug()<< Q_FUNC_INFO;

    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType,dim> ImageType;
    typedef dtkItkImageConverter<ImgT,dim> Converter;
    typename ImageType::Pointer image=ImageType::New();
    Converter::convertToNative(d->m_imageIn, image);

    if(image)
    {
        typedef itk::SubtractImageFilter <ImageType, ImageType, ImageType> SubtractImageFilterType;
        typename SubtractImageFilterType::Pointer filter = SubtractImageFilterType::New();
        filter->SetInput1(image);
        filter->SetConstant2(d->m_value);
        filter->Update();
        Converter::convertFromNative(filter->GetOutput(),d->m_imageOut);
    }
}
