// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtDilateWithPseudoBallFilterPlugin.h"

#include "dtkVtDilateWithPseudoBallFilter.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkVtDilateWithPseudoBallFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtDilateWithPseudoBallFilterPlugin::initialize(void)
{
    dtkImaging::filters::dilate::pluginFactory().record("dtkVtDilateWithPseudoBallFilter", dtkVtDilateWithPseudoBallFilterCreator);
}

void dtkVtDilateWithPseudoBallFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtDilateWithPseudoBallFilter)

//
// dtkVtDilateWithPseudoBallFilterPlugin.cpp ends here
