#include "dtkItkWindowingFilter.h"
#include "dtkItkImageConverter.h"

#include <dtkFilterExecutor.h>

#include <itkImage.h>
#include <itkIntensityWindowingImageFilter.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkWindowingFilterPrivate
{
public:
    dtkImage *m_imageIn;
    dtkImage *m_imageOut;

    double m_minIn;
    double m_maxIn;

    double m_minOut;
    double m_maxOut;

};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkItkWindowingFilter::dtkItkWindowingFilter(void): d(new dtkItkWindowingFilterPrivate)
{
    d->m_imageIn=NULL;
    d->m_imageOut=new dtkImage();

    d->m_minIn=0;
    d->m_maxIn=0;
    d->m_minOut=0;
    d->m_maxOut=0;

}

dtkItkWindowingFilter::~dtkItkWindowingFilter(void)
{
    if(d)
        delete d;
}

void dtkItkWindowingFilter::setMinimumIntensityValue (double minIn)
{
    d->m_minIn=minIn;
}

void dtkItkWindowingFilter::setMaximumIntensityValue (double maxIn)
{
    d->m_maxIn=maxIn;
}

void dtkItkWindowingFilter::setMinimumOutputIntensityValue(double minOut)
{
    d->m_minOut=minOut;
}

void dtkItkWindowingFilter::setMaximumOutputIntensityValue(double maxOut)
{
    d->m_maxOut=maxOut;
}

void dtkItkWindowingFilter::setImage(dtkImage *image)
{
    d->m_imageIn=image;
}

dtkImage *dtkItkWindowingFilter::filteredImage(void) const
{
    return d->m_imageOut;
}

void dtkItkWindowingFilter::run(void)
{
    if (!d->m_imageIn) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
    }

    dtkFilterExecutor<dtkItkWindowingFilter>::run(this, d->m_imageIn);
}

template < typename ImgT, int dim> inline void dtkItkWindowingFilter::exec(void)
{
    qDebug()<< Q_FUNC_INFO;

    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType,dim> ImageType;
    typedef dtkItkImageConverter<ImgT,dim> Converter;
    typename ImageType::Pointer image=ImageType::New();
    Converter::convertToNative(d->m_imageIn, image);

    if(image)
    {
        typedef itk::IntensityWindowingImageFilter <ImageType, ImageType> WindowingImageFilterType;
        typename WindowingImageFilterType::Pointer filter = WindowingImageFilterType::New();

        filter->SetInput(image);
        filter->SetWindowMinimum ( ( typename ImgT::PixelType ) d->m_minIn );
        filter->SetWindowMaximum ( ( typename ImgT::PixelType ) d->m_maxIn );
        filter->SetOutputMinimum ( ( typename ImgT::PixelType ) d->m_minOut);
        filter->SetOutputMaximum ( ( typename ImgT::PixelType ) d->m_maxOut);
        filter->Update();
        Converter::convertFromNative(filter->GetOutput(),d->m_imageOut);
    }
}
