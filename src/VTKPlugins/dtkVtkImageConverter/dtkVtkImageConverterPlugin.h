// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCore/dtkImageConverter>

class dtkVtkImageConverterPlugin : public dtkImageConverterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkImageConverterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtkImageConverterPlugin" FILE "dtkVtkImageConverterPlugin.json")

public:
     dtkVtkImageConverterPlugin(void) {}
    ~dtkVtkImageConverterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkImageConverterPlugin.h ends here
