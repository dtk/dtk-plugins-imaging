// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractOpenFilter.h>

class dtkImage;
class dtkVtOpenFilterPrivate;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtOpenFilter final : public dtkAbstractOpenFilter
{
public:
     dtkVtOpenFilter(void);
    ~dtkVtOpenFilter(void);

public:
    void setRadius(double radius);
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);

private:
    dtkVtOpenFilterPrivate *d;
};


inline dtkAbstractOpenFilter *dtkVtOpenFilterCreator(void)
{
    return new dtkVtOpenFilter();
}

//
// dtkVtOpenFilter.h ends here
