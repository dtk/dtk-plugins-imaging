#pragma once

#include <QObject>
#include <dtkAbstractInvertFilter.h>

class dtkImage;
class dtkItkInvertFilterPrivate;

class dtkItkInvertFilter final : public dtkAbstractInvertFilter
{
public:
     dtkItkInvertFilter(void);
    ~dtkItkInvertFilter(void);

public:
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);
    template < typename ImgT, int dim > void exec(void);

private:
    dtkItkInvertFilterPrivate *d;
};


inline dtkAbstractInvertFilter *dtkItkInvertFilterCreator(void)
{
    return new dtkItkInvertFilter();
}
