// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractConnectivityThreshold.h>

class dtkImage;
class dtkItkConnectivityThresholdPrivate;

// /////////////////////////////////////////////////////////////////
// dtkItkConnectivityThreshold
// /////////////////////////////////////////////////////////////////

class dtkItkConnectivityThreshold final : public dtkAbstractConnectivityThreshold
{
public:
     dtkItkConnectivityThreshold(void);
    ~dtkItkConnectivityThreshold(void);

public:
    void setMinSize(double minSize);
    void setImage(dtkImage *image);

public:
    dtkImage *resImage(void) const;

public:
    void run(void);

private:
    dtkItkConnectivityThresholdPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkAbstractConnectivityThreshold *dtkItkConnectivityThresholdCreator(void)
{
    return new dtkItkConnectivityThreshold();
}

//
// dtkItkConnectivityThreshold.h ends here
