// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImageWriter.h>

class dtkItkImageWriterPrivate;

// /////////////////////////////////////////////////////////////////
// dtkItkImageWriter
// /////////////////////////////////////////////////////////////////

class dtkItkImageWriter final : public dtkImageWriter
{
public:
      dtkItkImageWriter(void);
     ~dtkItkImageWriter(void);

public:
     void setPath(const QString& path);
     void setImage(dtkImage *image);

public:
    void write(void);

private:
    dtkItkImageWriterPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkImageWriter *dtkItkImageWriterCreator(void)
{
    return new dtkItkImageWriter();
}


//
// executive_visitor.h ends here
