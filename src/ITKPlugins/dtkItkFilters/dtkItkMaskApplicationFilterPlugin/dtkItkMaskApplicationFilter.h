#pragma once

#include <dtkCore>

#include <dtkAbstractMaskApplicationFilter.h>

class dtkItkMaskApplicationFilterPrivate;
class dtkImage;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkMaskApplicationFilter final : public dtkAbstractMaskApplicationFilter
{
public:
     dtkItkMaskApplicationFilter(void);
    ~dtkItkMaskApplicationFilter(void);

public:
    void setImage(dtkImage *);
    void  setMask(dtkImage *);
    void setBackgroundValue(double);

public:
    dtkImage *result(void) const;

public:
    void run(void);
    template < typename ImgT, int dim > inline void exec(void);

private:
    dtkItkMaskApplicationFilterPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkAbstractMaskApplicationFilter *dtkItkMaskApplicationFilterCreator(void)
{
    return new dtkItkMaskApplicationFilter();
}
