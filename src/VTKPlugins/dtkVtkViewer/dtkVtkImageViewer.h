// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingWidgets/dtkAbstractImageViewer.h>

class dtkImage;
class dtkImageMesh;

class dtkVtkImageViewer : public dtkAbstractImageViewer
{
public:
     dtkVtkImageViewer(void);
    ~dtkVtkImageViewer(void);

public:
    void display(dtkImage *image);
    void display(dtkImageMesh *mesh);

public:
    dtkImage *getInput(void) const;

public:
    QWidget *widget(void);

public:
    template <typename ImgT, int dim> void execDim2(void);
    template <typename ImgT, int dim> void execDim3(void);

private:
    class dtkVtkImageViewerPrivate* d;
};

//
// dtkVtkImageViewer.h ends here
