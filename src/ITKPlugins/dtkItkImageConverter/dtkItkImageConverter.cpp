// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkImageConverter.h"

// /////////////////////////////////////////////////////////////////
// 4D converters specializations
// /////////////////////////////////////////////////////////////////

void dtkItkImageConverter< dtkScalarPixel<unsigned char>, 4 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned char>, 4 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<char>, 4 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<char>, 4 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned short>, 4 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned short>, 4 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<short>, 4 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<short>, 4 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned int>, 4 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned int>, 4 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<int>, 4 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<int>, 4 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned long>, 4 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned long>, 4 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<long>, 4 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<long>, 4 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<float>, 4 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<float>, 4 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<double>, 4 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<double>, 4 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

// /////////////////////////////////////////////////////////////////
// 3D converters specializations
// /////////////////////////////////////////////////////////////////

void dtkItkImageConverter< dtkScalarPixel<unsigned char>, 3 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned char>, 3 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<char>, 3 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<char>, 3 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned short>, 3 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned short>, 3 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<short>, 3 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<short>, 3 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned int>, 3 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned int>, 3 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<int>, 3 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<int>, 3 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned long>, 3 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned long>, 3 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<long>, 3 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<long>, 3 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<float>, 3 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<float>, 3 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<double>, 3 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<double>, 3 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

// /////////////////////////////////////////////////////////////////
// 2D converters specializations
// /////////////////////////////////////////////////////////////////

void dtkItkImageConverter< dtkScalarPixel<unsigned char>, 2 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned char>, 2 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<char>, 2 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<char>, 2 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned short>, 2 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned short>, 2 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<short>, 2 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<short>, 2 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned int>, 2 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned int>, 2 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<int>, 2 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<int>, 2 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned long>, 2 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned long>, 2 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<long>, 2 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<long>, 2 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<float>, 2 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<float>, 2 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<double>, 2 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<double>, 2 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

// /////////////////////////////////////////////////////////////////
// 1D converters specializations
// /////////////////////////////////////////////////////////////////

void dtkItkImageConverter< dtkScalarPixel<unsigned char>, 1 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned char>, 1 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<char>, 1 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<char>, 1 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned short>, 1 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned short>, 1 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<short>, 1 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<short>, 1 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned int>, 1 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned int>, 1 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<int>, 1 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<int>, 1 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned long>, 1 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<unsigned long>, 1 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<long>, 1 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<long>, 1 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<float>, 1 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<float>, 1 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<double>, 1 >::convertToNative(dtkImage *source, ItkImagePointer target)
{
    Handler::convertToNative(source, target);
}

void dtkItkImageConverter< dtkScalarPixel<double>, 1 >::convertFromNative(ItkImagePointer source, dtkImage *target)
{
    Handler::convertFromNative(source, target);
}

//
// dtkItkImageConverter.cpp ends here
