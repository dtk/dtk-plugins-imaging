// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtErosionFilterPlugin.h"
#include "dtkVtErosionFilter.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkVtErosionFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtErosionFilterPlugin::initialize(void)
{
    dtkImaging::filters::erosion::pluginFactory().record("dtkVtErosionFilter", dtkVtErosionFilterCreator);
}

void dtkVtErosionFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtErosionFilter)

//
// dtkVtErosionFilterPlugin.cpp ends here
