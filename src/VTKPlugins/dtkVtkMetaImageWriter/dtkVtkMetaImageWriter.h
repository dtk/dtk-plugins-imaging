// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkVtkMetaImageWriterExport>

#include <dtkImageWriter.h>

// /////////////////////////////////////////////////////////////////
// dtkVtkMetaImageWriter interface
// /////////////////////////////////////////////////////////////////

class DTKVTKMETAIMAGEWRITER_EXPORT  dtkVtkMetaImageWriter : public dtkImageWriter
{
public:
     dtkVtkMetaImageWriter(void);
    ~dtkVtkMetaImageWriter(void);

public:
    void setPath(const QString& path) final;
    void setImage(dtkImage *image) final;

public:
    void write(void) final;

private:
    class dtkVtkMetaImageWriterPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkImageWriter *dtkVtkMetaImageWriterCreator(void)
{
    return new dtkVtkMetaImageWriter();
}

//
// dtkVtkMetaImageWriter.h ends here
