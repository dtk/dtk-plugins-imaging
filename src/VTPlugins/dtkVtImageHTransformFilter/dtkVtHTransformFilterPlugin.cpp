// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtHTransformFilterPlugin.h"
#include "dtkVtHTransformFilter.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkVtHTransformFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtHTransformFilterPlugin::initialize(void)
{
    dtkImaging::filters::htransform::pluginFactory().record("dtkVtHTransformFilter", dtkVtHTransformFilterCreator);
}

void dtkVtHTransformFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtHTransformFilter)

//
// dtkVtHTransformFilterPlugin.cpp ends here
