// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkImageWriter.h>

#include <QtCore>

class dtkVtImageWriterPrivate;

// /////////////////////////////////////////////////////////////////
// dtkVtImageWriter interface
// /////////////////////////////////////////////////////////////////

class dtkVtImageWriter final : public dtkImageWriter
{
public:
     dtkVtImageWriter(void);
    ~dtkVtImageWriter(void);

public:
    void setPath(const QString& path);
    void setImage(dtkImage *image);

public:
    void write(void);

private:
    dtkVtImageWriterPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkImageWriter *dtkVtImageWriterCreator(void)
{
    return new dtkVtImageWriter();
}

//
// dtkVtImageWriter.h ends here
