#include "dtkItkDivideFilter.h"
#include "dtkItkImageConverter.h"

#include <dtkFilterExecutor.h>

#include <itkImage.h>
#include <itkDivideImageFilter.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkDivideFilterPrivate
{
    public:
        dtkImage *m_imageIn;
        dtkImage *m_imageOut;

        double m_value;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkItkDivideFilter::dtkItkDivideFilter(void): d(new dtkItkDivideFilterPrivate)
{
    d->m_imageIn=NULL;
    d->m_imageOut=new dtkImage();
    
    d->m_value=0;
}

dtkItkDivideFilter::~dtkItkDivideFilter(void)
{
    if(d)
        delete d;
}

void dtkItkDivideFilter::setValue(double value)
{
    d->m_value=value;
}

void dtkItkDivideFilter::setImage(dtkImage *image)
{
    d->m_imageIn=image;
}

dtkImage *dtkItkDivideFilter::filteredImage(void) const
{
    return d->m_imageOut;
}

void dtkItkDivideFilter::run(void)
{
    if (!d->m_imageIn) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
    }

    dtkFilterExecutor<dtkItkDivideFilter>::run(this, d->m_imageIn);
}

template < typename ImgT, int dim> inline void dtkItkDivideFilter::exec(void)
{
    qDebug()<< Q_FUNC_INFO;

    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType,dim> ImageType;
    typedef dtkItkImageConverter<ImgT,dim> Converter;
    typename ImageType::Pointer image=ImageType::New();
    Converter::convertToNative(d->m_imageIn,image);

    if(image)
    {
        typedef itk::DivideImageFilter <ImageType, ImageType, ImageType> DivideImageFilterType;
        typename DivideImageFilterType::Pointer filter = DivideImageFilterType::New();
        filter->SetInput1(image);
        filter->SetConstant2(d->m_value);
        filter->Update();
        Converter::convertFromNative(filter->GetOutput(),d->m_imageOut);
    }
}
