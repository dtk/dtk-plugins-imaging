// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkCloseFilter.h"

#include "dtkItkImageConverter.h"

#include <dtkImage.h>
#include <dtkFilterExecutor.h>

#include <dtkLog>

#include <itkImage.h>
#include <itkGrayscaleMorphologicalClosingImageFilter.h>
#include <itkBinaryBallStructuringElement.h>

// /////////////////////////////////////////////////////////////////
// dtkItkCloseFilterPrivate
// /////////////////////////////////////////////////////////////////

class dtkItkCloseFilterPrivate
{
public:
    dtkImage *image_in;
    dtkImage *image_out;

    double radius;

public:
    template < typename ImgT, int dim > void exec(void);
};

// /////////////////////////////////////////////////////////////////
// dtkItkCloseFilter
// /////////////////////////////////////////////////////////////////

dtkItkCloseFilter::dtkItkCloseFilter(void): dtkAbstractCloseFilter(), d(new dtkItkCloseFilterPrivate)
{
    d->image_in = nullptr;
    d->image_out = new dtkImage();

    d->radius=0;
}

dtkItkCloseFilter::~dtkItkCloseFilter(void)
{
    delete d;
}

void dtkItkCloseFilter::setRadius(double radius)
{
    d->radius = radius;
}

void dtkItkCloseFilter::setImage(dtkImage *image)
{
    d->image_in = image;
}

dtkImage *dtkItkCloseFilter::filteredImage(void) const
{
    return d->image_out;
}

void dtkItkCloseFilter::run(void)
{
    if (!d->image_in) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkItkCloseFilterPrivate>::run(d, d->image_in);
}

template < typename ImgT, int dim> inline void dtkItkCloseFilterPrivate::exec(void)
{
    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType, dim> ImageType;
    typedef dtkItkImageConverter<ImgT, dim> Converter;

    typename ImageType::Pointer image = ImageType::New();

    Converter::convertToNative(this->image_in, image);

    if (image) {
        typedef itk::BinaryBallStructuringElement < typename ImgT::PixelType, dim> StructuringElementType;
        typedef itk::GrayscaleMorphologicalClosingImageFilter< ImageType, ImageType, StructuringElementType > CloseType;

        typename CloseType::Pointer filter = CloseType::New();

        StructuringElementType ball;
        ball.SetRadius(this->radius);
        ball.CreateStructuringElement();

        filter->SetInput (image);
        filter->SetKernel ( ball );
        filter->Update();

        Converter::convertFromNative(filter->GetOutput(), this->image_out);
    }
}

//
// dtkItkCloseFilter.cpp ends here
