#pragma once

#include <dtkCore>
#include <dtkAbstractConnectedComponentsFilter.h>

class dtkVtConnectedComponentsFilterPlugin: public dtkAbstractConnectedComponentsFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractConnectedComponentsFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtConnectedComponentsFilterPlugin" FILE "dtkVtConnectedComponentsFilterPlugin.json")

public:
     dtkVtConnectedComponentsFilterPlugin(void) {}
    ~dtkVtConnectedComponentsFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
