#pragma once

#include <QObject>
#include <dtkAbstractMedianFilter.h>

class dtkImage;
class dtkItkMedianFilterPrivate;

class dtkItkMedianFilter final : public dtkAbstractMedianFilter
{
public:
     dtkItkMedianFilter(void);
    ~dtkItkMedianFilter(void);

public:
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);
    template < typename ImgT, int dim > void exec(void);

private:
    dtkItkMedianFilterPrivate *d;
};


inline dtkAbstractMedianFilter *dtkItkMedianFilterCreator(void)
{
    return new dtkItkMedianFilter();
}
