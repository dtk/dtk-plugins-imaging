#include "dtkItkErodeFilter.h"
#include "dtkItkImageConverter.h"

#include <dtkFilterExecutor.h>

#include <itkImage.h>
#include <itkGrayscaleErodeImageFilter.h>
#include <itkBinaryBallStructuringElement.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkErodeFilterPrivate
{
    public:
        dtkImage *m_imageIn;
        dtkImage *m_imageOut;

        double m_radius;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkItkErodeFilter::dtkItkErodeFilter(void): d(new dtkItkErodeFilterPrivate)
{
    d->m_imageIn=NULL;
    d->m_imageOut=new dtkImage();
    
    d->m_radius=0;
}

dtkItkErodeFilter::~dtkItkErodeFilter(void)
{
    if(d)
        delete d;
}

void dtkItkErodeFilter::setRadius(double radius)
{
    d->m_radius=radius;
}

void dtkItkErodeFilter::setImage(dtkImage *image)
{
    d->m_imageIn=image;
}

dtkImage *dtkItkErodeFilter::filteredImage(void) const
{
    return d->m_imageOut;
}

void dtkItkErodeFilter::run(void)
{
    if (!d->m_imageIn) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkItkErodeFilter>::run(this, d->m_imageIn);
}

template < typename ImgT, int dim> inline void dtkItkErodeFilter::exec(void)
{
    qDebug()<< Q_FUNC_INFO;

    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType,dim> ImageType;
    typedef dtkItkImageConverter<ImgT,dim> Converter;
    typename ImageType::Pointer image=ImageType::New();
    Converter::convertToNative(d->m_imageIn, image);

    if(image)
    {
        typedef itk::BinaryBallStructuringElement < typename ImgT::PixelType, dim> StructuringElementType;
        typedef itk::GrayscaleErodeImageFilter <ImageType, ImageType, StructuringElementType> ErodeImageFilterType;
        typename ErodeImageFilterType::Pointer filter = ErodeImageFilterType::New();
        filter->SetInput(image);
        filter->SetRadius(d->m_radius);
        filter->Update();
        Converter::convertFromNative(filter->GetOutput(),d->m_imageOut);
    }
}
