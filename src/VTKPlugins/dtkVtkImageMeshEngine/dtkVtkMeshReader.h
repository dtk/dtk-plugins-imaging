#pragma once

#include <dtkImageMeshReader.h>

class QString;
class dtkImageMesh;

// /////////////////////////////////////////////////////////////////
// dtkVtkMeshReader interface
// /////////////////////////////////////////////////////////////////

class dtkVtkMeshReader final : public dtkImageMeshReader
{
public:
     dtkVtkMeshReader(void);
    ~dtkVtkMeshReader(void);

public:
    dtkImageMesh *read(const QString& path);
};

inline dtkImageMeshReader *dtkVtkMeshReaderCreator(void)
{
    return new dtkVtkMeshReader();
}
