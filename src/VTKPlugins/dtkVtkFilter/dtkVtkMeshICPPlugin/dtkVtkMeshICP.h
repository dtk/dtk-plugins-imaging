#pragma once

#include <dtkAbstractMeshICP.h>

class dtkImage;
class dtkImageMesh;

// /////////////////////////////////////////////////////////////////
// dtkVtkMeshICP
// /////////////////////////////////////////////////////////////////

class dtkVtkMeshICP : public dtkAbstractMeshICP
{
public:
     dtkVtkMeshICP(void);
    ~dtkVtkMeshICP(void);

public:
    void setMovingMesh(dtkImageMesh *movingMesh);
    void setFixedMesh(dtkImageMesh *fixedMsh);
    void setMode(int mode);

public:
    dtkImageMesh* resMesh(void) const;

public:
    void run();
//    template < typename ImgT, int dim>  void exec(void);

private:
    class dtkVtkMeshICPPrivate* d;
};

// /////////////////////////////////////////////////////////////////

inline dtkAbstractMeshICP *dtkVtkMeshICPCreator(void)
{
    return new dtkVtkMeshICP();
}
