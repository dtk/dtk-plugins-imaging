// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkAddFilterPlugin.h"

#include "dtkItkAddFilter.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkAddFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkAddFilterPlugin::initialize(void)
{
    dtkImaging::filters::add::pluginFactory().record("dtkItkAddFilter", dtkItkAddFilterCreator);
}

void dtkItkAddFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkAddFilter)


//
// dtkItkAddFilterPlugin.cpp ends here
