// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkImageWriter.h>

// /////////////////////////////////////////////////////////////////
// dtkVtImageWriterPlugin interface
// /////////////////////////////////////////////////////////////////

class dtkVtImageWriterPlugin : public dtkImageWriterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkImageWriterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtImageWriterPlugin" FILE "dtkVtImageWriterPlugin.json")

public:
     dtkVtImageWriterPlugin(void) {}
    ~dtkVtImageWriterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkVtImageWriterPlugin.h ends here
