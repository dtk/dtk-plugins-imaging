// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtImageReader.h"
#include "dtkVtImageConverter.h"

#include <dtkImage.h>

#include <dtkLog>

#include <dtkImageReader.h>

#include <vt_image.h>
#include <vt_inrimage.h>

// /////////////////////////////////////////////////////////////////
// dtkVtImageReader implementation
// /////////////////////////////////////////////////////////////////

dtkVtImageReader::dtkVtImageReader(void) : dtkImageReader()
{

}

dtkVtImageReader::~dtkVtImageReader(void)
{

}

dtkImage *dtkVtImageReader::read(const QString& path)
{
    vt_image myvtimage;

    VT_Image( &myvtimage );

    char *str = const_cast<char *>(qPrintable(path));
    if ( VT_ReadInrimage( &myvtimage, str ) != 1 ) {
        dtkError() << Q_FUNC_INFO << "Cannot read image ";
        return NULL;
    }

    dtkImage *image = new dtkImage;
    dtkVtImageConverter::convertFromNative(&myvtimage, image);
    free( myvtimage.array );

    return image;
}

QStringList dtkVtImageReader::types(void)
{
    return QStringList();
}

//
// dtkVtImageReader.cpp ends here
