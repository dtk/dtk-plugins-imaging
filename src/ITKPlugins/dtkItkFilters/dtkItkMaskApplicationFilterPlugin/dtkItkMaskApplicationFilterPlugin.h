#pragma once

#include <dtkCore>
#include <dtkAbstractMaskApplicationFilter.h>

class dtkItkMaskApplicationFilterPlugin: public dtkAbstractMaskApplicationFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractMaskApplicationFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkMaskApplicationFilterPlugin" FILE "dtkItkMaskApplicationFilterPlugin.json")

public:
     dtkItkMaskApplicationFilterPlugin(void) {}
    ~dtkItkMaskApplicationFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
