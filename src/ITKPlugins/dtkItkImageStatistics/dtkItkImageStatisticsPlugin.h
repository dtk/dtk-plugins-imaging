// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractImageStatistics.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkImageStatisticsPlugin: public dtkAbstractImageStatisticsPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractImageStatisticsPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkImageStatisticsPlugin" FILE "dtkItkImageStatisticsPlugin.json")

public:
     dtkItkImageStatisticsPlugin(void) {}
    ~dtkItkImageStatisticsPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkItkImageStatisticsPlugin.h ends here
