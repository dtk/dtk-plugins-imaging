// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtImageConverter.h"

#include <dtkImage.h>
#include <dtkImageData.h>
#include <dtkTemplatedImageData.h>
#include <dtkImageDefaultStorage.h>
#include <dtkPixelImpl.h>

#include <vt_image.h>
#include <vt_common.h>
#include <convert.h>

#include <cstdlib>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////


inline dtkImageData *dtkVtImageConverter::createDtkImageData(int dim, int vt_pixel_type, const dtkArray<qlonglong>& extent, void *raw_data)
{
    dtkImageData *img_data = nullptr;

    if (dim == 2) {
        switch (vt_pixel_type) {
        case SCHAR:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<char>, 2>(extent, reinterpret_cast<char *>(raw_data));
            break;

        case UCHAR:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned char>, 2>(extent, reinterpret_cast<unsigned char *>(raw_data));
            break;

        case SSHORT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<short>, 2>(extent, reinterpret_cast<short *>(raw_data));
            break;

        case USHORT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned short>, 2>(extent, reinterpret_cast<unsigned short *>(raw_data));
            break;

        case SINT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<int>, 2>(extent, reinterpret_cast<int *>(raw_data));
            break;

        case UINT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned int>, 2>(extent, reinterpret_cast<unsigned int *>(raw_data));
            break;

        case SLINT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<long>, 2>(extent, reinterpret_cast<long *>(raw_data));
            break;

        case ULINT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned long>, 2>(extent, reinterpret_cast<unsigned long *>(raw_data));
            break;

        case FLOAT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<float>, 2>(extent, reinterpret_cast<float *>(raw_data));
            break;

        case DOUBLE:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<double>, 2>(extent, reinterpret_cast<double *>(raw_data));
            break;

        case TYPE_UNKNOWN:
        default:
            dtkWarn() << Q_FUNC_INFO << "Pixel type " << vt_pixel_type << "is not handled. Nothing is done";
            img_data = nullptr;
            break;
        }

    } else if (dim == 3) {

        switch (vt_pixel_type) {
        case SCHAR:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<char>, 3>(extent, reinterpret_cast<char *>(raw_data));
            break;

        case UCHAR:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned char>, 3>(extent, reinterpret_cast<unsigned char *>(raw_data));
            break;

        case SSHORT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<short>, 3>(extent, reinterpret_cast<short *>(raw_data));
            break;

        case USHORT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned short>, 3>(extent, reinterpret_cast<unsigned short *>(raw_data));
            break;

        case SINT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<int>, 3>(extent, reinterpret_cast<int *>(raw_data));
            break;

        case UINT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned int>, 3>(extent, reinterpret_cast<unsigned int *>(raw_data));
            break;

        case SLINT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<long>, 3>(extent, reinterpret_cast<long *>(raw_data));
            break;

        case ULINT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned long>, 3>(extent, reinterpret_cast<unsigned long *>(raw_data));
            break;

        case FLOAT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<float>, 3>(extent, reinterpret_cast<float *>(raw_data));
            break;

        case DOUBLE:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<double>, 3>(extent, reinterpret_cast<double *>(raw_data));
            break;

        case TYPE_UNKNOWN:
        default:
            dtkWarn() << Q_FUNC_INFO << "Pixel type " << vt_pixel_type << "is not handled. Nothing is done";
            img_data = nullptr;
            break;
        }

    } else {
        dtkWarn() << Q_FUNC_INFO << "Image of dimension " << dim << "is not handled. Nothing is done";
        img_data = nullptr;
    }

    return img_data;
}

int dtkVtImageConverter::convertToNative(dtkImage *source, vt_image *target)
{
    VT_Image(target);

    int storage_type = source->storageType();

    switch(storage_type) {
    default :
    case QMetaType::Void:
        target->type = TYPE_UNKNOWN;
        break;
    case QMetaType::Char:
        target->type = SCHAR;
        break;
    case QMetaType::UChar:
        target->type = UCHAR;
        break;
    case QMetaType::Short:
        target->type = SSHORT;
        break;
    case QMetaType::UShort:
        target->type = USHORT;
        break;
    case QMetaType::Int:
        target->type = SINT;
        break;
    case QMetaType::UInt:
        target->type = UINT;
        break;
    case QMetaType::Long:
        target->type = SLINT;
        break;
    case QMetaType::ULong:
        target->type = ULINT;
        break;
    case QMetaType::Float:
        target->type = FLOAT;
        break;
    case QMetaType::Double:
        target->type = DOUBLE;
        break;
    }

    if (target->type == TYPE_UNKNOWN) {
        dtkError() << Q_FUNC_INFO << "Input image type " << storage_type << "is unknown. Aborting.";
        return EXIT_FAILURE;
    }

    int dim = source->dim();

    target->dim.v = source->pixelDim();

    target->dim.x = source->xDim();
    target->dim.y = source->yDim();
    if (dim > 2 ) {
        target->dim.z = source->zDim();
    } else {
        target->dim.z = 1;
    }

    target->siz.x = source->spacing()[0];
    target->siz.y = source->spacing()[1];
    if (dim > 2 ) {
        target->siz.z = source->spacing()[2];
    } else {
        target->siz.z = 1;
    }

    target->buf = source->rawData();

    // Experimental
    target->ctr.x = source->extent()[0];
    target->ctr.y = source->extent()[2];
    if (dim > 2 ) {
        target->ctr.z = source->extent()[4];
    } else {
        target->ctr.z = 0;
    }
    // end experimental

    if (!target->buf) {
        dtkError() << Q_FUNC_INFO << "Input image has no raw data. Aborting.";
        return EXIT_FAILURE;
    }

    if (VT_AllocArrayImage(target) != 1) {
        dtkError() <<  Q_FUNC_INFO << "Cannot allocate image array";
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int dtkVtImageConverter::convertFromNative(vt_image *source, dtkImage *target)
{
    int dim = 3;
    if (source->dim.z == 1) {
        dim = 2;
    }

    dtkArray<qlonglong> extent(2 * dim, 0LL);
    extent[0] = source->ctr.x;
    extent[1] = source->ctr.x + source->dim.x - 1;
    extent[2] = source->ctr.y;
    extent[3] = source->ctr.y + source->dim.y - 1;
    if (dim > 2) {
        extent[4] = source->ctr.z;
        extent[5] = source->ctr.z + source->dim.z - 1;
    }

    dtkArray<double> origin(dim);
    origin[0] = source->off.x;
    origin[1] = source->off.y;
    if (dim > 2) {
        origin[2] = source->off.z;
    }

    dtkArray<double> spacing(dim);
    spacing[0] = source->siz.x;
    spacing[1] = source->siz.y;
    if (dim > 2) {
        spacing[2] = source->siz.z;
    }

    dtkImageData *img_data = dtkVtImageConverter::createDtkImageData(dim, source->type, extent, source->buf);

    if (!img_data) {
        dtkError() << Q_FUNC_INFO << "Convertion from VT to dtk failed. Aborting.";
        return EXIT_FAILURE;
    }

    target->setData(img_data);
    target->setSpacing(spacing);
    target->setOrigin(origin);

    return EXIT_SUCCESS;
}

//
// dtkVtImageConverter.cpp ends here
