// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractDivideFilter.h>
#include <QObject>

class dtkImage;
class dtkItkDivideFilterPrivate;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkDivideFilter final : public dtkAbstractDivideFilter
{
public:
     dtkItkDivideFilter(void);
    ~dtkItkDivideFilter(void);

public:
    void setValue(double value);
     void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);
    template < typename ImgT, int dim > void exec(void);

private:
    dtkItkDivideFilterPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkAbstractDivideFilter *dtkItkDivideFilterCreator(void)
{
    return new dtkItkDivideFilter();
}

//
// dtkItkDivideFilter.h ends here
