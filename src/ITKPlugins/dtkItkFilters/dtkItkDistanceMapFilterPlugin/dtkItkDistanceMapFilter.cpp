// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkDistanceMapFilter.h"

#include "dtkItkImageConverter.h"

#include <dtkImaging.h>
#include <dtkFilterExecutor.h>

#include <dtkLog>

#include <itkImage.h>
#include <itkSignedMaurerDistanceMapImageFilter.h>

// ///////////////////////////////////////////////////////////////////
//  dtkItkImageSubtractionPrivate
// ///////////////////////////////////////////////////////////////////

class dtkItkDistanceMapFilterPrivate
{
public:
    dtkImage *input;
    dtkImage *res;

public:
    template < typename ImgT, int dim > typename std::enable_if<(dim >  1)>::type exec(void);
    template < typename ImgT, int dim > typename std::enable_if<(dim <= 1)>::type exec(void);
};

// ///////////////////////////////////////////////////////////////////
// dtkItkDistanceMapFilter
// ///////////////////////////////////////////////////////////////////

dtkItkDistanceMapFilter::dtkItkDistanceMapFilter(void) : dtkAbstractDistanceMapFilter(), d(new dtkItkDistanceMapFilterPrivate)
{
    d->input = nullptr;
    d->res = new dtkImage();
}

dtkItkDistanceMapFilter::~dtkItkDistanceMapFilter(void)
{
    delete d;
}

void dtkItkDistanceMapFilter::setInput(dtkImage *image)
{
    d->input = image;
}

dtkImage *dtkItkDistanceMapFilter::filteredImage(void) const
{
    return d->res;
}

void dtkItkDistanceMapFilter::run(void)
{
    if (!d->input) {
        dtkWarn() << Q_FUNC_INFO << "input image is missing";
        return;
    }

    dtkFilterExecutor<dtkItkDistanceMapFilterPrivate>::run(d, d->input);
}

template < typename ImgT, int dim> inline typename std::enable_if<(dim > 1)>::type dtkItkDistanceMapFilterPrivate::exec(void)
{
    typedef dtkItkImageConverter<ImgT,dim> Converter;
    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType, dim> ImageType;

    typename ImageType::Pointer image = ImageType::New();

    Converter::convertToNative(this->input, image);

    if (image) {

        typedef itk::Image< float, dim > FloatImageType;

        typedef  itk::SignedMaurerDistanceMapImageFilter< ImageType, FloatImageType  > SignedMaurerDistanceMapImageFilterType;

        typename SignedMaurerDistanceMapImageFilterType::Pointer distanceMapImageFilter = SignedMaurerDistanceMapImageFilterType::New();
        distanceMapImageFilter->SetInput(image);
        distanceMapImageFilter->SetSquaredDistance(0);
        distanceMapImageFilter->SetUseImageSpacing(1);

        try {
            distanceMapImageFilter->Update();
        } catch (itk::ExceptionObject& err) {
            std::cerr << "ExceptionObject caught!"<< std::endl;
        }

        typedef dtkItkImageConverter< dtkScalarPixel<float>, dim > OutputConverter;
        OutputConverter::convertFromNative(distanceMapImageFilter->GetOutput(), this->res);
    }
}

template < typename ImgT, int dim> inline typename std::enable_if<(dim <= 1)>::type dtkItkDistanceMapFilterPrivate::exec(void)
{
    dtkDebug() << Q_FUNC_INFO << "Method is not implemented for dimension less than 2";
}

//
// dtkItkDistanceMapFilter.cpp ends here
