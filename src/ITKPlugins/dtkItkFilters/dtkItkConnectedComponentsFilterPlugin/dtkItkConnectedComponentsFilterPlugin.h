// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>
#include <dtkAbstractConnectedComponentsFilter.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkConnectedComponentsFilterPlugin: public dtkAbstractConnectedComponentsFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractConnectedComponentsFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkConnectedComponentsFilterPlugin" FILE "dtkItkConnectedComponentsFilterPlugin.json")

public:
     dtkItkConnectedComponentsFilterPlugin(void) {}
    ~dtkItkConnectedComponentsFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkItkConnectedComponentsFilterPlugin.h ends here
