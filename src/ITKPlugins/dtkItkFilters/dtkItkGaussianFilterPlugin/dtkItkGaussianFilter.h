// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractGaussianFilter.h>

class dtkImage;
class dtkItkGaussianFilterPrivate;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkGaussianFilter final : public dtkAbstractGaussianFilter
{
public:
     dtkItkGaussianFilter(void);
    ~dtkItkGaussianFilter(void);

public:
    void setSigma(double sigma);
    void setSigmaValues(double *sigma_values);
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);
    template < typename ImgT, int dim > void exec(void);

private:
    dtkItkGaussianFilterPrivate *d;
};


inline dtkAbstractGaussianFilter *dtkItkGaussianFilterCreator(void)
{
    return new dtkItkGaussianFilter();
}

//
// dtkItkGaussianFilter.h ends here
