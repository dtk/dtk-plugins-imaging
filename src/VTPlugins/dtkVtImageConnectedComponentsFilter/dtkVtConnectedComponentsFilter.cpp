// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtConnectedComponentsFilter.h"
#include "dtkVtImageConverter.h"

#include <dtkFilterExecutor.h>

#include <vt_image.h>
#include <vt_common.h>

#include <api-connexe.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtConnectedComponentsFilterPrivate
{
public:
    dtkImage *image_in;
    dtkImage *image_out;

    int min;
    int max;

    double value;

public:
    template < typename ImgT, int dim > void exec(void);
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkVtConnectedComponentsFilter::dtkVtConnectedComponentsFilter(void): dtkAbstractConnectedComponentsFilter(), d(new dtkVtConnectedComponentsFilterPrivate)
{
    d->image_in = nullptr;
    d->image_out = new dtkImage();

    d->min = 1;
    d->max = 3;

    d->value = 0;
}

dtkVtConnectedComponentsFilter::~dtkVtConnectedComponentsFilter(void)
{
    delete d;
}

void dtkVtConnectedComponentsFilter::setValue(double value)
{
    d->value = value;
}

void dtkVtConnectedComponentsFilter::setImage(dtkImage *image)
{
    d->image_in = image;
}

dtkImage *dtkVtConnectedComponentsFilter::filteredImage(void) const
{
    return d->image_out;
}

void dtkVtConnectedComponentsFilter::run(void)
{
    if (!d->image_in) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkVtConnectedComponentsFilterPrivate>::run(d, d->image_in);
}

template < typename ImgT, int dim> inline void dtkVtConnectedComponentsFilterPrivate::exec(void)
{
    vt_image image;

    QString str = QString("-low-threshold %1 -high-threshold %2 -label-output")
        .arg(this->min)
        .arg(this->max);

    if (dtkVtImageConverter::convertToNative(this->image_in, &image) != EXIT_FAILURE) {

        vt_image res; VT_InitFromImage(&res, &image, (char *)(nullptr), USHORT);

        if ( API_connexe( &image, nullptr, &res, str.toLocal8Bit().data(), (char*)nullptr ) != 1 ) {
            free( image.array );
            if (res.array) {
                free( res.array );
            }
            dtkError() << Q_FUNC_INFO << "Computation failed. Aborting.";
            return;
        }

        dtkVtImageConverter::convertFromNative(&res, this->image_out);

        free( image.array );
        image.array = nullptr;
        free( res.array );
        res.array = nullptr;

    } else {
        dtkError() << Q_FUNC_INFO << "Conversion to VT format failed. Aborting.";
    }
}

//
// dtkVtConnectedComponentsFilter.cpp ends here
