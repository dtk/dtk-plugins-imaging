#pragma once

#include <dtkCore>
#include <dtkAbstractDilateFilter.h>

class dtkVtDilateWithPseudoBallFilterPlugin: public dtkAbstractDilateFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractDilateFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtDilateWithPseudoBallFilterPlugin" FILE "dtkVtDilateWithPseudoBallFilterPlugin.json")

public:
     dtkVtDilateWithPseudoBallFilterPlugin(void) {}
    ~dtkVtDilateWithPseudoBallFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
