// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <dtkImageData.h>
#include <dtkImage.h>

#include <dtkTemplatedImageData.h>
#include <dtkImageDefaultStorage.h>
#include <dtkPixelImpl.h>
#include <dtkVtkPixelPolicy.h>

#include <vtkImageData.h>
#include <vtkImageExport.h>
#include <vtkImageImport.h>
#include <vtkSmartPointer.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

template < typename Pixel, int dim> inline  void dtkVtkImageConverter<Pixel, dim>::convertToNative(dtkImage *source, vtkImageData *target)
{
    dtkImageData *data = source->data();

    if (!data) {
        dtkWarn() << Q_FUNC_INFO << "Source image has no data.";
        return;
    }
    if (dim > 3) {
        dtkWarn() << Q_FUNC_INFO << "Vtk is usuable only with 3D images, cannot convert";
        return;
    }

    const dtkArray<qlonglong>& extent = source->extent();

    vtkSmartPointer<vtkImageImport> imageImport = vtkSmartPointer<vtkImageImport>::New();
    if (dim == 2) {
        imageImport->SetWholeExtent(extent[0], extent[1], extent[2], extent[3], 0, 0);
        imageImport->SetDataOrigin(source->origin().at(0), source->origin().at(1), 0);
        imageImport->SetDataSpacing(source->spacing().at(0), source->spacing().at(1), 1);

    } else {
        imageImport->SetWholeExtent(extent[0], extent[1], extent[2], extent[3], extent[4], extent[5]);
        imageImport->SetDataOrigin(source->origin().at(0), source->origin().at(1), source->origin().at(2));
        imageImport->SetDataSpacing(source->spacing().at(0), source->spacing().at(1), source->spacing().at(2));
    }
    imageImport->SetDataExtentToWholeExtent();

    dtkVtkPixelTypeSetter<typename Pixel::PixelType>::setPixelType(imageImport);

    imageImport->SetNumberOfScalarComponents(Pixel::PixelDim);
    imageImport->SetImportVoidPointer(source->rawData(), 1);
    imageImport->Update();

    target->ShallowCopy(imageImport->GetOutput());
}

template < typename Pixel, int dim> void dtkVtkImageConverter<Pixel, dim>::convertFromNative(vtkImageData *source, dtkImage *target)
{
    int vtk_pixel_type = dtkVtkPixelTrait<typename Pixel::PixelType>::vtkType;

    if (vtk_pixel_type == source->GetScalarType()) {

        dtkArray<qlonglong> extent(2 * dim, 0LL);
        dtkArray<double> origin(dim);
        dtkArray<double> spacing(dim);
        dtkArray<double> direction(dim * dim);

        int *ext = source->GetExtent();
        for (int i = 0; i < 2 * dim; ++i) {
            extent[i] = ext[i];
        }

        double *ori = source->GetOrigin();
        for (int i = 0; i < dim; ++i) {
            origin[i] = ori[i];
        }

        double *spa = source->GetSpacing();
        for (int i = 0; i < dim; ++i) {
            spacing[i] = spa[i];
        }

        typename Pixel::PixelType *data = reinterpret_cast<typename Pixel::PixelType *>(source->GetScalarPointer());

        dtkImageDefaultStorage<Pixel, dim> *storage = new dtkImageDefaultStorage<Pixel, dim>(extent, data);

        target->setData(storage);
        target->setSpacing(spacing);
        target->setOrigin(origin);

    } else {
        dtkError() << Q_FUNC_INFO << "Cannot convert from VTK to dtk, vtk pixel is" << source->GetScalarType() << ". Pixel type expected is" << vtk_pixel_type;
    }
}

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

inline int dtkVtkImageConverterNoTemplate::storageTypeToVtkType(int storage_type)
{
    switch (storage_type) {
    case QMetaType::Void:
        return VTK_VOID;

    case QMetaType::Bool:
        return VTK_BIT;

    case QMetaType::Char:
        return VTK_CHAR;

    case QMetaType::UChar:
        return VTK_UNSIGNED_CHAR;

    case QMetaType::Short:
        return VTK_SHORT;

    case QMetaType::UShort:
        return VTK_UNSIGNED_SHORT;

    case QMetaType::Int:
        return VTK_INT;

    case QMetaType::UInt:
        return VTK_UNSIGNED_INT;

    case QMetaType::Long:
        return VTK_LONG;

    case QMetaType::ULong:
        return VTK_UNSIGNED_LONG;

    case QMetaType::Float:
        return VTK_FLOAT;

    case QMetaType::Double:
        return VTK_DOUBLE;

    default:
        return VTK_VOID;
    }
}

inline dtkImageData *dtkVtkImageConverterNoTemplate::createDtkImageData(int dim, int vtk_pixel_type, const dtkArray<qlonglong>& extent, void *raw_data)
{
    dtkImageData *img_data = nullptr;

    if (dim == 2) {
        switch (vtk_pixel_type) {
        case VTK_CHAR:
        case VTK_SIGNED_CHAR:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<char>, 2>(extent, reinterpret_cast<char *>(raw_data));
            break;

        case VTK_UNSIGNED_CHAR:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned char>, 2>(extent, reinterpret_cast<unsigned char *>(raw_data));
            break;

        case VTK_SHORT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<short>, 2>(extent, reinterpret_cast<short *>(raw_data));
            break;

        case VTK_UNSIGNED_SHORT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned short>, 2>(extent, reinterpret_cast<unsigned short *>(raw_data));
            break;

        case VTK_INT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<int>, 2>(extent, reinterpret_cast<int *>(raw_data));
            break;

        case VTK_UNSIGNED_INT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned int>, 2>(extent, reinterpret_cast<unsigned int *>(raw_data));
            break;

        case VTK_LONG:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<long>, 2>(extent, reinterpret_cast<long *>(raw_data));
            break;

        case VTK_UNSIGNED_LONG:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned long>, 2>(extent, reinterpret_cast<unsigned long *>(raw_data));
            break;

        case VTK_FLOAT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<float>, 2>(extent, reinterpret_cast<float *>(raw_data));
            break;

        case VTK_DOUBLE:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<double>, 2>(extent, reinterpret_cast<double *>(raw_data));
            break;

        case VTK_BIT:
        case VTK_VOID:
        default:
            dtkWarn() << Q_FUNC_INFO << "Pixel type " << vtk_pixel_type << "is not handled. Nothing is done";
            img_data = nullptr;
            break;
        }

    } else if (dim == 3) {

        switch (vtk_pixel_type) {
        case VTK_CHAR:
        case VTK_SIGNED_CHAR:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<char>, 3>(extent, reinterpret_cast<char *>(raw_data));
            break;

        case VTK_UNSIGNED_CHAR:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned char>, 3>(extent, reinterpret_cast<unsigned char *>(raw_data));
            break;

        case VTK_SHORT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<short>, 3>(extent, reinterpret_cast<short *>(raw_data));
            break;

        case VTK_UNSIGNED_SHORT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned short>, 3>(extent, reinterpret_cast<unsigned short *>(raw_data));
            break;

        case VTK_INT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<int>, 3>(extent, reinterpret_cast<int *>(raw_data));
            break;

        case VTK_UNSIGNED_INT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned int>, 3>(extent, reinterpret_cast<unsigned int *>(raw_data));
            break;

        case VTK_LONG:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<long>, 3>(extent, reinterpret_cast<long *>(raw_data));
            break;

        case VTK_UNSIGNED_LONG:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<unsigned long>, 3>(extent, reinterpret_cast<unsigned long *>(raw_data));
            break;

        case VTK_FLOAT:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<float>, 3>(extent, reinterpret_cast<float *>(raw_data));
            break;

        case VTK_DOUBLE:
            img_data = new dtkImageDefaultStorage< dtkScalarPixel<double>, 3>(extent, reinterpret_cast<double *>(raw_data));
            break;

        case VTK_BIT:
        case VTK_VOID:
        default:
            dtkWarn() << Q_FUNC_INFO << "Pixel type " << vtk_pixel_type << "is not handled. Nothing is done";
            img_data = nullptr;
            break;
        }

    } else {
        dtkWarn() << Q_FUNC_INFO << "Image of dimension " << dim << "is not handled. Nothing is done";
        img_data = nullptr;
    }

    return img_data;
}

inline void dtkVtkImageConverterNoTemplate::convertToNative(dtkImage *source, vtkImageData *target)
{
    dtkImageData *data = source->data();
    if (!data) {
        dtkWarn() << Q_FUNC_INFO << "Input Image is void. Nothing is done";
        return;
    }

    int dim = source->dim();

    if (dim > 3) {
        dtkWarn() << Q_FUNC_INFO << "vtk only deal with 3D images. Nothing is done";
        return;
    }

    const dtkArray<qlonglong>& extent = source->extent();
    const dtkArray<double>& origin    = source->origin();
    const dtkArray<double>& spacing   = source->spacing();

    vtkSmartPointer<vtkImageImport> imageImport = vtkSmartPointer<vtkImageImport>::New();
    if (dim == 2) {
        imageImport->SetWholeExtent(extent[0], extent[1], extent[2], extent[3], 0, 0);
        imageImport->SetDataOrigin(origin[0], origin[1], 0);
        imageImport->SetDataSpacing(spacing[0], spacing[1], 1);

    } else {
        imageImport->SetWholeExtent(extent[0], extent[1], extent[2], extent[3], extent[4], extent[5]);
        imageImport->SetDataOrigin(origin[0], origin[1], origin[2]);
        imageImport->SetDataSpacing(spacing[0], spacing[1], spacing[2]);
    }
    imageImport->SetDataExtentToWholeExtent();

    int vtk_type = dtkVtkImageConverterNoTemplate::storageTypeToVtkType(source->storageType());
    imageImport->SetDataScalarType(vtk_type);

    imageImport->SetNumberOfScalarComponents(source->pixelDim());
    imageImport->SetImportVoidPointer(source->rawData(), 1);

    imageImport->Update();

    target->ShallowCopy(imageImport->GetOutput());
}

inline void dtkVtkImageConverterNoTemplate::convertFromNative(vtkImageData *source, dtkImage *target)
{
    int *ext = source->GetExtent();
    int dim = 3;
    if (ext[4] == 0 && ext[5] == 0) {
        dim = 2;
    }

    dtkArray<qlonglong> extent(2 * dim, 0LL);
    for (int i = 0; i < 2 * dim; ++i) {
        extent[i] = ext[i];
    }

    dtkArray<double> origin(dim);
    double *ori = source->GetOrigin();
    for (int i = 0; i < dim; ++i) {
        origin[i] = ori[i];
    }

    dtkArray<double> spacing(dim);
    double *spa = source->GetSpacing();
    for (int i = 0; i < dim; ++i) {
        spacing[i] = spa[i];
    }

    dtkImageData *img_data = dtkVtkImageConverterNoTemplate::createDtkImageData(dim, source->GetScalarType(), extent, source->GetScalarPointer());

    if (!img_data) {
        dtkError() << Q_FUNC_INFO << "Convertion from Vtk to dtk failed. Aborting.";
        return;
    }

    target->setData(img_data);
    target->setSpacing(spacing);
    target->setOrigin(origin);
}

//
// dtkVtkImageConverter.tpp ends here
