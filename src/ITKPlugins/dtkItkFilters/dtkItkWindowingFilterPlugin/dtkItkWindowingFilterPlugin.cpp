#include "dtkItkWindowingFilter.h"
#include "dtkItkWindowingFilterPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkWindowingFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkWindowingFilterPlugin::initialize(void)
{
    dtkImaging::filters::windowing::pluginFactory().record("itkWindowingFilter", dtkItkWindowingFilterCreator);
}

void dtkItkWindowingFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkWindowingFilter)

