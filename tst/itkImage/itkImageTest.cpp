// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "itkImageTest.h"
//#include "itkImage.h"


#include "dtkItkImageConverter.h"
#include <dtkImaging>

void itkImageTestCase::initTestCase(void)
{

}

void itkImageTestCase::init(void)
{

}


void itkImageTestCase::testItkConverter(void)
{
    // qDebug()<<Q_FUNC_INFO<<__LINE__;
    // int *tab=new int[25];

    // for(int i=0;i<25;i++)
    //     tab[i]=i;
    //     qDebug()<<Q_FUNC_INFO<<__LINE__<<tab;

    // dtkImageDefaultStorage<dtkScalarPixel<int>,2 >* storage=new dtkImageDefaultStorage<dtkScalarPixel<int>,2>(5,5,(int*)tab);
    // dtkImage img;
    // img.setData(storage);
    //     qDebug()<<Q_FUNC_INFO<<__LINE__;

    // dtkItkImageConverter<dtkScalarPixel<int> ,2> converter;
    // itk::Image<int>::Pointer itkImg=itk::Image<int>::New();
    // converter.convertToNative(&img, itkImg);
    // typename itk::Image<int>::IndexType index;
    //     qDebug()<<Q_FUNC_INFO<<__LINE__;

    // for(int i=0;i<25;i++)
    // {
    //           qDebug()<<Q_FUNC_INFO<<__LINE__;

    //   index[0]=i%5;
    //   index[1]=i/5;
    //           qDebug()<<Q_FUNC_INFO<<__LINE__;

    //   std::cout<<itkImg->GetPixel(index);
    //   QVERIFY(itkImg->GetPixel(index)==tab[i]);
    // }
}

void itkImageTestCase::testItkConverter2(void)
{
    // qDebug()<<Q_FUNC_INFO<<__LINE__;
    // typedef itk::Image<int,2> ImageType;

    // ImageType::RegionType region;
    // ImageType::IndexType start;

    // start[0]=0;
    // start[1]=0;

    // ImageType::SizeType size;
    // size[0]=5;
    // size[1]=5;

    // region.SetSize(size);
    // region.SetIndex(start);

    // ImageType::Pointer image=ImageType::New();
    // image->SetRegions(region);
    // image->Allocate();

    // for(int i=0;i<25;i++)
    // {
	// ImageType::IndexType pixelIndex;
	// pixelIndex[0]=i%5;
	// pixelIndex[1]=i/5;

	// image->SetPixel(pixelIndex, i);
    // }

    // dtkItkImageConverter<dtkScalarPixel<int> ,2> converter;
    // dtkImage img=NULL;
    // converter.convertFromNative(image, &img);

    // int* tab=static_cast<int*>(img.rawData());
    // for(int i=0;i<25;i++)
    // {
    //           qDebug()<<Q_FUNC_INFO<<__LINE__;
    //   QVERIFY(i==tab[i]);
    // }
}

void itkImageTestCase::cleanupTestCase(void)
{

}

void itkImageTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(itkImageTest, itkImageTestCase)

//
// itkImageTest.cpp ends here
