#pragma once

#include <dtkCore>
#include <dtkAbstractOpenFilter.h>

class dtkItkOpenFilterPlugin: public dtkAbstractOpenFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractOpenFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkOpenFilterPlugin" FILE "dtkItkOpenFilterPlugin.json")

public:
     dtkItkOpenFilterPlugin(void) {}
    ~dtkItkOpenFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
