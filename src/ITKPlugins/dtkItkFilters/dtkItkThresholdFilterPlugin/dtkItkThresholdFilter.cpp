#include "dtkItkThresholdFilter.h"
#include "dtkItkImageConverter.h"

#include <dtkImage.h>
#include <dtkFilterExecutor.h>

#include <itkImage.h>
#include <itkThresholdImageFilter.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkThresholdFilterPrivate
{
public:
    dtkImage *m_imageIn;
    dtkImage *m_imageOut;

    double m_threshold;
    double m_outsideValue;
    bool m_mode;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkItkThresholdFilter::dtkItkThresholdFilter(void): d(new dtkItkThresholdFilterPrivate)
{
    d->m_imageIn = NULL;
    d->m_imageOut = new dtkImage();

    d->m_threshold = 0;
    d->m_outsideValue = 0;
    d->m_mode = true;
}

dtkItkThresholdFilter::~dtkItkThresholdFilter(void)
{
    if (d)
        delete d;
}

void dtkItkThresholdFilter::setThreshold(double threshold)
{
    d->m_threshold = threshold;
}

void dtkItkThresholdFilter::setOutsideValue(double outsiderValue)
{
    d->m_outsideValue = outsiderValue;
}

void dtkItkThresholdFilter::setMode(bool mode)
{
    d->m_mode = mode;
}

void dtkItkThresholdFilter::setImage(dtkImage *image)
{
    d->m_imageIn = image;
}

dtkImage *dtkItkThresholdFilter::filteredImage(void) const
{
    return d->m_imageOut;
}

void dtkItkThresholdFilter::run(void)
{
    if (!d->m_imageIn) {
        dtkWarn() << Q_FUNC_INFO << "Missing input";
        return;
    }

    dtkFilterExecutor<dtkItkThresholdFilter>::run(this, d->m_imageIn);
}

template < typename ImgT, int dim> inline void dtkItkThresholdFilter::exec(void)
{
    qDebug()<< Q_FUNC_INFO;

    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType, dim> ImageType;
    typedef dtkItkImageConverter<ImgT, dim> Converter;
    typename ImageType::Pointer image = ImageType::New();
    Converter::convertToNative(d->m_imageIn, image);

    if (image) {
        typedef itk::ThresholdImageFilter <ImageType> ThresholdImageFilterType;
        typename ThresholdImageFilterType::Pointer filter = ThresholdImageFilterType::New();
        filter->SetInput(image);

        if (d->m_mode) {
            filter->ThresholdBelow(d->m_threshold);
        } else {
            filter->ThresholdAbove(d->m_threshold);
	}
            filter->SetOutsideValue(d->m_outsideValue);
            filter->Update();
            Converter::convertFromNative(filter->GetOutput(), d->m_imageOut);
    }
}
