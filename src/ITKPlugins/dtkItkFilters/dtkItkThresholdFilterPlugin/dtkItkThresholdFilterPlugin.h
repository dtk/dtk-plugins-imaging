#pragma once

#include <dtkCore>
#include <dtkAbstractThresholdFilter.h>

class dtkItkThresholdFilterPlugin: public dtkAbstractThresholdFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractThresholdFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkThresholdFilterPlugin" FILE "dtkItkThresholdFilterPlugin.json")

public:
     dtkItkThresholdFilterPlugin(void) {}
    ~dtkItkThresholdFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
