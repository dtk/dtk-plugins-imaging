// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>
#include <dtkAbstractHTransformFilter.h>

class dtkVtHTransformFilterPlugin : public dtkAbstractHTransformFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractHTransformFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtHTransformFilterPlugin" FILE "dtkVtHTransformFilterPlugin.json")

public:
     dtkVtHTransformFilterPlugin(void) {}
    ~dtkVtHTransformFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkVtHTransformFilterPlugin.h ends here
