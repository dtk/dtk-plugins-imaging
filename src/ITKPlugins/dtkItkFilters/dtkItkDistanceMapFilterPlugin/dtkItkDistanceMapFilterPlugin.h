#pragma once

#include <dtkCore>
#include <dtkAbstractDistanceMapFilter.h>

class dtkItkDistanceMapFilterPlugin: public dtkAbstractDistanceMapFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractDistanceMapFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkDistanceMapFilterPlugin" FILE "dtkItkDistanceMapFilterPlugin.json")

public:
     dtkItkDistanceMapFilterPlugin(void) {}
    ~dtkItkDistanceMapFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
