// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImageReader.h>

// /////////////////////////////////////////////////////////////////
// dtkVtImageReader interface
// /////////////////////////////////////////////////////////////////

class dtkVtImageReader final : public dtkImageReader
{
public:
     dtkVtImageReader(void);
    ~dtkVtImageReader(void);

public:
    dtkImage *read(const QString& path);

public:
    QStringList types(void);
};

// /////////////////////////////////////////////////////////////////

inline dtkImageReader *dtkVtImageReaderCreator(void)
{
    return new dtkVtImageReader();
}

//
// dtkVtImageReader.h ends here
