#pragma once

#include <dtkCore>
#include <dtkAbstractCloseFilter.h>

class dtkItkCloseFilterPlugin: public dtkAbstractCloseFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractCloseFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkCloseFilterPlugin" FILE "dtkItkCloseFilterPlugin.json")

public:
     dtkItkCloseFilterPlugin(void) {}
    ~dtkItkCloseFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
