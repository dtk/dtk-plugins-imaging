// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkImageWriter.h>

class itkImageWriterPlugin : public dtkImageWriterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkImageWriterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkImageWriterPlugin" FILE "dtkItkImageWriterPlugin.json")

public:
     itkImageWriterPlugin(void) {}
    ~itkImageWriterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkImageWriterPlugin.h ends here
