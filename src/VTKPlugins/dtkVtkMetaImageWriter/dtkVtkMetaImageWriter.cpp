// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtkMetaImageWriter.h"
#include "dtkVtkImageConverter.h"

#include <dtkLog>

#include "vtkImageData.h"
#include "vtkSmartPointer.h"
#include "vtkImageCast.h"
#include "vtkMetaImageWriter.h"

// /////////////////////////////////////////////////////////////////
// dtkVtkMetaImageWriterPrivate
// /////////////////////////////////////////////////////////////////

class dtkVtkMetaImageWriterPrivate
{
public:
    QString path;
    dtkImage *image;
};

// /////////////////////////////////////////////////////////////////
// dtkVtkMetaImageWriter
// /////////////////////////////////////////////////////////////////

dtkVtkMetaImageWriter::dtkVtkMetaImageWriter(void) : dtkImageWriter(), d(new dtkVtkMetaImageWriterPrivate)
{
    d->image = nullptr;
}

dtkVtkMetaImageWriter::~dtkVtkMetaImageWriter(void)
{
    d->image = nullptr;
    delete d;
}

void dtkVtkMetaImageWriter::setPath(const QString& path)
{
    d->path = path;
}

void dtkVtkMetaImageWriter::setImage(dtkImage *image)
{
    if (!image) {
        dtkWarn() << Q_FUNC_INFO << "Input image is null. Nothing is done";
        return;
    }
    d->image = image;
}

void dtkVtkMetaImageWriter::write(void)
{
    if (!d->image) {
        dtkWarn() << Q_FUNC_INFO << "No image to write. Nothing is done.";
        return;
    }

    if (d->path.isEmpty()) {
        dtkWarn() << Q_FUNC_INFO << "No path has been set. Nothing is done.";
        return;
    }

    vtkImageData *img = vtkImageData::New();
    dtkVtkImageConverter::convertToNative(d->image, img);

    vtkSmartPointer<vtkMetaImageWriter> writer = vtkSmartPointer<vtkMetaImageWriter>::New();
    writer->SetCompression(false);
    writer->SetInputData(img);
    writer->SetFileName(qPrintable(d->path));
    writer->Write();

    img->Delete();
}

//
// dtkVtkMetaImageWriter.cpp ends here
