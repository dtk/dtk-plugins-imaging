#include "dtkItkXorFilter.h"
#include "dtkItkImageConverter.h"

#include <dtkImaging.h>
#include <dtkFilterExecutor.h>

#include <type_traits>

#include <itkImage.h>
#include <itkXorImageFilter.h>

// ///////////////////////////////////////////////////////////////////
//  dtkItkImageSubtractionPrivate
// ///////////////////////////////////////////////////////////////////

class dtkItkXorFilterPrivate
{
public:
    dtkImage *m_lhs;
    dtkImage *m_rhs;
    dtkImage *m_res;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkItkXorFilter::dtkItkXorFilter(void) : d(new dtkItkXorFilterPrivate)
{
    d->m_lhs = NULL;
    d->m_rhs = NULL;
    d->m_res = new dtkImage();
}

dtkItkXorFilter::~dtkItkXorFilter(void)
{
    delete d;
}

void dtkItkXorFilter::setLhs(dtkImage *image)
{
    d->m_lhs = image;
}

void dtkItkXorFilter::setRhs(dtkImage *image)
{
    d->m_rhs = image;
}

dtkImage *dtkItkXorFilter::result(void) const
{
    return d->m_res;
}

void dtkItkXorFilter::run(void)
{
    if (!d->m_lhs || !d->m_rhs) {
        dtkWarn() << Q_FUNC_INFO << "input image is missing";
        return;
    }
    
    dtkFilterExecutor<dtkItkXorFilter, INTEGER_ONLY >::run(this, d->m_lhs);
}

template < typename ImgT, int dim> inline void dtkItkXorFilter::exec(void)
{
    typedef dtkItkImageConverter<ImgT,dim> Converter;
    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType,dim> ImageType;
    
    typename ImageType::Pointer image1 = ImageType::New();
    typename ImageType::Pointer image2 = ImageType::New();
    
    Converter::convertToNative(d->m_lhs, image1);
    Converter::convertToNative(d->m_rhs, image2);
    
    if (image1 && image2) {
        
        typedef itk::XorImageFilter <ImageType, ImageType, ImageType> XorImageFilterType;
        
        typename XorImageFilterType::Pointer xorFilter = XorImageFilterType::New();
        xorFilter->SetInput1(image1);
        xorFilter->SetInput2(image2);
        
        try {
            xorFilter->Update();
        } catch (itk::ExceptionObject & err) {
            std::cerr << "ExceptionObject caught in "/*<< metaObject()->className()*/ << std::endl;
            std::cerr << err << std::endl;
        }
        
        Converter::convertFromNative(xorFilter->GetOutput(), d->m_res);
    }
}

