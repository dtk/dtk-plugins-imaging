// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractCloseFilter.h>

class dtkImage;
class dtkItkCloseFilterPrivate;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkCloseFilter final : public dtkAbstractCloseFilter
{
public:
     dtkItkCloseFilter(void);
    ~dtkItkCloseFilter(void);

public:
    void setRadius(double radius);
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);

private:
    dtkItkCloseFilterPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkAbstractCloseFilter *dtkItkCloseFilterCreator(void)
{
    return new dtkItkCloseFilter();
}

//
// dtkItkCloseFilter.h ends here
