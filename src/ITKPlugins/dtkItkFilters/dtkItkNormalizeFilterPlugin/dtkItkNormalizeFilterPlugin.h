#pragma once

#include <dtkCore>
#include <dtkAbstractNormalizeFilter.h>

class dtkItkNormalizeFilterPlugin: public dtkAbstractNormalizeFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractNormalizeFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkNormalizeFilterPlugin" FILE "dtkItkNormalizeFilterPlugin.json")

public:
     dtkItkNormalizeFilterPlugin(void) {}
    ~dtkItkNormalizeFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
