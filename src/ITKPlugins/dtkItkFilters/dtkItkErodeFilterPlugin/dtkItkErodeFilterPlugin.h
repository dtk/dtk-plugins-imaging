#pragma once

#include <dtkCore>
#include <dtkAbstractErodeFilter.h>

class dtkItkErodeFilterPlugin: public dtkAbstractErodeFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractErodeFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkErodeFilterPlugin" FILE "dtkItkErodeFilterPlugin.json")

public:
     dtkItkErodeFilterPlugin(void) {}
    ~dtkItkErodeFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
