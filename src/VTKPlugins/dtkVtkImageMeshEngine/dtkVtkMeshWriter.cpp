// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>

#include "dtkVtkMeshWriter.h"
#include "dtkVtkImageMeshEngine.h"

#include "vtkImageCast.h"
#include "vtkMetaImageWriter.h"
#include "vtkPolyDataWriter.h"
#include "vtkPolyData.h"
#include "vtkSmartPointer.h"

#include "dtkImageMesh.h"

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtkMeshWriterPrivate
{
public:
    dtkImageMesh *mesh;
    QString path;
};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

dtkVtkMeshWriter::dtkVtkMeshWriter(void) : d(new dtkVtkMeshWriterPrivate)
{
    d->mesh = nullptr;
}

dtkVtkMeshWriter::~dtkVtkMeshWriter(void)
{
    delete d;
}

void dtkVtkMeshWriter::setPath(const QString& path)
{
    d->path = path;
}

void dtkVtkMeshWriter::setMesh(dtkImageMesh *mesh)
{
    if (!mesh) {
        dtkWarn() << "No mesh has been set";
        return;
    }
    d->mesh = mesh;
}

void dtkVtkMeshWriter::write(void)
{
    if (!d->mesh) {
        qWarning() << "no mesh set, aborting";
        return;
    }

    if (d->path.isEmpty()) {
        qWarning() << "no path set, aborting";
        return;
    }

    dtkVtkImageMeshEngine *engine = dynamic_cast<dtkVtkImageMeshEngine *>(d->mesh->engine());
    if (engine) {
        vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New();
        writer->SetFileName(qPrintable(d->path));

        vtkPolyData *pdata = engine->polyData();
        if (!pdata) {
            dtkError() << "No polydata is available. Nothing is written.";
            return;
        }
        writer->SetInputData(pdata);
        writer->Write();
    }
}

//
// dtkVtkMeshWriter.cpp ends here
