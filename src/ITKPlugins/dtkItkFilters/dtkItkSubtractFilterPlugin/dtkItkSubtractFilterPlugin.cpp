#include "dtkItkSubtractFilter.h"
#include "dtkItkSubtractFilterPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkSubtractFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkSubtractFilterPlugin::initialize(void)
{
    dtkImaging::filters::subtract::pluginFactory().record("itkSubtractFilter", dtkItkSubtractFilterCreator);
}

void dtkItkSubtractFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkSubtractFilter)

