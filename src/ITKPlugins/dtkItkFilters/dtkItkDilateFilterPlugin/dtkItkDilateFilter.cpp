// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkDilateFilter.h"

#include "dtkItkImageConverter.h"

#include <dtkImage.h>
#include <dtkFilterExecutor.h>

#include <dtkLog>

#include <itkImage.h>
#include <itkGrayscaleDilateImageFilter.h>
#include <itkBinaryBallStructuringElement.h>

// /////////////////////////////////////////////////////////////////
// dtkItkDilateFilterPrivate
// /////////////////////////////////////////////////////////////////

class dtkItkDilateFilterPrivate
{
public:
    dtkImage *image_in;
    dtkImage *image_out;

    double radius;

public:
    template < typename ImgT, int dim > void exec(void);
};

// ///////////////////////////////////////////////////////////////////
// dtkItkDilateFilter
// ///////////////////////////////////////////////////////////////////

dtkItkDilateFilter::dtkItkDilateFilter(void): dtkAbstractDilateFilter(), d(new dtkItkDilateFilterPrivate)
{
    d->image_in = nullptr;
    d->image_out = new dtkImage();

    d->radius = 0;
}

dtkItkDilateFilter::~dtkItkDilateFilter(void)
{
    delete d;
}

void dtkItkDilateFilter::setRadius(double radius)
{
    d->radius = radius;
}

void dtkItkDilateFilter::setImage(dtkImage *image)
{
    d->image_in = image;
}

dtkImage *dtkItkDilateFilter::filteredImage(void) const
{
    return d->image_out;
}

void dtkItkDilateFilter::run(void)
{
    if (!d->image_in) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkItkDilateFilterPrivate>::run(d, d->image_in);
}

template < typename ImgT, int dim> inline void dtkItkDilateFilterPrivate::exec(void)
{
    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType, dim> ImageType;
    typedef dtkItkImageConverter<ImgT, dim> Converter;

    typename ImageType::Pointer image = ImageType::New();

    Converter::convertToNative(this->image_in, image);

    if (image) {
        typedef itk::BinaryBallStructuringElement <typename ImgT::PixelType, dim> StructuringElementType;
        typedef itk::GrayscaleDilateImageFilter< ImageType, ImageType, StructuringElementType > DilateType;

        typename DilateType::Pointer filter = DilateType::New();

        StructuringElementType ball;
        ball.SetRadius(this->radius);
        ball.CreateStructuringElement();

        filter->SetInput(image);
        filter->SetKernel(ball);
        filter->Update();
        Converter::convertFromNative(filter->GetOutput(), this->image_out);
    }
}

//
// dtkItkDilateFilter.cpp ends here
