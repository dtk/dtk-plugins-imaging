// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtWatershedFilter.h"
#include "dtkVtWatershedFilterPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkVtWatershedFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtWatershedFilterPlugin::initialize(void)
{
    dtkImaging::filters::watershed::pluginFactory().record("dtkVtWatershedFilter", dtkVtWatershedFilterCreator);
}

void dtkVtWatershedFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtWatershedFilter)

//
// dtkVtWatershedFilterPlugin.cpp ends here
