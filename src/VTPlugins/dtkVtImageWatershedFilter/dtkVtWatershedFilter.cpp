// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtWatershedFilter.h"

#include "dtkVtImageConverter.h"

#include <dtkFilterExecutor.h>

#include <vt_image.h>

#include <api-watershed.h>

// /////////////////////////////////////////////////////////////////
// dtkVtWatershedFilterPrivate implementation
// /////////////////////////////////////////////////////////////////

class dtkVtWatershedFilterPrivate
{
public:
    dtkImage *image_in;
    dtkImage *seed;
    dtkImage *image_out;

    QString control;

public:
    template < typename ImgT, int dim > void exec(void);
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkVtWatershedFilter::dtkVtWatershedFilter(void) : dtkAbstractWatershedFilter(), d(new dtkVtWatershedFilterPrivate)
{
    d->image_in = nullptr;
    d->seed = nullptr;
    d->image_out = new dtkImage();

    d->control = "first";
}

dtkVtWatershedFilter::~dtkVtWatershedFilter(void)
{
    delete d;
}

void dtkVtWatershedFilter::setImage(dtkImage *image)
{
    d->image_in = image;
}

void dtkVtWatershedFilter::setSeed(dtkImage *seed)
{
    d->seed = seed;
}

void dtkVtWatershedFilter::setControlParameter(const QString& control_parameter)
{
    d->control = control_parameter;
}

dtkImage *dtkVtWatershedFilter::filteredImage(void) const
{
    return d->image_out;
}

void dtkVtWatershedFilter::run(void)
{
    if (!d->image_in || !d->seed) {
        dtkWarn() << Q_FUNC_INFO << "no image input or no seed in input";
        return;
    }

    dtkFilterExecutor<dtkVtWatershedFilterPrivate>::run(d, d->image_in);
}

template < typename ImgT, int dim> inline void dtkVtWatershedFilterPrivate::exec(void)
{
    vt_image image;
    vt_image seed;
    char str[100];

    sprintf( str, "-labelchoice %s", qPrintable(control));

    if (dtkVtImageConverter::convertToNative(this->image_in, &image) != EXIT_FAILURE) {
        if (dtkVtImageConverter::convertToNative(this->seed, &seed) != EXIT_FAILURE) {

            if ( API_watershed( &image, &seed, &image, str, (char*)nullptr ) != 1 ) {
                free( image.array );
                image.array = nullptr;
                free( seed.array );
                seed.array = nullptr;
                dtkError() << Q_FUNC_INFO << "Computation failed. Aborting.";
                return;
            }

            dtkVtImageConverter::convertFromNative(&image, this->image_out);

            free( seed.array );
            seed.array = nullptr;

        } else {
            dtkError() << Q_FUNC_INFO << "Conversion to VT format failed for seed image. Aborting.";
        }
        free( image.array );
        image.array = nullptr;

    } else {
        dtkError() << Q_FUNC_INFO << "Conversion to VT format failed for input image. Aborting.";
    }
}

//
// dtkVtWatershedFilter.cpp ends here
