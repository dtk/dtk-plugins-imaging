#include "dtkVtConnectedComponentsFilter.h"
#include "dtkVtConnectedComponentsFilterPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkVtConnectedComponentsFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtConnectedComponentsFilterPlugin::initialize(void)
{
    dtkImaging::filters::connectedComponents::pluginFactory().record("dtkVtConnectedComponentsFilter", dtkVtConnectedComponentsFilterCreator);
}

void dtkVtConnectedComponentsFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtConnectedComponentsFilter)
