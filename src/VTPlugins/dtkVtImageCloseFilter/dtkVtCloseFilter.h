// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractCloseFilter.h>

class dtkImage;
class dtkVtCloseFilterPrivate;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtCloseFilter final : public dtkAbstractCloseFilter
{
public:
     dtkVtCloseFilter(void);
    ~dtkVtCloseFilter(void);

public:
    void setRadius(double radius);
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);

private:
    dtkVtCloseFilterPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkAbstractCloseFilter *dtkVtCloseFilterCreator(void)
{
    return new dtkVtCloseFilter();
}

//
// dtkVtCloseFilter.h ends here
