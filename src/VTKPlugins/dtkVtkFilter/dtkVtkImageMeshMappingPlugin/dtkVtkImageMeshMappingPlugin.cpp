// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtkImageMeshMapping.h"
#include "dtkVtkImageMeshMappingPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// vtkImageMeshMappingPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtkImageMeshMappingPlugin::initialize(void)
{
    dtkImaging::filters::imageMeshMapping::pluginFactory().record("vtkImageMeshMappingFilter", dtkVtkImageMeshMappingCreator);
}

void dtkVtkImageMeshMappingPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtkImageMeshMapping)

//
// dtkVtkImageMeshMappingPlugin.cpp ends here
