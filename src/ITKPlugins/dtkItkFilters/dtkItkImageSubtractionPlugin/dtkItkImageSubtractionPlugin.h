#pragma once

#include <dtkCore>
#include <dtkAbstractImageSubtraction.h>

class dtkItkImageSubtractionPlugin: public dtkAbstractImageSubtractionPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractImageSubtractionPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkImageSubtractionPlugin" FILE "dtkItkImageSubtractionPlugin.json")

public:
     dtkItkImageSubtractionPlugin(void) {}
    ~dtkItkImageSubtractionPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
