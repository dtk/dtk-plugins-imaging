#pragma once

#include <dtkCore>
#include <dtkAbstractDilateFilter.h>

class dtkItkDilateFilterPlugin: public dtkAbstractDilateFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractDilateFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkDilateFilterPlugin" FILE "dtkItkDilateFilterPlugin.json")

public:
     dtkItkDilateFilterPlugin(void) {}
    ~dtkItkDilateFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
