#pragma once

#include <QObject>
#include <dtkAbstractOpenFilter.h>

class dtkImage;
class dtkItkOpenFilterPrivate;

class dtkItkOpenFilter final : public dtkAbstractOpenFilter
{
    

public:
     dtkItkOpenFilter(void);
    ~dtkItkOpenFilter(void);

public:
    void setRadius(double radius);
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);
    template < typename ImgT, int dim > void exec(void);

private:
    dtkItkOpenFilterPrivate *d;
};


inline dtkAbstractOpenFilter *dtkItkOpenFilterCreator(void)
{
    return new dtkItkOpenFilter();
}
