#pragma once

#include <QObject>
#include <dtkAbstractImageSubtraction.h>

class dtkImage;
class dtkItkImageSubtractionPrivate;

class dtkItkImageSubtraction : public dtkAbstractImageSubtraction
{
    

public:
     dtkItkImageSubtraction(void);
    ~dtkItkImageSubtraction(void);

public:
    void setLhs(dtkImage *image);
    void setRhs(dtkImage *image);

public:
    dtkImage *result(void) const;

public:
    void run(void);
    template < typename ImgT, int dim > inline void exec(void);

private:
    dtkItkImageSubtractionPrivate *d;
};


inline dtkAbstractImageSubtraction *dtkItkImageSubtractionCreator(void)
{
    return new dtkItkImageSubtraction();
}
