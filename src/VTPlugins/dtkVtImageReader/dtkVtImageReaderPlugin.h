// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkImage>

#include <dtkImageReader.h>

class dtkVtImageReaderPlugin : public dtkImageReaderPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkImageReaderPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtImageReaderPlugin" FILE "dtkVtImageReaderPlugin.json")

public:
     dtkVtImageReaderPlugin(void) {}
    ~dtkVtImageReaderPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkImageReaderPlugin.h ends here
