// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtkMetaImageWriter.h"
#include "dtkVtkMetaImageWriterPlugin.h"

#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkVtkMetaImageWriterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtkMetaImageWriterPlugin::initialize(void)
{
    dtkImaging::writer::pluginFactory().record("dtkVtkMetaImageWriter", dtkVtkMetaImageWriterCreator);
}

void dtkVtkMetaImageWriterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta Writer
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtkMetaImageWriter)

//
// dtkVtkMetaImageWriterPlugin.cpp ends here
