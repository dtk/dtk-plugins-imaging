// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractGaussianFilter.h>

class dtkImage;
class dtkVtGaussianFilterPrivate;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtGaussianFilter final : public dtkAbstractGaussianFilter
{
public:
     dtkVtGaussianFilter(void);
    ~dtkVtGaussianFilter(void);

public:
    void setSigma(double sigma);
    void setSigmaValues(double *sigma_values);
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);

private:
    dtkVtGaussianFilterPrivate *d;
};


inline dtkAbstractGaussianFilter *dtkVtGaussianFilterCreator(void)
{
    return new dtkVtGaussianFilter();
}

//
// dtkVtGaussianFilter.h ends here
