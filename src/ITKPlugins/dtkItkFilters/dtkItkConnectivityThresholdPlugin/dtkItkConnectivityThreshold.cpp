// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkConnectivityThreshold.h"

#include "dtkItkImageConverter.h"

#include <dtkImage.h>
#include <dtkFilterExecutor.h>

#include <dtkLog>

#include <itkImage.h>
#include <itkConnectedComponentImageFilter.h>
#include <itkRelabelComponentImageFilter.h>
#include <itkBinaryThresholdImageFilter.h>
#include <itkExceptionObject.h>

// /////////////////////////////////////////////////////////////////
// dtkItkConnectivityThresholdPrivate
// /////////////////////////////////////////////////////////////////

class dtkItkConnectivityThresholdPrivate
{
public:
    dtkImage *image_in;
    dtkImage *image_out;

    double min_size;

public:
    template < typename ImgT, int dim > void exec(void);
};

// ///////////////////////////////////////////////////////////////////
// dtkItkConnectivityThreshold
// ///////////////////////////////////////////////////////////////////

dtkItkConnectivityThreshold::dtkItkConnectivityThreshold(void): dtkAbstractConnectivityThreshold(), d(new dtkItkConnectivityThresholdPrivate)
{
    d->image_in = nullptr;
    d->image_out = new dtkImage();

    d->min_size = 0;
}

dtkItkConnectivityThreshold::~dtkItkConnectivityThreshold(void)
{
    delete d;
}

void dtkItkConnectivityThreshold::setMinSize(double minSize)
{
    d->min_size = minSize;
}

void dtkItkConnectivityThreshold::setImage(dtkImage *image)
{
    d->image_in = image;
}

dtkImage *dtkItkConnectivityThreshold::resImage(void) const
{
    return d->image_out;
}

void dtkItkConnectivityThreshold::run(void)
{
    if (!d->image_in || !d->min_size) {
        dtkWarn() << Q_FUNC_INFO << "Missing inputs";
        return;
    }

    dtkFilterExecutor<dtkItkConnectivityThresholdPrivate, INTEGER_ONLY>::run(d, d->image_in);
}

template < typename ImgT, int dim> inline void dtkItkConnectivityThresholdPrivate::exec(void)
{
    try {
       typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType, dim > ImageType;
       typedef dtkItkImageConverter<ImgT, dim> Converter;

       typename ImageType::Pointer image = ImageType::New();

       Converter::convertToNative(this->image_in, image);

       if (image) {
           typedef itk::ConnectedComponentImageFilter <ImageType, ImageType> ConnectedComponentFilterType;

           typename ConnectedComponentFilterType::Pointer connectedComponentFilter = ConnectedComponentFilterType::New();

           connectedComponentFilter->SetInput(image);
           connectedComponentFilter->Update();

           // RELABEL COMPONENTS according to their sizes (0:largest(background))
           typedef itk::RelabelComponentImageFilter<ImageType, ImageType> FilterType;
           typename FilterType::Pointer relabelFilter = FilterType::New();
           relabelFilter->SetInput(connectedComponentFilter->GetOutput());
           relabelFilter->SetMinimumObjectSize(this->min_size);
           relabelFilter->Update();

           // BINARY FILTER
           typedef itk::BinaryThresholdImageFilter <ImageType, ImageType> BinaryThresholdImageFilterType;

           typename BinaryThresholdImageFilterType::Pointer thresholdFilter = BinaryThresholdImageFilterType::New();
           thresholdFilter->SetInput(relabelFilter->GetOutput());
           thresholdFilter->SetUpperThreshold(0);
           thresholdFilter->SetInsideValue(0);
           thresholdFilter->SetOutsideValue(1);
           thresholdFilter->Update();

           Converter::convertFromNative(thresholdFilter->GetOutput(), this->image_out);
       }

    } catch( itk::ExceptionObject & err ) {
        dtkError() << Q_FUNC_INFO << "ExceptionObject caught in sizeThresholdingProcess!";
        std::cerr << err << std::endl;
    }
}

//
// dtkItkConnectivityThreshold.cpp ends here
