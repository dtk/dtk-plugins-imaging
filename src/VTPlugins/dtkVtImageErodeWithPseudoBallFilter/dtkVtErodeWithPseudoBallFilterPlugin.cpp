// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtErodeWithPseudoBallFilterPlugin.h"

#include "dtkVtErodeWithPseudoBallFilter.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkVtErodeWithPseudoBallFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtErodeWithPseudoBallFilterPlugin::initialize(void)
{
    dtkImaging::filters::erode::pluginFactory().record("dtkVtErodeWithPseudoBallFilter", dtkVtErodeWithPseudoBallFilterCreator);
}

void dtkVtErodeWithPseudoBallFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtErodeWithPseudoBallFilter)

//
// dtkVtErodeWithPseudoBallFilterPlugin.cpp ends here
