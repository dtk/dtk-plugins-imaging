#pragma once

#include <dtkCore>
#include <dtkAbstractWindowingFilter.h>

class dtkItkWindowingFilterPlugin: public dtkAbstractWindowingFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractWindowingFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkWindowingFilterPlugin" FILE "dtkItkWindowingFilterPlugin.json")

public:
     dtkItkWindowingFilterPlugin(void) {}
    ~dtkItkWindowingFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
