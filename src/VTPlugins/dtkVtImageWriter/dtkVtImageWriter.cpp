// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtImageWriter.h"
#include "dtkVtImageConverter.h"

#include <dtkLog>

#include <vt_image.h>
#include <vt_inrimage.h>

// /////////////////////////////////////////////////////////////////
// dtkVtImageWriterPrivate
// /////////////////////////////////////////////////////////////////

class dtkVtImageWriterPrivate
{
public:
    QString path;
    dtkImage *image;
};

// /////////////////////////////////////////////////////////////////
// dtkVtImageWriter
// /////////////////////////////////////////////////////////////////

dtkVtImageWriter::dtkVtImageWriter(void) : dtkImageWriter(), d(new dtkVtImageWriterPrivate)
{
    d->image = nullptr;
}

dtkVtImageWriter::~dtkVtImageWriter(void)
{
    d->image = nullptr;
    delete d;
}

void dtkVtImageWriter::setPath(const QString& path)
{
    d->path = path;
}

void dtkVtImageWriter::setImage(dtkImage *image)
{
    if (!image) {
        dtkWarn() << Q_FUNC_INFO << "Input image is null. Nothing is done";
        return;
    }
    d->image = image;
}

void dtkVtImageWriter::write(void)
{
    if (!d->image) {
        dtkWarn() << Q_FUNC_INFO << "No image to write. Nothing is done.";
        return;
    }

    if (d->path.isEmpty()) {
        dtkWarn() << Q_FUNC_INFO << "No path has been set. Nothing is done.";
        return;
    }

    vt_image myvtimage;
    VT_Image( &myvtimage );
    if (dtkVtImageConverter::convertToNative(d->image, &myvtimage) != EXIT_FAILURE) {
        char *str = const_cast<char *>(qPrintable(d->path));
        if ( VT_WriteInrimageWithName( &myvtimage, str ) != 1 ) {
            dtkError() << Q_FUNC_INFO << "Can not write image. Aborting.";
        }
        free( myvtimage.array );

    } else {
        dtkError() << Q_FUNC_INFO << "Conversion to VT format failed. Aborting.";
    }
}

//
// dtkVtImageWriter.cpp ends here
