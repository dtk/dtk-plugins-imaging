""" Converter from dtkImage to SpatialImage """

import ctypes
import numpy as np

import dtkimagingcore
from dtkimagingcore import dtkImage

try:
    from timagetk.components import SpatialImage
except ImportError:
    raise ImportError('Import Error')

__all__ = ['dtk_img_to_sp_img', 'sp_img_to_dtk_img']

def np_type_to_dtk_type(dtype):
    """
    Convert SpatialImage type to dtkImage pixel type
    """
    if dtype == np.uint8:
        _type = 37
    elif dtype == np.int8:
        _type = 34
    elif dtype == np.int16:
        _type = 33
    elif dtype == np.uint16:
        _type = 36
    elif dtype == np.uint32:
        _type = 3
    elif dtype == np.int32:
        _type = 2
    elif dtype == np.uint64:
        _type = 35
    elif dtype == np.int64:
        _type = 32
    elif dtype == np.float32:
        _type = 38
    elif dtype == np.float64:
        _type = 6
    else:
        _type = 0
    return _type

def dtk_type_to_c_type(dtk_type):
    """
    Convert dtkImage type to SpatialImage type
    """
    if dtk_type == 37:   # QMetaType::UChar
        return ctypes.c_ubyte
    elif dtk_type == 34: # QMetaType::Char
        return ctypes.c_byte
    elif dtk_type == 33: # QMetaType::Short
        return ctypes.c_short
    elif dtk_type == 36: # QMetaType::UShort
        return ctypes.c_ushort
    elif dtk_type ==  3: # QMetaType::UInt
        return ctypes.c_uint
    elif dtk_type ==  2: # QMetaType::Int
        return ctypes.c_int
    elif dtk_type == 35: # QMetaType::ULong
        return ctypes.c_ulong
    elif dtk_type == 32: # QMetaType::Long
        return ctypes.c_long
    elif dtk_type == 38: # QMetaType::Float
        return ctypes.c_float
    elif dtk_type ==  6: # QMetaType::Double
        return ctypes.c_double
    else:
        print("TYPE_UNKNOWN")
        return -1

def dtk_img_to_sp_img(dtk_img):
    """
    dtkImage to SpatialImage
    """
    dtype = dtk_type_to_c_type(dtk_img.storageType())
    x, y, z = dtk_img.xDim(), dtk_img.yDim(), dtk_img.zDim()
    size = x*y*z
    sp = dtk_img.spacing()
    ori = dtk_img.origin()

    _np_array = np.ravel(dtk_img.array())

    out_arr = np.array(_np_array.reshape((x, y, z), order="F"))

    out_sp_img = SpatialImage(out_arr, origin=ori, voxelsize=sp)
    if 1 in out_sp_img.shape: # 2D management
        out_sp_img = out_sp_img.to_2D()

    return out_sp_img


def sp_img_to_dtk_img(sp_img):
    """
    SpatialImage to dtkImage
    """
    if isinstance(sp_img, SpatialImage):

        if sp_img.get_dim()==2: # 2D management
            sp_img = sp_img.to_3D()

        _storage_type = np_type_to_dtk_type(sp_img.dtype)
        _x_dim, _y_dim, _z_dim = sp_img.shape

        _buf = sp_img.ctypes.data_as(ctypes.c_void_p)

        dtk_img = dtkImage.fromRaw(_storage_type, _x_dim, _y_dim, _z_dim, _buf.value, dtkimagingcore.Scalar)

        dtk_img.setOrigin(sp_img.origin)
        dtk_img.setSpacing(sp_img.voxelsize)

        return dtk_img

    else:
        print('Input is not a SpatialImage instance')
        return
