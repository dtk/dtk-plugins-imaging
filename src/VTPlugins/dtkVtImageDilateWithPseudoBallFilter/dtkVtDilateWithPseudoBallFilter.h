// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractDilateFilter.h>

class dtkImage;
class dtkVtDilateWithPseudoBallFilterPrivate;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtDilateWithPseudoBallFilter final : public dtkAbstractDilateFilter
{
public:
     dtkVtDilateWithPseudoBallFilter(void);
    ~dtkVtDilateWithPseudoBallFilter(void);

public:
    void setRadius(double radius);
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);

private:
    dtkVtDilateWithPseudoBallFilterPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkAbstractDilateFilter *dtkVtDilateWithPseudoBallFilterCreator(void)
{
    return new dtkVtDilateWithPseudoBallFilter();
}

//
// dtkVtDilateWithPseudoBallFilter.h ends here
