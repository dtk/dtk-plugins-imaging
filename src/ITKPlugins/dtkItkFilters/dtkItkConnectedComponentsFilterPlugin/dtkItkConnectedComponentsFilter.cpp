// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkConnectedComponentsFilter.h"

#include "dtkItkImageConverter.h"

#include <dtkImage.h>
#include <dtkFilterExecutor.h>

#include <dtkLog>

#include <itkImage.h>
#include "itkConnectedComponentImageFilter.h"

// ///////////////////////////////////////////////////////////////////
//  dtkItkConnectedComponentsFilterPrivate
// ///////////////////////////////////////////////////////////////////

class dtkItkConnectedComponentsFilterPrivate
{
public:
    dtkImage *image_in;
    dtkImage *image_out;

    double value;

public:
    template < typename ImgT, int dim > void exec(void);
};

// ///////////////////////////////////////////////////////////////////
//  dtkItkConnectedComponentsFilter
// ///////////////////////////////////////////////////////////////////

dtkItkConnectedComponentsFilter::dtkItkConnectedComponentsFilter(void): d(new dtkItkConnectedComponentsFilterPrivate)
{
    d->image_in = nullptr;
    d->image_out = new dtkImage();

    d->value = 0;
}

dtkItkConnectedComponentsFilter::~dtkItkConnectedComponentsFilter(void)
{
    delete d;
}

void dtkItkConnectedComponentsFilter::setValue(double value)
{
    d->value = value;
}

void dtkItkConnectedComponentsFilter::setImage(dtkImage *image)
{
    d->image_in = image;
}

dtkImage *dtkItkConnectedComponentsFilter::filteredImage(void) const
{
    return d->image_out;
}

void dtkItkConnectedComponentsFilter::run(void)
{
    if (!d->image_in) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkItkConnectedComponentsFilterPrivate>::run(d, d->image_in);
}

template < typename ImgT, int dim> inline void dtkItkConnectedComponentsFilterPrivate::exec(void)
{
    typedef dtkItkImageConverter<ImgT, dim> Converter;
    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType, dim> ImageType;

    typename ImageType::Pointer image = ImageType::New();

    Converter::convertToNative(this->image_in, image);

    if (image) {
        typedef itk::ConnectedComponentImageFilter<ImageType, itk::Image< unsigned char,dim > > ConnectedComponentImageFilterType;

        typename ConnectedComponentImageFilterType::Pointer filter = ConnectedComponentImageFilterType::New();
		filter->SetBackgroundValue(this->value);
		filter->SetInput(image);
		filter->Update();

        dtkItkImageConverter<dtkScalarPixel<unsigned char>,dim>::convertFromNative(filter->GetOutput(), this->image_out);
    }
}

//
// dtkItkConnectedComponentsFilter.cpp ends here
