#include "dtkItkErodeFilter.h"
#include "dtkItkErodeFilterPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkErodeFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkErodeFilterPlugin::initialize(void)
{
    dtkImaging::filters::erode::pluginFactory().record("itkErodeFilter", dtkItkErodeFilterCreator);
}

void dtkItkErodeFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkErodeFilter)

