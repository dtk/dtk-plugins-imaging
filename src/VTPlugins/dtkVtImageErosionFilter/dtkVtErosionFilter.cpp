// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtErosionFilter.h"

#include "dtkVtImageConverter.h"

#include <dtkFilterExecutor.h>

#include <vt_image.h>

#include <api-cellfilter.h>

// /////////////////////////////////////////////////////////////////
// dtkVtErosionFilterPrivate implementation
// /////////////////////////////////////////////////////////////////

class dtkVtErosionFilterPrivate
{
public:
    dtkImage *image_in;
    dtkImage *image_out;

    int radius;
    int iterations;

public:
    template < typename ImgT, int dim > void exec(void);
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkVtErosionFilter::dtkVtErosionFilter(void) : dtkAbstractErosionFilter(), d(new dtkVtErosionFilterPrivate)
{
    d->image_in = nullptr;
    d->image_out = new dtkImage();

    d->radius = 1;
    d->iterations = 1;
}

dtkVtErosionFilter::~dtkVtErosionFilter(void)
{
    delete d;
}

void dtkVtErosionFilter::setImage(dtkImage *image)
{
    d->image_in = image;
}

void dtkVtErosionFilter::setRadius(int radius)
{
    d->radius = radius;
}

void dtkVtErosionFilter::setIterationCount(int iteration_count)
{
    d->iterations = iteration_count;
}

dtkImage *dtkVtErosionFilter::filteredImage(void) const
{
    return d->image_out;
}

void dtkVtErosionFilter::run(void)
{
    if (!d->image_in) {
        dtkWarn() << Q_FUNC_INFO << "no image input or no label in input";
        return;
    }

    dtkFilterExecutor<dtkVtErosionFilterPrivate>::run(d, d->image_in);
}

template < typename ImgT, int dim> inline void dtkVtErosionFilterPrivate::exec(void)
{
    vt_image image;

    QString str = QString("-erosion -iterations %1 -radius %2")
        .arg(this->iterations)
        .arg(this->radius);

    if (dtkVtImageConverter::convertToNative(this->image_in, &image) != EXIT_FAILURE) {
        if ( API_cellfilter( &image, &image, nullptr, str.toLocal8Bit().data(), (char*)nullptr ) != 1 ) {
            free( image.array );
            image.array = nullptr;
            dtkError() << Q_FUNC_INFO << "Computation failed. Aborting.";
            return;
        }

        dtkVtImageConverter::convertFromNative(&image, this->image_out);

        free( image.array );
        image.array = nullptr;

    } else {
        dtkError() << Q_FUNC_INFO << "Conversion to VT format failed for input image. Aborting.";
    }
}

//
// dtkVtErosionFilter.cpp ends here
