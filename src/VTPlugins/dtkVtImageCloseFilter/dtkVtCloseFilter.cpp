// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtCloseFilter.h"
#include "dtkVtImageConverter.h"

#include <dtkFilterExecutor.h>

#include <vt_image.h>

#include <api-morpho.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtCloseFilterPrivate
{
public:
    dtkImage *image_in;
    dtkImage *image_out;

    double radius;

public:
    template < typename ImgT, int dim > void exec(void);
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkVtCloseFilter::dtkVtCloseFilter(void): dtkAbstractCloseFilter(), d(new dtkVtCloseFilterPrivate)
{
    d->image_in = nullptr;
    d->image_out = new dtkImage();

    d->radius = 0;
}

dtkVtCloseFilter::~dtkVtCloseFilter(void)
{
    delete d;
}

void dtkVtCloseFilter::setRadius(double radius)
{
    d->radius = radius;
}

void dtkVtCloseFilter::setImage(dtkImage *image)
{
    d->image_in = image;
}

dtkImage *dtkVtCloseFilter::filteredImage(void) const
{
    return d->image_out;
}

void dtkVtCloseFilter::run(void)
{
    if (!d->image_in) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkVtCloseFilterPrivate>::run(d, d->image_in);
}

template < typename ImgT, int dim> inline void dtkVtCloseFilterPrivate::exec(void)
{
    vt_image image;
    char str[100];
    sprintf(str, "-operation closing -R %d", (int)this->radius);

    if (dtkVtImageConverter::convertToNative(this->image_in, &image) != EXIT_FAILURE) {
        dtkError() << Q_FUNC_INFO << "Conversion to VT format failed. Aborting.";

    } else {
        if ( API_morpho( &image, &image, nullptr, str, nullptr) != 1 ) {
            dtkError() << Q_FUNC_INFO << "Computation failed. Aborting.";

        } else {
            dtkVtImageConverter::convertFromNative(&image, this->image_out);
        }
        free( image.array );
        image.array = NULL;
    }
}

//
// dtkVtCloseFilter.cpp ends here
