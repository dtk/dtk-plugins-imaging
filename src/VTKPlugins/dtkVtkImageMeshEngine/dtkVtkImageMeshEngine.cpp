// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtkImageMeshEngine.h"

#include <vtkPolyData.h>

// /////////////////////////////////////////////////////////////////
// dtkVtkImageMeshEnginePrivate
// /////////////////////////////////////////////////////////////////

struct dtkVtkImageMeshEnginePrivate
{
    vtkPolyData *poly_data;
};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

dtkVtkImageMeshEngine::dtkVtkImageMeshEngine(void) : d(new dtkVtkImageMeshEnginePrivate)
{
    d->poly_data = nullptr;
}

dtkVtkImageMeshEngine::dtkVtkImageMeshEngine(const dtkVtkImageMeshEngine& o) : d(new dtkVtkImageMeshEnginePrivate)
{
    if (o.d->poly_data){
        d->poly_data = vtkPolyData::New();
        d->poly_data->DeepCopy(o.d->poly_data);
    }
}

dtkVtkImageMeshEngine::~dtkVtkImageMeshEngine(void)
{
    if (d->poly_data) {
        d->poly_data->Delete();
    }
    delete d;
}

dtkVtkImageMeshEngine* dtkVtkImageMeshEngine::clone(void) const
{
    return new dtkVtkImageMeshEngine(*this);
}

void dtkVtkImageMeshEngine::setPolyData(vtkPolyData *poly_data)
{
    if (d->poly_data && d->poly_data != poly_data) {
        d->poly_data->Delete();
    }
    d->poly_data = poly_data;
}

vtkPolyData *dtkVtkImageMeshEngine::polyData(void) const
{
    return d->poly_data;
}

//
// dtkVtkImageMeshEngine.cpp ends here
