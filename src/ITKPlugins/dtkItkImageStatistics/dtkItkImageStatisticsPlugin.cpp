// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkImageStatistics.h"
#include "dtkItkImageStatisticsPlugin.h"

#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkImageStatisticsPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkImageStatisticsPlugin::initialize(void)
{
    dtkImaging::statistics::pluginFactory().record("dtkItkImageStatistics", dtkItkImageStatisticsCreator);
}

void dtkItkImageStatisticsPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkImageStatistics)

//
// dtkItkImageStatisticsPlugin.cpp ends here
