// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractAddFilter.h>

#include <QtCore>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkAddFilterPlugin: public dtkAbstractAddFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractAddFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkAddFilterPlugin" FILE "dtkItkAddFilterPlugin.json")

public:
     dtkItkAddFilterPlugin(void) {}
    ~dtkItkAddFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkItkAddFilterPlugin.h ends here
