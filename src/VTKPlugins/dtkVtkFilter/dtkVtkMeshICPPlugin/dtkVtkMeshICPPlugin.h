// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>
#include <dtkAbstractMeshICP.h>

class dtkVtkMeshICPPlugin : public dtkAbstractMeshICPPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractMeshICPPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtkMeshICPPlugin" FILE "dtkVtkMeshICPPlugin.json")

public:
     dtkVtkMeshICPPlugin(void) {}
    ~dtkVtkMeshICPPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkMeshICPPlugin.h ends here
