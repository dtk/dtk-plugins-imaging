#pragma once

#include <QObject>
#include <dtkAbstractSubtractFilter.h>

class dtkImage;
class dtkItkSubtractFilterPrivate;

class dtkItkSubtractFilter final : public dtkAbstractSubtractFilter
{
    

public:
     dtkItkSubtractFilter(void);
    ~dtkItkSubtractFilter(void);

public:
    void setValue(double value);
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);
    template < typename ImgT, int dim > void exec(void);

private:
    dtkItkSubtractFilterPrivate *d;
};


inline dtkAbstractSubtractFilter *dtkItkSubtractFilterCreator(void)
{
    return new dtkItkSubtractFilter();
}
