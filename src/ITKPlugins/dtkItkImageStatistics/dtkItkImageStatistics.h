// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractImageStatistics.h>

class dtkItkImageStatisticsPrivate;
class dtkImage;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkImageStatistics : public dtkAbstractImageStatistics
{
public:
     dtkItkImageStatistics(void);
    ~dtkItkImageStatistics(void);

public:
    void setImage(dtkImage *image) final;

public:
    double mean(void) const final;
    double sigma(void) const final;
    double variance(void) const final;
    double sum(void) const final;

    double min(void) const final;
    double max(void) const final;

public:
    void run();

private:
    dtkItkImageStatisticsPrivate* d;
};

// /////////////////////////////////////////////////////////////////

inline dtkAbstractImageStatistics *dtkItkImageStatisticsCreator(void)
{
    return new dtkItkImageStatistics();
}

//
// dtkItkImageStatistics.h ends here
