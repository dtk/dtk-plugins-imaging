#pragma once

#include <dtkCore>
#include <dtkAbstractShrinkFilter.h>

class dtkItkShrinkFilterPlugin: public dtkAbstractShrinkFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractShrinkFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkShrinkFilterPlugin" FILE "dtkItkShrinkFilterPlugin.json")

public:
     dtkItkShrinkFilterPlugin(void) {}
    ~dtkItkShrinkFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
