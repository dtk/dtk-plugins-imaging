// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkDilateFilterPlugin.h"

#include "dtkItkDilateFilter.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkDilateFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkDilateFilterPlugin::initialize(void)
{
    dtkImaging::filters::dilate::pluginFactory().record("dtkItkDilateFilter", dtkItkDilateFilterCreator);
}

void dtkItkDilateFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkDilateFilter)

//
// dtkItkDilateFilterPlugin.cpp ends here
