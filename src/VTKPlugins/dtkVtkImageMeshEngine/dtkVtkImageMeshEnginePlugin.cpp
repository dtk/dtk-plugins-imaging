// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtkImageMeshEngine.h"
#include "dtkVtkImageMeshEnginePlugin.h"
#include "dtkVtkMeshReader.h"
#include "dtkVtkMeshWriter.h"

#include <dtkCore>

// ///////////////////////////////////////////////////////////////////
// itkImageHandlerPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtkImageMeshEnginePlugin::initialize(void)
{
    dtkImaging::meshEngine::pluginFactory().record("dtkVtkImageMeshEngine", dtkVtkImageMeshEngineCreator);
    dtkImaging::meshReader::pluginFactory().record("dtkVtkMeshReader", dtkVtkMeshReaderCreator);
    dtkImaging::meshWriter::pluginFactory().record("dtkVtkMeshWriter", dtkVtkMeshWriterCreator);
}

void dtkVtkImageMeshEnginePlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtkImageMeshEngine)

//
// dtkVtkImageMeshEnginePlugin.cpp ends here
