#pragma once

#include <dtkCore>
#include <dtkAbstractErodeFilter.h>

class dtkVtErodeWithPseudoBallFilterPlugin: public dtkAbstractErodeFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractErodeFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtErodeWithPseudoBallFilterPlugin" FILE "dtkVtErodeWithPseudoBallFilterPlugin.json")

public:
     dtkVtErodeWithPseudoBallFilterPlugin(void) {}
    ~dtkVtErodeWithPseudoBallFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
