// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkImageStatistics.h"

#include "dtkItkImageConverter.h"

#include <dtkFilterExecutor.h>

#include <itkImage.h>
#include <itkStatisticsImageFilter.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkImageStatisticsPrivate
// ///////////////////////////////////////////////////////////////////
/// \brief The dtkItkImageStatisticsPrivate class
///
class dtkItkImageStatisticsPrivate
{
public:
    dtkImage *image;

    double mean;
    double sigma;
    double variance;
    double sum;

    double min;
    double max;

public:
    template < typename ImgT, int dim > void exec(void);
};

// ///////////////////////////////////////////////////////////////////
// dtkItkImageStatistics
// ///////////////////////////////////////////////////////////////////

dtkItkImageStatistics::dtkItkImageStatistics(void): dtkAbstractImageStatistics(), d(new dtkItkImageStatisticsPrivate)
{
    d->image = NULL;
    d->mean = -1;
    d->sigma = -1;
    d->variance = -1;
    d->sum = -1;

    d->min = -1;
    d->max = -1;
}

dtkItkImageStatistics::~dtkItkImageStatistics(void)
{
    delete d;
}

void dtkItkImageStatistics::setImage(dtkImage *image)
{
    d->image = image;
}

double dtkItkImageStatistics::mean(void) const
{
    return d->mean;
}

double dtkItkImageStatistics::sigma(void) const
{
    return d->sigma;
}

double dtkItkImageStatistics::variance(void) const
{
    return d->variance;
}

double dtkItkImageStatistics::sum(void) const
{
    return d->sum;
}

double dtkItkImageStatistics::min(void) const
{
    return d->min;
}

double dtkItkImageStatistics::max(void) const
{
    return d->max;
}

void dtkItkImageStatistics::run()
{
    dtkFilterExecutor<dtkItkImageStatisticsPrivate>::run(d, d->image);
}

template < typename ImgT, int dim> inline void dtkItkImageStatisticsPrivate::exec(void)
{
    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType, dim> ImageType;
    typedef typename ImageType::Pointer ImageTypePointer;
    typedef dtkItkImageConverter<ImgT, dim> Converter;

    ImageTypePointer img = ImageType::New();

    Converter::convertToNative(this->image, img);

    if (img) {
        typedef itk::StatisticsImageFilter<ImageType> StatsFilterType;
        typename StatsFilterType::Pointer filter = StatsFilterType::New();
        filter->SetInput(img);
        filter->Update();

        this->mean     = filter->GetMean();
        this->sigma    = filter->GetSigma();
        this->variance = filter->GetVariance();
        this->sum      = filter->GetSum();

        this->min = filter->GetMinimum();
        this->max = filter->GetMaximum();
    }
}

//
// dtkItkImageStatistics.cpp ends here
