#include "dtkVtGaussianFilter.h"
#include "dtkVtGaussianFilterPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkVtGaussianFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtGaussianFilterPlugin::initialize(void)
{
    dtkImaging::filters::gaussian::pluginFactory().record("dtkVtGaussianFilter", dtkVtGaussianFilterCreator);
}

void dtkVtGaussianFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtGaussianFilter)
