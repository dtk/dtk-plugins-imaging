// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractAddFilter.h>

class dtkImage;
class dtkItkAddFilterPrivate;

// /////////////////////////////////////////////////////////////////
// dtkItkAddFilter interface
// /////////////////////////////////////////////////////////////////

class dtkItkAddFilter final : public dtkAbstractAddFilter
{
public:
     dtkItkAddFilter(void);
    ~dtkItkAddFilter(void);

public:
    void setValue(double value);
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);

private:
    dtkItkAddFilterPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkAbstractAddFilter *dtkItkAddFilterCreator(void)
{
    return new dtkItkAddFilter();
}

//
// dtkItkAddFilter.h ends here
