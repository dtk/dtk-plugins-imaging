// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractDistanceMapFilter.h>

class dtkItkDistanceMapFilterPrivate;

// /////////////////////////////////////////////////////////////////
// dtkItkDistanceMapFilter
// /////////////////////////////////////////////////////////////////

class dtkItkDistanceMapFilter final : public dtkAbstractDistanceMapFilter
{
public:
     dtkItkDistanceMapFilter(void);
    ~dtkItkDistanceMapFilter(void);

public:
    void setInput(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);

private:
    dtkItkDistanceMapFilterPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkAbstractDistanceMapFilter *dtkItkDistanceMapFilterCreator(void)
{
    return new dtkItkDistanceMapFilter();
}

//
// dtkItkDistanceMapFilter.h ends here
