#include "dtkItkCloseFilter.h"
#include "dtkItkCloseFilterPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkCloseFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkCloseFilterPlugin::initialize(void)
{
    dtkImaging::filters::close::pluginFactory().record("itkCloseFilter", dtkItkCloseFilterCreator);
}

void dtkItkCloseFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkCloseFilter)

