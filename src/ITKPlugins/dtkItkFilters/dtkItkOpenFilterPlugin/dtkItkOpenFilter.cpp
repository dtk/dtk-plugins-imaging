#include "dtkItkOpenFilter.h"
#include "dtkItkImageConverter.h"

#include <dtkFilterExecutor.h>

#include <itkImage.h>
#include <itkGrayscaleMorphologicalOpeningImageFilter.h>
#include <itkBinaryBallStructuringElement.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkOpenFilterPrivate
{
public:
    dtkImage *m_imageIn;
    dtkImage *m_imageOut;

    double m_radius;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkItkOpenFilter::dtkItkOpenFilter(void): d(new dtkItkOpenFilterPrivate)
{
    d->m_imageIn=NULL;
    d->m_imageOut=new dtkImage();
}

dtkItkOpenFilter::~dtkItkOpenFilter(void)
{
    if(d)
        delete d;
}

void dtkItkOpenFilter::setRadius(double radius)
{
    d->m_radius=radius;
}

void dtkItkOpenFilter::setImage(dtkImage *image)
{
    d->m_imageIn=image;
}

dtkImage *dtkItkOpenFilter::filteredImage(void) const
{
    return d->m_imageOut;
}

void dtkItkOpenFilter::run(void)
{
    if (!d->m_imageIn) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkItkOpenFilter>::run(this, d->m_imageIn);
}

template < typename ImgT, int dim> inline void dtkItkOpenFilter::exec(void)
{
    qDebug()<< Q_FUNC_INFO;

    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType,dim> ImageType;
    typedef dtkItkImageConverter<ImgT,dim> Converter;

    
    typename ImageType::Pointer image=ImageType::New();
    Converter::convertToNative(d->m_imageIn, image);

    if(image)
    {

        typedef itk::BinaryBallStructuringElement < typename ImgT::PixelType, dim> StructuringElementType;
        typedef itk::GrayscaleMorphologicalOpeningImageFilter< ImageType, ImageType, StructuringElementType >  OpenType;
        typename OpenType::Pointer filter = OpenType::New();

        StructuringElementType ball;
        ball.SetRadius(d->m_radius);
        ball.CreateStructuringElement();
        filter->SetInput(image);
        filter->SetRadius(d->m_radius);
        filter->Update();
        Converter::convertFromNative(filter->GetOutput(), d->m_imageOut);
    }
}
