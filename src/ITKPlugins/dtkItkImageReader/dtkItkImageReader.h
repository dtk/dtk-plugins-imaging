// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImageReader.h>

class dtkItkImageReaderPrivate;

// /////////////////////////////////////////////////////////////////
// dtkItkImageReader interface
// /////////////////////////////////////////////////////////////////

class dtkItkImageReader : public dtkImageReader
{
public:
     dtkItkImageReader(void);
    ~dtkItkImageReader(void);

public:
    dtkImage *read(const QString& path) final;

public:
    QStringList types(void) final;

private:
    dtkItkImageReaderPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkImageReader *dtkItkImageReaderCreator(void)
{
    return new dtkItkImageReader();
}

// /////////////////////////////////////////////////////////////////
// Helper Macros
// /////////////////////////////////////////////////////////////////

#include <dtkPixelImpl.h>
#include <dtkItkImageConverter.h>

#define READ_IMAGE_MACRO(type, N, dtkPixel, itkPixel) {         \
        typedef itk::Image<itkPixel, N> ImageType;              \
        ImageType::Pointer image = ImageType::New();            \
        readFile<ImageType>(path, image);                       \
        typedef dtkItkImageConverter<dtkPixel, N> Converter;    \
        Converter::convertFromNative(image, target); }

#define DEFINE_PIXEL_TYPE(type, N)                                      \
    if (imageIO->GetPixelType() == itk::ImageIOBase::SCALAR) {          \
        READ_IMAGE_MACRO(type, N, dtkScalarPixel<type>, type);          \
                                                                        \
    } else if (imageIO->GetPixelType() == itk::ImageIOBase::RGB) {      \
        READ_IMAGE_MACRO(type, N, dtkRGBPixel<type>, itk::RGBPixel<type>); \
                                                                        \
    } else if( imageIO->GetPixelType() == itk::ImageIOBase::RGBA) {     \
        READ_IMAGE_MACRO(type, N, dtkRGBAPixel<type>, itk::RGBAPixel<type>); \
    }

//the following types are currently not handled

/*    else if (imageIO->GetPixelType() == itk::ImageIOBase::UNKNOWNPIXELTYPE) {\
        READ_IMAGE_MACRO(type, N, dtkScalarPixel);    \
    } else if (imageIO->GetPixelType() == itk::ImageIOBase::OFFSET) {\
        READ_IMAGE_MACRO(type, N, dtkScalarPixel);    \
    } else if (imageIO->GetPixelType() == itk::ImageIOBase::VECTOR) {\
        READ_IMAGE_MACRO(type, N, dtkScalarPixel);    \
    } else if (imageIO->GetPixelType() == itk::ImageIOBase::POINT) {\
        READ_IMAGE_MACRO(type, N, dtkScalarPixel);    \
    } else if (imageIO->GetPixelType() == itk::ImageIOBase::COVARIANTVECTOR) {\
        READ_IMAGE_MACRO(type, N, dtkScalarPixel);    \
    } else if (imageIO->GetPixelType() == itk::ImageIOBase::SYMMETRICSECONDRANKTENSOR) {\
        READ_IMAGE_MACRO(type, N, dtkScalarPixel);    \
    } else if (imageIO->GetPixelType() == itk::ImageIOBase::DIFFUSIONTENSOR3D) {\
        READ_IMAGE_MACRO(type, N, dtkScalarPixel);    \
    } else if (imageIO->GetPixelType() == itk::ImageIOBase::COMPLEX) {\
        READ_IMAGE_MACRO(type, N, dtkScalarPixel);    \
    } else if (imageIO->GetPixelType() == itk::ImageIOBase::FIXEDARRAY) {\
        READ_IMAGE_MACRO(type, N, dtkScalarPixel);    \
    } else if (imageIO->GetPixelType() == itk::ImageIOBase::MATRIX) {\
        READ_IMAGE_MACRO(type, N, dtkScalarPixel); \
    }
*/

#define READ_TYPED_IMAGE_MACRO(type) \
    if (imageIO->GetNumberOfDimensions() == 2) {        \
        DEFINE_PIXEL_TYPE(type, 2);                     \
                                                        \
    } else if (imageIO->GetNumberOfDimensions() == 3) { \
        DEFINE_PIXEL_TYPE (type, 3);                    \
                                                        \
    } else if (imageIO->GetNumberOfDimensions() == 4) { \
        DEFINE_PIXEL_TYPE (type, 4);                    \
    }

//
// dtkItkImageReader.h ends here
