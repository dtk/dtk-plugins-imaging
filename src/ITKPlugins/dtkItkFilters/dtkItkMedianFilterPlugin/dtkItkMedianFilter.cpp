#include "dtkItkMedianFilter.h"
#include "dtkItkImageConverter.h"

#include <dtkFilterExecutor.h>

#include <itkImage.h>
#include <itkMedianImageFilter.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkMedianFilterPrivate
{
    public:
        dtkImage *m_imageIn;
        dtkImage *m_imageOut;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkItkMedianFilter::dtkItkMedianFilter(void): d(new dtkItkMedianFilterPrivate)
{
    d->m_imageIn=NULL;
    d->m_imageOut=new dtkImage();
}

dtkItkMedianFilter::~dtkItkMedianFilter(void)
{
    if(d)
        delete d;
}

void dtkItkMedianFilter::setImage(dtkImage *image)
{
    d->m_imageIn=image;
}

dtkImage *dtkItkMedianFilter::filteredImage(void) const
{
    return d->m_imageOut;
}

void dtkItkMedianFilter::run(void)
{
    if (!d->m_imageIn) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkItkMedianFilter>::run(this, d->m_imageIn);
}

template < typename ImgT, int dim> inline void dtkItkMedianFilter::exec(void)
{
    qDebug()<< Q_FUNC_INFO;

    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType,dim> ImageType;
    typedef dtkItkImageConverter<ImgT,dim> Converter;
    typename ImageType::Pointer image=ImageType::New();
    Converter::convertToNative(d->m_imageIn,image);

    if(image)
    {
        typedef itk::MedianImageFilter <ImageType, ImageType> MedianImageFilterType;
        typename MedianImageFilterType::Pointer filter = MedianImageFilterType::New();
        filter->SetInput(image);
        filter->Update();
        Converter::convertFromNative(filter->GetOutput(),d->m_imageOut);
    }
}
