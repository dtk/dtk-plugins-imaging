// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImageMeshWriter.h>

class QString;
class dtkImageMesh;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtkMeshWriter : public dtkImageMeshWriter
{
public:
     dtkVtkMeshWriter(void);
    ~dtkVtkMeshWriter(void);

public:
    void setPath(const QString& path) override;
    void setMesh(dtkImageMesh *mesh) override;

public:
    void write(void) override;

private:
    class dtkVtkMeshWriterPrivate *d;
};

inline dtkImageMeshWriter *dtkVtkMeshWriterCreator(void)
{
    return new dtkVtkMeshWriter();
}

//
// dtkVtkMeshWriter.h ends here
