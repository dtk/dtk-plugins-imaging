# ChangeLog
## version 2.0.3 - 2021-07-30
 * add export headers for vtk plugins
 * update to vtk 9.0
## version 2.0.2 - 2019-09-26
 * fix conversion to spatial image (reshaping)
## version 2.0.1 - 2019-09-25
 * fix install rpath for plugin that depends on other plugins
## version 2.0.0 - 2019-09-24
 * use new dtk-imaging 2
 * ITK and VT are not yet ready
 * refactor TimageTk converter
## version 1.1.1 - 2018-11-09
 * update timagetk converter
