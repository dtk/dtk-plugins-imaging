// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkImageWriter.h>

// /////////////////////////////////////////////////////////////////
// dtkVtkMetaImageWriterPlugin interface
// /////////////////////////////////////////////////////////////////

class dtkVtkMetaImageWriterPlugin : public dtkImageWriterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkImageWriterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtkMetaImageWriterPlugin" FILE "dtkVtkMetaImageWriterPlugin.json")

public:
     dtkVtkMetaImageWriterPlugin(void) {}
    ~dtkVtkMetaImageWriterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkVtkMetaImageWriterPlugin.h ends here
