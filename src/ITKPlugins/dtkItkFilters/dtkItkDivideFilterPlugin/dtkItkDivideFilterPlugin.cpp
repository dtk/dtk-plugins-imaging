#include "dtkItkDivideFilter.h"
#include "dtkItkDivideFilterPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkDivideFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkDivideFilterPlugin::initialize(void)
{
    dtkImaging::filters::divide::pluginFactory().record("itkDivideFilter", dtkItkDivideFilterCreator);
}

void dtkItkDivideFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkDivideFilter)

