// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractDilateFilter.h>

class dtkImage;
class dtkItkDilateFilterPrivate;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkDilateFilter final : public dtkAbstractDilateFilter
{
public:
     dtkItkDilateFilter(void);
    ~dtkItkDilateFilter(void);

public:
    void setRadius(double radius) final;
    void setImage(dtkImage *image) final;

public:
    dtkImage *filteredImage(void) const final;

public:
    void run(void) final;

private:
    dtkItkDilateFilterPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkAbstractDilateFilter *dtkItkDilateFilterCreator(void)
{
    return new dtkItkDilateFilter();
}

//
// dtkItkDilateFilter.h ends here
