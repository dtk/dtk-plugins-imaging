// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtHTransformFilter.h"

#include "dtkVtImageConverter.h"

#include <dtkImage.h>
#include <dtkFilterExecutor.h>

#include <vt_image.h>

#include <api-regionalext.h>

// /////////////////////////////////////////////////////////////////
// dtkVtHTransformFilterPrivate implementation
// /////////////////////////////////////////////////////////////////

class dtkVtHTransformFilterPrivate
{
public:
    dtkImage *image_in;
    dtkImage *image_out;

    int hvalue;

public:
    template < typename ImgT, int dim > void exec(void);
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkVtHTransformFilter::dtkVtHTransformFilter(void) : dtkAbstractHTransformFilter(), d(new dtkVtHTransformFilterPrivate)
{
    d->image_in = nullptr;
    d->image_out = new dtkImage();

    d->hvalue = 0;
}

dtkVtHTransformFilter::~dtkVtHTransformFilter(void)
{
    delete d;
}

void dtkVtHTransformFilter::setImage(dtkImage *image)
{
    d->image_in = image;
}

void dtkVtHTransformFilter::setHValue(int h_value)
{
    d->hvalue = h_value;
}

dtkImage *dtkVtHTransformFilter::filteredImage(void) const
{
    return d->image_out;
}

void dtkVtHTransformFilter::run(void)
{
    if (!d->image_in) {
        dtkWarn() << Q_FUNC_INFO << "no image input or no label in input";
        return;
    }

    dtkFilterExecutor<dtkVtHTransformFilterPrivate>::run(d, d->image_in);
}

template < typename ImgT, int dim> inline void dtkVtHTransformFilterPrivate::exec(void)
{
    vt_image image;

    QString str = QString("-minima -h %1").arg(this->hvalue);

    if (dtkVtImageConverter::convertToNative(this->image_in, &image) != EXIT_FAILURE) {
        dtkError() << Q_FUNC_INFO << "Conversion to VT format failed. Aborting.";

    } else {
        if ( API_regionalext( &image, &image, str.toLocal8Bit().data(), nullptr ) != 1 ) {
            dtkError() << Q_FUNC_INFO << "Computation failed. Aborting.";

        } else {
            dtkVtImageConverter::convertFromNative(&image, this->image_out);
        }

        free( image.array );
        image.array = nullptr;
    }
}

//
// dtkVtHTransformFilter.cpp ends here
