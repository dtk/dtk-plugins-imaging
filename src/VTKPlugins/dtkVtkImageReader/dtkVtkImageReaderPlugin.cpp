// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtkImageReader.h"
#include "dtkVtkImageReaderPlugin.h"

#include <dtkCore>

#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// itkImageHandlerPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtkImageReaderPlugin::initialize(void)
{
    dtkImaging::reader::pluginFactory().record("dtkVtkImageReader", dtkVtkImageReaderCreator);
}

void dtkVtkImageReaderPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtkImageReader)

//
// itkImageHandlerPlugin.cpp ends here
