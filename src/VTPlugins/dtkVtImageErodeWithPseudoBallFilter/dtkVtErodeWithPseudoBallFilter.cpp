// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtErodeWithPseudoBallFilter.h"

#include "dtkVtImageConverter.h"

#include <dtkImage.h>
#include <dtkFilterExecutor.h>

#include <dtkLog>

#include <vt_image.h>

#include <api-morpho.h>

// /////////////////////////////////////////////////////////////////
// dtkVtErodeWithPseudoBallFilterPrivate
// /////////////////////////////////////////////////////////////////

class dtkVtErodeWithPseudoBallFilterPrivate
{
public:
    dtkImage *image_in;
    dtkImage *image_out;

    double radius;

public:
    template < typename ImgT, int dim > void exec(void);
};

// ///////////////////////////////////////////////////////////////////
// dtkVtErodeWithPseudoBallFilter
// ///////////////////////////////////////////////////////////////////

dtkVtErodeWithPseudoBallFilter::dtkVtErodeWithPseudoBallFilter(void): dtkAbstractErodeFilter(), d(new dtkVtErodeWithPseudoBallFilterPrivate)
{
    d->image_in = nullptr;
    d->image_out = new dtkImage();

    d->radius = 0;
}

dtkVtErodeWithPseudoBallFilter::~dtkVtErodeWithPseudoBallFilter(void)
{
    delete d;
}

void dtkVtErodeWithPseudoBallFilter::setRadius(double radius)
{
    d->radius = radius;
}

void dtkVtErodeWithPseudoBallFilter::setImage(dtkImage *image)
{
    d->image_in = image;
}

dtkImage *dtkVtErodeWithPseudoBallFilter::filteredImage(void) const
{
    return d->image_out;
}

void dtkVtErodeWithPseudoBallFilter::run(void)
{
    if (!d->image_in) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkVtErodeWithPseudoBallFilterPrivate>::run(d, d->image_in);
}

template < typename ImgT, int dim> inline void dtkVtErodeWithPseudoBallFilterPrivate::exec(void)
{
    vt_image image;
    char str[100];

    sprintf( str, "-ero -R %d", (int)(radius + 0.5) );

    if (dtkVtImageConverter::convertToNative(this->image_in, &image) != EXIT_FAILURE) {
        if ( API_morpho( &image, &image, nullptr, str, (char*)nullptr ) != 1 ) {
            free( image.array );
            dtkError() << Q_FUNC_INFO << "Computation failed. Aborting.";
            return;
        }

        dtkVtImageConverter::convertFromNative(&image, this->image_out);

        free( image.array );
        image.array = nullptr;

    } else {
        dtkError() << Q_FUNC_INFO << "Conversion to VT format failed. Aborting.";
    }
}

//
// dtkVtErodeWithPseudoBallFilter.cpp ends here
