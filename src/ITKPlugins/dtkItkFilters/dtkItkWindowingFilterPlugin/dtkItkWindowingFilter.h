#pragma once

#include <QObject>
#include <dtkAbstractWindowingFilter.h>

class dtkImage;
class dtkItkWindowingFilterPrivate;

class dtkItkWindowingFilter final : public dtkAbstractWindowingFilter
{
    

public:
     dtkItkWindowingFilter(void);
    ~dtkItkWindowingFilter(void);

public:
    void setImage(dtkImage *image);

     void setMinimumIntensityValue(double value);
     void setMaximumIntensityValue(double value);
     void setMinimumOutputIntensityValue(double value);
     void setMaximumOutputIntensityValue(double value);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);
    template < typename ImgT, int dim > void exec(void);

private:
    dtkItkWindowingFilterPrivate *d;
};


inline dtkAbstractWindowingFilter *dtkItkWindowingFilterCreator(void)
{
    return new dtkItkWindowingFilter();
}
