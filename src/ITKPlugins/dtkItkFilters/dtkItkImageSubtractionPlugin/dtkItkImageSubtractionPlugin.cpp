#include "dtkItkImageSubtraction.h"
#include "dtkItkImageSubtractionPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkImageSubtractionPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkImageSubtractionPlugin::initialize(void)
{
    dtkImaging::filters::imageSubtraction::pluginFactory().record("dtkItkImageSubtraction", dtkItkImageSubtractionCreator);
}

void dtkItkImageSubtractionPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkImageSubtraction)

