// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

class dtkImage;
class dtkImageData;
struct vt_image;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtImageConverter
{
public:
    static dtkImageData *createDtkImageData(int dim, int vt_pixel_type, const dtkArray<qlonglong>& extent, void *raw_data);

    static int convertToNative(dtkImage *source, vt_image *target);
    static int convertFromNative(vt_image *source, dtkImage *target);
};

//
