// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractConnectivityThreshold.h>

#include <QtCore>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkConnectivityThresholdPlugin: public dtkAbstractConnectivityThresholdPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractConnectivityThresholdPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkConnectivityThresholdPlugin" FILE "dtkItkConnectivityThresholdPlugin.json")

public:
     dtkItkConnectivityThresholdPlugin(void) {}
    ~dtkItkConnectivityThresholdPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkItkConnectivityThresholdPlugin.h ends here
