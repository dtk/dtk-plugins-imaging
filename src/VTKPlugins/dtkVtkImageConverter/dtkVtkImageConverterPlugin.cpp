// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtkImageConverter.h"
#include "dtkVtkImageConverterPlugin.h"

#include <dtkCore>

// ///////////////////////////////////////////////////////////////////
// itkImageHandlerPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtkImageConverterPlugin::initialize(void)
{
    dtkImaging::converter::pluginFactory().record("dtkVtkImageConverter", dtkVtkImageConverterCreator);
}

void dtkVtkImageConverterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtkImageConverter)

//
// dtkVtkImageConverterPlugin.cpp ends here
