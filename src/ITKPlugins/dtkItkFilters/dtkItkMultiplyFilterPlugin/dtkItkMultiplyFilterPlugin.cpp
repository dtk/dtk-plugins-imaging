#include "dtkItkMultiplyFilter.h"
#include "dtkItkMultiplyFilterPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkMultiplyFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkMultiplyFilterPlugin::initialize(void)
{
    dtkImaging::filters::multiply::pluginFactory().record("itkMultiplyFilter", dtkItkMultiplyFilterCreator);
}

void dtkItkMultiplyFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkMultiplyFilter)

