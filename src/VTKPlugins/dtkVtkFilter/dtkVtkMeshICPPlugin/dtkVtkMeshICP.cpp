#include "dtkVtkMeshICP.h"

#include <dtkImage.h>
#include <dtkImageMesh.h>
#include <dtkFilterExecutor.h>

#include <type_traits>

#include <dtkVtkImageMeshEngine.h>
#include <dtkVtkImageConverter.h>

#include <vtkPolyData.h>
#include <vtkMatrix4x4.h>
#include <vtkIterativeClosestPointTransform.h>

#include <vtkLandmarkTransform.h>
#include <vtkTransformPolyDataFilter.h>

class dtkVtkMeshICPPrivate
{
public:
    dtkImageMesh *m_movingMesh;
    dtkImageMesh *m_fixedMesh;
    int m_mode;

    dtkImageMesh *m_resMesh;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkVtkMeshICP::dtkVtkMeshICP(void) : d(new dtkVtkMeshICPPrivate)
{
    d->m_movingMesh=nullptr;
    d->m_fixedMesh=nullptr;
    d->m_mode=0;

    d->m_resMesh=new dtkImageMesh();
}

dtkVtkMeshICP::~dtkVtkMeshICP(void)
{
    delete d;
}

void dtkVtkMeshICP::setMovingMesh(dtkImageMesh *movingMesh)
{
    d->m_movingMesh = movingMesh;
}

void dtkVtkMeshICP::setFixedMesh(dtkImageMesh *fixedMesh)
{
    d->m_fixedMesh = fixedMesh;
}

void dtkVtkMeshICP::setMode(int mode)
{
    d->m_mode = mode;
}

dtkImageMesh* dtkVtkMeshICP::resMesh(void) const
{
    return d->m_resMesh;
}


void dtkVtkMeshICP::run(void)
{
    if (!d->m_movingMesh || !d->m_fixedMesh) {
        dtkWarn() << Q_FUNC_INFO << "Missing input mesh";
        return;
    }

//    dtkFilterExecutor<dtkVtkMeshICP>::run(this, d->m_movingMesh);

//template < typename ImgT, int dim> inline void dtkVtkMeshICP::exec(void)

    dtkVtkImageMeshEngine* movingMesh=dynamic_cast<dtkVtkImageMeshEngine*>(d->m_movingMesh->engine());
    dtkVtkImageMeshEngine* fixedMesh=dynamic_cast<dtkVtkImageMeshEngine*>(d->m_fixedMesh->engine());

    if(movingMesh && fixedMesh) {

	vtkPolyData *movingPolyData = movingMesh->polyData();
	vtkPolyData *fixedPolyData = fixedMesh->polyData();

        // for now, we set the parameters
        double ScaleFactor=1;
        int MaxNumIterations=50;
        int MaxNumLandmarks=200;
        double MaxMeanDistance=0.01;

	vtkIterativeClosestPointTransform * transformICP = vtkIterativeClosestPointTransform::New();
	transformICP->SetSource(movingPolyData);
	transformICP->SetTarget(fixedPolyData);

	transformICP->StartByMatchingCentroidsOn();
	transformICP->SetMaximumNumberOfLandmarks(MaxNumLandmarks);
	transformICP->SetMaximumNumberOfIterations(MaxNumIterations);
	transformICP->SetMaximumMeanDistance(MaxMeanDistance);

	switch (d->m_mode)
	{
	case 0:
	transformICP->GetLandmarkTransform()->SetMode(VTK_LANDMARK_RIGIDBODY);
	break;
	case 1:
	transformICP->GetLandmarkTransform()->SetMode(VTK_LANDMARK_SIMILARITY);
	break;
	case 2:
	transformICP->GetLandmarkTransform()->SetMode(VTK_LANDMARK_AFFINE);
	break;
	default:
	break;
	}

	transformICP->Modified();
	transformICP->Update();

	vtkSmartPointer<vtkMatrix4x4> m = transformICP->GetMatrix();
    static int index=0;
    index++;
    QFile file("out"+QString::number(index)+".txt");
    if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream outStream(&file);
        for(int i=0;i<16;i++)
            outStream<<m->GetElement(i%4,i/4);
    }

	//bring the source to the target
	vtkTransformPolyDataFilter *TransformFilter = vtkTransformPolyDataFilter::New();
	TransformFilter->SetInputData(movingPolyData);
	TransformFilter->SetTransform(transformICP);
	TransformFilter->Update();



	vtkPolyData *resPolyData = TransformFilter->GetOutput();

        dtkVtkImageMeshEngine *ve=new dtkVtkImageMeshEngine();
        ve->setPolyData(resPolyData);
        d->m_resMesh=new dtkImageMesh(ve);
    }
}
