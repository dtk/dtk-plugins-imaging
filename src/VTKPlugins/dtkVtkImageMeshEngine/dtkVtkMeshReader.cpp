#include <QtCore>

#include <dtkImageMeshReader.h>

#include <vtkPolyData.h>
#include <vtkGenericDataObjectReader.h>

#include "dtkVtkMeshReader.h"
#include "dtkVtkImageMeshEngine.h"

// /////////////////////////////////////////////////////////////////

dtkVtkMeshReader::dtkVtkMeshReader(void) : dtkImageMeshReader()
{

}

dtkVtkMeshReader::~dtkVtkMeshReader(void)
{

}

dtkImageMesh* dtkVtkMeshReader::read(const QString &path)
{ 
    vtkGenericDataObjectReader* reader = vtkGenericDataObjectReader::New();
    reader->SetFileName(path.toStdString().c_str());
    reader->Update();
 
    if(reader->IsFilePolyData()) {
        vtkPolyData* output = reader->GetPolyDataOutput();
        dtkVtkImageMeshEngine* engine=new dtkVtkImageMeshEngine();
        engine->setPolyData(output);
        dtkImageMesh* imageMesh=new dtkImageMesh(engine);
        return imageMesh;
    }
    return nullptr;
}
