#pragma once

#include <dtkCore>
#include <dtkAbstractErosionFilter.h>

class dtkVtErosionFilterPlugin: public dtkAbstractErosionFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractErosionFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtErosionFilterPlugin" FILE "dtkVtErosionFilterPlugin.json")

public:
     dtkVtErosionFilterPlugin(void) {}
    ~dtkVtErosionFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
