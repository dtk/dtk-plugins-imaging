// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkAbstractThresholdFilter.h>

class dtkImage;
class dtkItkThresholdFilterPrivate;

// /////////////////////////////////////////////////////////////////
// dtkItkThresholdFilter
// /////////////////////////////////////////////////////////////////

class dtkItkThresholdFilter final : public dtkAbstractThresholdFilter
{
public:
     dtkItkThresholdFilter(void);
    ~dtkItkThresholdFilter(void);

public:
    void setThreshold(double threshold);
    void setOutsideValue(double outsideValue);
    void setMode(bool mode);
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);
    template < typename ImgT, int dim > void exec(void);

private:
    dtkItkThresholdFilterPrivate *d;
};

// /////////////////////////////////////////////////////////////////

inline dtkAbstractThresholdFilter *dtkItkThresholdFilterCreator(void)
{
    return new dtkItkThresholdFilter();
}

//
// dtkItkThresholdFilter.h ends here
