#include "dtkItkMedianFilter.h"
#include "dtkItkMedianFilterPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkMedianFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkMedianFilterPlugin::initialize(void)
{
    dtkImaging::filters::median::pluginFactory().record("itkMedianFilter", dtkItkMedianFilterCreator);
}

void dtkItkMedianFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkMedianFilter)

