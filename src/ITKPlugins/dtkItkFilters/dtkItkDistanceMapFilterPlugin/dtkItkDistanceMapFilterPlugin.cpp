#include "dtkItkDistanceMapFilter.h"
#include "dtkItkDistanceMapFilterPlugin.h"

#include <dtkCore>
#include "dtkImaging.h"

// ///////////////////////////////////////////////////////////////////
// dtkItkDistanceMapFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkDistanceMapFilterPlugin::initialize(void)
{
    dtkImaging::filters::distanceMap::pluginFactory().record("dtkItkDistanceMapFilter", dtkItkDistanceMapFilterCreator);
}

void dtkItkDistanceMapFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkDistanceMapFilter)

