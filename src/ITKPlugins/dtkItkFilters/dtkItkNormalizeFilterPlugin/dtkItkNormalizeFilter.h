#pragma once

#include <QObject>
#include <dtkAbstractNormalizeFilter.h>

class dtkImage;
class dtkItkNormalizeFilterPrivate;

class dtkItkNormalizeFilter final : public dtkAbstractNormalizeFilter
{
    

public:
     dtkItkNormalizeFilter(void);
    ~dtkItkNormalizeFilter(void);

public:
    void setImage(dtkImage *image);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);
    template < typename ImgT, int dim > void exec(void);

private:
    dtkItkNormalizeFilterPrivate *d;
};


inline dtkAbstractNormalizeFilter *dtkItkNormalizeFilterCreator(void)
{
    return new dtkItkNormalizeFilter();
}
