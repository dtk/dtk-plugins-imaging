#include "dtkItkShrinkFilter.h"
#include "dtkItkImageConverter.h"

#include <dtkFilterExecutor.h>

#include <itkImage.h>
#include <itkShrinkImageFilter.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkItkShrinkFilterPrivate
{
public:
    dtkImage *m_imageIn;
    dtkImage *m_imageOut;

    double m_shrinkFactors[3];

};
// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkItkShrinkFilter::dtkItkShrinkFilter(void): d(new dtkItkShrinkFilterPrivate)
{
    d->m_imageIn=NULL;
    d->m_imageOut=new dtkImage();

    d->m_shrinkFactors[0]=1;
    d->m_shrinkFactors[1]=1;
    d->m_shrinkFactors[2]=1;
}

dtkItkShrinkFilter::~dtkItkShrinkFilter(void)
{
    if(d)
        delete d;
}

void dtkItkShrinkFilter::setXShrink(double xShrink)
{
    d->m_shrinkFactors[0]=xShrink;
}

void dtkItkShrinkFilter::setYShrink(double yShrink)
{
    d->m_shrinkFactors[0]=yShrink;
}

void dtkItkShrinkFilter::setZShrink(double zShrink)
{
    d->m_shrinkFactors[0]=zShrink;
}

void dtkItkShrinkFilter::setImage(dtkImage *image)
{
    d->m_imageIn=image;
}

dtkImage *dtkItkShrinkFilter::filteredImage(void) const
{
    return d->m_imageOut;
}

void dtkItkShrinkFilter::run(void)
{
    if (!d->m_imageIn) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkItkShrinkFilter>::run(this, d->m_imageIn);
}

template < typename ImgT, int dim> inline void dtkItkShrinkFilter::exec(void)
{
    qDebug()<< Q_FUNC_INFO;

    typedef itk::Image<typename dtkItkPixelTypeTrait<ImgT>::itkPixelType,dim> ImageType;
    typedef dtkItkImageConverter<ImgT,dim> Converter;
    typename ImageType::Pointer image=ImageType::New();
    Converter::convertToNative(d->m_imageIn, image);

    if(image)
    {
        typedef itk::ShrinkImageFilter <ImageType, ImageType> ShrinkImageFilterType;
        typename ShrinkImageFilterType::Pointer filter = ShrinkImageFilterType::New();
        filter->SetInput(image);
        filter->SetShrinkFactors(d->m_shrinkFactors);
        filter->Update();
        Converter::convertFromNative(filter->GetOutput(),d->m_imageOut);
    }
}
