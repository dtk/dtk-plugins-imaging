// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:
#pragma once

#include <QObject>

#include "itkImage.h"

#include <dtkItkPixelTypeTrait.h>

class dtkImage;

// /////////////////////////////////////////////////////////////////
// dtkPixelClassDataExtractor
// /////////////////////////////////////////////////////////////////

template < typename Pixel, int dim > class dtkPixelClassDataExtractor
{
public:
	static typename Pixel::PixelType *getData(typename itk::Image<typename dtkItkPixelTypeTrait<Pixel>::itkPixelType, dim>::Pointer image)
	{
        return (typename Pixel::PixelType *)(image->GetBufferPointer()->GetDataPointer());
	}
};

// /////////////////////////////////////////////////////////////////
// dtkPixelRawDataExtractor
// /////////////////////////////////////////////////////////////////

template < typename Pixel, int dim > class dtkPixelRawDataExtractor
{
public:
	static typename Pixel::PixelType *getData(typename itk::Image<typename dtkItkPixelTypeTrait<Pixel>::itkPixelType, dim>::Pointer image)
	{
        return (typename Pixel::PixelType *)(image->GetBufferPointer());
	}
};

// /////////////////////////////////////////////////////////////////
// dtkItkImageConverterHandler
// /////////////////////////////////////////////////////////////////

template < typename Pixel, int dim > class dtkItkImageConverterHandler
{
    typedef itk::Image<typename dtkItkPixelTypeTrait<Pixel>::itkPixelType, dim> ItkImageType;
    typedef typename ItkImageType::Pointer                                      ItkImagePointer;

public:
    static void convertToNative  (dtkImage* source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage* target);
};

// /////////////////////////////////////////////////////////////////
// dtkItkImageConverter
// /////////////////////////////////////////////////////////////////

template < typename Pixel, int dim > class dtkItkImageConverter
{
    typedef dtkItkImageConverterHandler<Pixel, dim> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait<Pixel>::itkPixelType, dim> ItkImageType;
    typedef typename ItkImageType::Pointer                                      ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////
// 4D converter specializations
// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned char>, 4>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned char>, 4> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned char> >::itkPixelType, 4> ItkImageType;
    typedef typename ItkImageType::Pointer                                                              ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<char>, 4>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<char>, 4> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<char> >::itkPixelType, 4> ItkImageType;
    typedef typename ItkImageType::Pointer                                                     ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned short>, 4>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned short>, 4> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned short> >::itkPixelType, 4> ItkImageType;
    typedef typename ItkImageType::Pointer                                                               ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<short>, 4>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<short>, 4> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<short> >::itkPixelType, 4> ItkImageType;
    typedef typename ItkImageType::Pointer                                                      ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned int>, 4>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned int>, 4> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned int> >::itkPixelType, 4> ItkImageType;
    typedef typename ItkImageType::Pointer                                                             ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<int>, 4>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<int>, 4> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<int> >::itkPixelType, 4> ItkImageType;
    typedef typename ItkImageType::Pointer                                                    ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned long>, 4>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned long>, 4> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned long> >::itkPixelType, 4> ItkImageType;
    typedef typename ItkImageType::Pointer                                                              ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<long>, 4>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<long>, 4> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<long> >::itkPixelType, 4> ItkImageType;
    typedef typename ItkImageType::Pointer                                                     ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<float>, 4>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<float>, 4> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<float> >::itkPixelType, 4> ItkImageType;
    typedef typename ItkImageType::Pointer                                                      ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<double>, 4>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<double>, 4> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<double> >::itkPixelType, 4> ItkImageType;
    typedef typename ItkImageType::Pointer                                                       ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////
// 3D converter specializations
// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned char>, 3>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned char>, 3> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned char> >::itkPixelType, 3> ItkImageType;
    typedef typename ItkImageType::Pointer                                                              ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<char>, 3>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<char>, 3> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<char> >::itkPixelType, 3> ItkImageType;
    typedef typename ItkImageType::Pointer                                                     ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned short>, 3>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned short>, 3> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned short> >::itkPixelType, 3> ItkImageType;
    typedef typename ItkImageType::Pointer                                                               ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<short>, 3>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<short>, 3> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<short> >::itkPixelType, 3> ItkImageType;
    typedef typename ItkImageType::Pointer                                                      ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned int>, 3>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned int>, 3> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned int> >::itkPixelType, 3> ItkImageType;
    typedef typename ItkImageType::Pointer                                                             ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<int>, 3>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<int>, 3> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<int> >::itkPixelType, 3> ItkImageType;
    typedef typename ItkImageType::Pointer                                                    ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned long>, 3>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned long>, 3> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned long> >::itkPixelType, 3> ItkImageType;
    typedef typename ItkImageType::Pointer                                                              ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<long>, 3>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<long>, 3> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<long> >::itkPixelType, 3> ItkImageType;
    typedef typename ItkImageType::Pointer                                                     ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<float>, 3>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<float>, 3> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<float> >::itkPixelType, 3> ItkImageType;
    typedef typename ItkImageType::Pointer                                                      ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<double>, 3>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<double>, 3> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<double> >::itkPixelType, 3> ItkImageType;
    typedef typename ItkImageType::Pointer                                                       ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////
// 2D specializations
// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned char>, 2>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned char>, 2> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned char> >::itkPixelType, 2> ItkImageType;
    typedef typename ItkImageType::Pointer                                                              ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<char>, 2>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<char>, 2> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<char> >::itkPixelType, 2> ItkImageType;
    typedef typename ItkImageType::Pointer                                                     ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned short>, 2>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned short>, 2> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned short> >::itkPixelType, 2> ItkImageType;
    typedef typename ItkImageType::Pointer                                                               ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<short>, 2>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<short>, 2> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<short> >::itkPixelType, 2> ItkImageType;
    typedef typename ItkImageType::Pointer                                                      ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned int>, 2>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned int>, 2> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned int> >::itkPixelType, 2> ItkImageType;
    typedef typename ItkImageType::Pointer                                                             ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<int>, 2>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<int>, 2> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<int> >::itkPixelType, 2> ItkImageType;
    typedef typename ItkImageType::Pointer                                                    ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned long>, 2>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned long>, 2> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned long> >::itkPixelType, 2> ItkImageType;
    typedef typename ItkImageType::Pointer                                                              ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<long>, 2>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<long>, 2> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<long> >::itkPixelType, 2> ItkImageType;
    typedef typename ItkImageType::Pointer                                                     ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<float>, 2>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<float>, 2> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<float> >::itkPixelType, 2> ItkImageType;
    typedef typename ItkImageType::Pointer                                                      ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<double>, 2>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<double>, 2> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<double> >::itkPixelType, 2> ItkImageType;
    typedef typename ItkImageType::Pointer                                                       ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};



// /////////////////////////////////////////////////////////////////
// 1D specializations
// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned char>, 1>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned char>, 1> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned char> >::itkPixelType, 1> ItkImageType;
    typedef typename ItkImageType::Pointer                                                              ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<char>, 1>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<char>, 1> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<char> >::itkPixelType, 1> ItkImageType;
    typedef typename ItkImageType::Pointer                                                     ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned short>, 1>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned short>, 1> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned short> >::itkPixelType, 1> ItkImageType;
    typedef typename ItkImageType::Pointer                                                               ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<short>, 1>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<short>, 1> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<short> >::itkPixelType, 1> ItkImageType;
    typedef typename ItkImageType::Pointer                                                      ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned int>, 1>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned int>, 1> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned int> >::itkPixelType, 1> ItkImageType;
    typedef typename ItkImageType::Pointer                                                             ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<int>, 1>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<int>, 1> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<int> >::itkPixelType, 1> ItkImageType;
    typedef typename ItkImageType::Pointer                                                    ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<unsigned long>, 1>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<unsigned long>, 1> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<unsigned long> >::itkPixelType, 1> ItkImageType;
    typedef typename ItkImageType::Pointer                                                              ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<long>, 1>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<long>, 1> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<long> >::itkPixelType, 1> ItkImageType;
    typedef typename ItkImageType::Pointer                                                     ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<float>, 1>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<float>, 1> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<float> >::itkPixelType, 1> ItkImageType;
    typedef typename ItkImageType::Pointer                                                      ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// /////////////////////////////////////////////////////////////////

template <> class dtkItkImageConverter<dtkScalarPixel<double>, 1>
{
    typedef dtkItkImageConverterHandler<dtkScalarPixel<double>, 1> Handler;

public:
    typedef itk::Image<typename dtkItkPixelTypeTrait< dtkScalarPixel<double> >::itkPixelType, 1> ItkImageType;
    typedef typename ItkImageType::Pointer                                                       ItkImagePointer;

    static void convertToNative  (dtkImage *source, ItkImagePointer target);
    static void convertFromNative(ItkImagePointer source, dtkImage *target);
};

// ///////////////////////////////////////////////////////////////

#include "dtkItkImageConverter.tpp"

//
