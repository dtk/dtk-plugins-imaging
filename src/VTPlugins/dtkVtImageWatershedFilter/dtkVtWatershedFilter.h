// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractWatershedFilter>

class dtkImage;

// /////////////////////////////////////////////////////////////////
// dtkVtWatershedFilter definition
// /////////////////////////////////////////////////////////////////

class dtkVtWatershedFilter final : public dtkAbstractWatershedFilter
{
public:
     dtkVtWatershedFilter(void);
    ~dtkVtWatershedFilter(void);

public:
    void setImage(dtkImage *image);
    void setSeed(dtkImage *seed);

    void setControlParameter(const QString& control_parameter);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);

private:
    class dtkVtWatershedFilterPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkAbstractWatershedFilter *dtkVtWatershedFilterCreator(void)
{
    return new dtkVtWatershedFilter();
}

//
// dtkVtWatershedFilter.h ends here
