// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtkImageConverter.h"

#include <QtCore>

#include <dtkImagingCore>

#include <vtkImageData.h>
#include <vtkImageImport.h>
#include <vtkSmartPointer.h>

class dtkVtkImageConverterPrivate
{
public:
    dtkImage *input;

public:
    vtkImageData *output;
    static int storageTypeToVtkType(int);
};


int dtkVtkImageConverterPrivate::storageTypeToVtkType(int storage_type)
{
    switch (storage_type) {
    case QMetaType::Void:
        return VTK_VOID;

    case QMetaType::Bool:
        return VTK_BIT;

    case QMetaType::Char:
        return VTK_CHAR;

    case QMetaType::UChar:
        return VTK_UNSIGNED_CHAR;

    case QMetaType::Short:
        return VTK_SHORT;

    case QMetaType::UShort:
        return VTK_UNSIGNED_SHORT;

    case QMetaType::Int:
        return VTK_INT;

    case QMetaType::UInt:
        return VTK_UNSIGNED_INT;

    case QMetaType::Long:
        return VTK_LONG;

    case QMetaType::ULong:
        return VTK_UNSIGNED_LONG;

    case QMetaType::Float:
        return VTK_FLOAT;

    case QMetaType::Double:
        return VTK_DOUBLE;

    default:
        return VTK_VOID;
    }
}


dtkVtkImageConverter::dtkVtkImageConverter(void)
{
    d = new dtkVtkImageConverterPrivate;
    d->input = nullptr;
    d->output = nullptr;
}

dtkVtkImageConverter::~dtkVtkImageConverter(void)
{
    delete d;
}

void dtkVtkImageConverter::setInput(dtkImage *input)
{
    d->input = input;
}

void *dtkVtkImageConverter::output(void)
{
    return d->output;
}

int dtkVtkImageConverter::convert(void)
{
    if(!d->input)
        return 0;

    if(!d->output)
        d->output = vtkImageData::New();

    this->convertToNative(d->input, d->output);

    return 1;
}

void dtkVtkImageConverter::convertFromNative(vtkImageData *source, dtkImage *target)
{
    int *ext = source->GetExtent();
    int dim = 3;
    if (ext[4] == 0 && ext[5] == 0) {
        dim = 2;
    }

    int vtk_pixel_type = source->GetScalarType();
    using PixelType = dtk::imaging::PixelType;

    if (dim == 2) {

        switch (vtk_pixel_type) {
        case VTK_CHAR:
        case VTK_SIGNED_CHAR:
            *target = dtkImage(ext[1]+1, ext[3]+1, 1, PixelType::Scalar, reinterpret_cast<char *>(source->GetScalarPointer()));
            break;
        case VTK_UNSIGNED_CHAR:
            *target = dtkImage(ext[1]+1, ext[3]+1, 1, PixelType::Scalar, reinterpret_cast<unsigned char *>(source->GetScalarPointer()));
            break;
        case VTK_SHORT:
            *target = dtkImage(ext[1]+1, ext[3]+1, 1, PixelType::Scalar, reinterpret_cast<short *>(source->GetScalarPointer()));
            break;
        case VTK_UNSIGNED_SHORT:
            *target = dtkImage(ext[1]+1, ext[3]+1, 1, PixelType::Scalar, reinterpret_cast<unsigned short *>(source->GetScalarPointer()));
            break;
        case VTK_INT:
            *target = dtkImage(ext[1]+1, ext[3]+1, 1, PixelType::Scalar, reinterpret_cast<int *>(source->GetScalarPointer()));
            break;
        case VTK_UNSIGNED_INT:
            *target = dtkImage(ext[1]+1, ext[3]+1, 1, PixelType::Scalar, reinterpret_cast<unsigned int *>(source->GetScalarPointer()));
            break;
        case VTK_LONG:
            *target = dtkImage(ext[1]+1, ext[3]+1, 1, PixelType::Scalar, reinterpret_cast<long *>(source->GetScalarPointer()));
            break;
        case VTK_UNSIGNED_LONG:
            *target = dtkImage(ext[1]+1, ext[3]+1, 1, PixelType::Scalar, reinterpret_cast<unsigned long *>(source->GetScalarPointer()));
            break;
        case VTK_FLOAT:
            *target = dtkImage(ext[1]+1, ext[3]+1, 1, PixelType::Scalar, reinterpret_cast<float *>(source->GetScalarPointer()));
            break;
        case VTK_DOUBLE:
            *target = dtkImage(ext[1]+1, ext[3]+1, 1, PixelType::Scalar, reinterpret_cast<double *>(source->GetScalarPointer()));
            break;
        case VTK_BIT:
        case VTK_VOID:
        default:
            dtkWarn() << Q_FUNC_INFO << "Pixel type " << vtk_pixel_type << "is not handled. Nothing is done";
            target = nullptr;
            break;
        }
    } else if (dim == 3) {

        switch (vtk_pixel_type) {
        case VTK_CHAR:
        case VTK_SIGNED_CHAR:
            *target = dtkImage(ext[1]+1, ext[3]+1, ext[5]+1, PixelType::Scalar, reinterpret_cast<char *>(source->GetScalarPointer()));
            break;

        case VTK_UNSIGNED_CHAR:
            *target = dtkImage(ext[1]+1, ext[3]+1, ext[5]+1, PixelType::Scalar, reinterpret_cast<unsigned char *>(source->GetScalarPointer()));
            break;

        case VTK_SHORT:
            *target = dtkImage(ext[1]+1, ext[3]+1, ext[5]+1, PixelType::Scalar, reinterpret_cast<short *>(source->GetScalarPointer()));
            break;

        case VTK_UNSIGNED_SHORT:
            *target = dtkImage(ext[1]+1, ext[3]+1, ext[5]+1, PixelType::Scalar, reinterpret_cast<unsigned short *>(source->GetScalarPointer()));
            break;

        case VTK_INT:
            *target = dtkImage(ext[1]+1, ext[3]+1, ext[5]+1, PixelType::Scalar, reinterpret_cast<int *>(source->GetScalarPointer()));
            break;

        case VTK_UNSIGNED_INT:
            *target = dtkImage(ext[1]+1, ext[3]+1, ext[5]+1, PixelType::Scalar, reinterpret_cast<unsigned int *>(source->GetScalarPointer()));
            break;

        case VTK_LONG:
            *target = dtkImage(ext[1]+1, ext[3]+1, ext[5]+1, PixelType::Scalar, reinterpret_cast<long *>(source->GetScalarPointer()));
            break;

        case VTK_UNSIGNED_LONG:
            *target = dtkImage(ext[1]+1, ext[3]+1, ext[5]+1, PixelType::Scalar, reinterpret_cast<unsigned long *>(source->GetScalarPointer()));
            break;

        case VTK_FLOAT:
            *target = dtkImage(ext[1]+1, ext[3]+1, ext[5]+1, PixelType::Scalar, reinterpret_cast<float *>(source->GetScalarPointer()));
            break;

        case VTK_DOUBLE:
            *target = dtkImage(ext[1]+1, ext[3]+1, ext[5]+1, PixelType::Scalar, reinterpret_cast<double *>(source->GetScalarPointer()));
            break;

        case VTK_BIT:
        case VTK_VOID:
        default:
            dtkWarn() << Q_FUNC_INFO << "Pixel type " << vtk_pixel_type << "is not handled. Nothing is done";
            target = nullptr;
            break;
        }

    } else {
        dtkWarn() << Q_FUNC_INFO << "Image of dimension " << dim << "is not handled. Nothing is done";
        target = nullptr;
    }

    if (target){
        target->setSpacing(source->GetSpacing());
        target->setOrigin(source->GetOrigin());
    }
}

void dtkVtkImageConverter::convertToNative(dtkImage *source, vtkImageData *target)
{

    int dim = source->dims();

    if (dim > 3) {
        dtkWarn() << Q_FUNC_INFO << "vtk only deal with 3D images. Nothing is done";
        return;
    }

    qlonglong xDim = source->xDim();
    qlonglong yDim = source->yDim();
    qlonglong zDim = source->zDim();
    const double *origin  = source->origin();
    const double *spacing = source->spacing();

    vtkSmartPointer<vtkImageImport> imageImport = vtkSmartPointer<vtkImageImport>::New();
    if (dim == 2) {
        imageImport->SetWholeExtent(0, xDim -1, 0, yDim-1, 0, 0);
        imageImport->SetDataOrigin(origin[0], origin[1], 0);
        imageImport->SetDataSpacing(spacing[0], spacing[1], 1);

    } else {
        imageImport->SetWholeExtent(0, xDim-1, 0, yDim-1, 0, zDim-1);
        imageImport->SetDataOrigin(origin[0], origin[1], origin[2]);
        imageImport->SetDataSpacing(spacing[0], spacing[1], spacing[2]);
    }
    imageImport->SetDataExtentToWholeExtent();

    int vtk_type = dtkVtkImageConverterPrivate::storageTypeToVtkType(source->storageType());
    imageImport->SetDataScalarType(vtk_type);

    imageImport->SetNumberOfScalarComponents(source->numberOfChannels());
    imageImport->SetImportVoidPointer(source->rawData(), 1);

    imageImport->Update();

    target->ShallowCopy(imageImport->GetOutput());
}

//
// dtkVtkImageConverter.cpp ends here




// inline dtkImage *dtkVtkImageConverterNoTemplate::createDtkImageData(int dim, int vtk_pixel_type, void *raw_data)
// {
//     dtkImage *img = nullptr;

//     if (dim == 2) {
//         switch (vtk_pixel_type) {
//         case VTK_CHAR:
//         case VTK_SIGNED_CHAR:
//             img = new  dtkImage();
//             img_data = new dtkImageDefaultStorage< dtkScalarPixel<char>, 2>(extent, reinterpret_cast<char *>(raw_data));
//             break;


//     return img_data;
// }
