#pragma once

#include <dtkCore>
#include <dtkAbstractCloseFilter.h>

class dtkVtCloseFilterPlugin: public dtkAbstractCloseFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractCloseFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtCloseFilterPlugin" FILE "dtkVtCloseFilterPlugin.json")

public:
     dtkVtCloseFilterPlugin(void) {}
    ~dtkVtCloseFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
