// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkImagingFilters/dtkAbstractMeshingFilter.h>

class dtkImage;
class dtkImageMesh;

// /////////////////////////////////////////////////////////////////
// dtkVtkMeshingFilter
// /////////////////////////////////////////////////////////////////

class dtkVtkMeshingFilter : public dtkAbstractMeshingFilter
{
public:
     dtkVtkMeshingFilter(void);
    ~dtkVtkMeshingFilter(void);

public:
    void setInput(dtkImage *);

public:
    void setDecimate(bool decimate);
    void setIsoValue(double isoValue);
    void setTargetReduction(double targetReduction);
    void setSmooth(bool smooth);
    void setIterations(int iterations);
    void setRelaxationFactor(double relaxationFactor);

public:
    dtkImageMesh *result(void) const;

public:
    void run(void);
    template < typename ImgT, int dim > inline void exec(void);

private:
    class dtkVtkMeshingFilterPrivate* d;
};

inline dtkAbstractMeshingFilter *dtkVtkMeshingFilterCreator(void)
{
    return new dtkVtkMeshingFilter();
}

//
// dtkVtkMeshingFilter.h ends here
