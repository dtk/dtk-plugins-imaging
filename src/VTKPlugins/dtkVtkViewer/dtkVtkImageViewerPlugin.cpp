// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtkImageViewer.h"
#include "dtkVtkImageViewerPlugin.h"

dtkAbstractImageViewer *dtkVtkImageViewerCreator(void);

// ///////////////////////////////////////////////////////////////////
// itkImageHandlerPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtkImageViewerPlugin::initialize(void)
{
    dtkImaging::viewer::pluginFactory().record("vtkImageViewer", dtkVtkImageViewerCreator);
}

void dtkVtkImageViewerPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtkImageViewer)


dtkAbstractImageViewer *dtkVtkImageViewerCreator(void)
{
    return new dtkVtkImageViewer();
}


//
// itkImageHandlerPlugin.cpp ends here
