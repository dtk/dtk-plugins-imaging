## Version: $Id$
## 
######################################################################
## 
### Commentary: 
## 
######################################################################
## 
### Change Log:
## 
######################################################################
## 
### Code:

project(itkImagePluginTest)

## ###################################################################
## Build tree setup
## ###################################################################

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

include_directories("../../src/ITKPlugins/dtkItkImageConverter/")

## ###################################################################
## Input
## ###################################################################

set(${PROJECT_NAME}_HEADERS
  itkImageTest.h)

set(${PROJECT_NAME}_SOURCES
  itkImageTest.cpp)

## ###################################################################
## Input - introspected
## ###################################################################

create_test_sourcelist(
    ${PROJECT_NAME}_SOURCES_TST
    ${PROJECT_NAME}.cpp
  ${${PROJECT_NAME}_SOURCES})

## ###################################################################
## Build rules
## ###################################################################

add_executable(${PROJECT_NAME}
  ${${PROJECT_NAME}_SOURCES_TST}
  ${${PROJECT_NAME}_SOURCES})

## ###################################################################
## Link rules
## ###################################################################

target_link_libraries(${PROJECT_NAME} Qt5::Core)
target_link_libraries(${PROJECT_NAME} Qt5::Test)

target_link_libraries(${PROJECT_NAME} dtkImaging)
target_link_libraries(${PROJECT_NAME} ${ITK_LIBRARIES})


## ###################################################################
## Test rules
## ###################################################################

add_test(itkImageTest ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${PROJECT_NAME} itkImageTest)


######################################################################
### CMakeLists.txt ends here
