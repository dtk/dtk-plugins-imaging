// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkCore>
#include <dtkAbstractImageMeshMapping.h>

class dtkVtkImageMeshMappingPlugin : public dtkAbstractImageMeshMappingPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractImageMeshMappingPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtkImageMeshMappingPlugin" FILE "dtkVtkImageMeshMappingPlugin.json")

public:
     dtkVtkImageMeshMappingPlugin(void) {}
    ~dtkVtkImageMeshMappingPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkImageMeshMappingPlugin.h ends here
