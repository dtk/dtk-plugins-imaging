// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkAbstractWatershedFilter.h>

class dtkVtWatershedFilterPlugin : public dtkAbstractWatershedFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractWatershedFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtWatershedFilterPlugin" FILE "dtkVtWatershedFilterPlugin.json")

public:
     dtkVtWatershedFilterPlugin(void) {}
    ~dtkVtWatershedFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkVtWatershedFilterPlugin.h ends here
