// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtImageReader.h"
#include "dtkVtImageReaderPlugin.h"

#include <dtkCore>

#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// itkImageHandlerPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtImageReaderPlugin::initialize(void)
{
    dtkImaging::reader::pluginFactory().record("dtkVtImageReader", dtkVtImageReaderCreator);
}

void dtkVtImageReaderPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtImageReader)

//
// itkImageHandlerPlugin.cpp ends here
