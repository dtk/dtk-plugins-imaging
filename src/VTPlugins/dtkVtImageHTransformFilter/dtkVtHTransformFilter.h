// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractHTransformFilter.h>

class dtkImage;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtHTransformFilter final : public dtkAbstractHTransformFilter
{
public:
     dtkVtHTransformFilter(void);
    ~dtkVtHTransformFilter(void);

public:
    void setImage(dtkImage *image);
    void setHValue(int h_value);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);

private:
    class dtkVtHTransformFilterPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkAbstractHTransformFilter *dtkVtHTransformFilterCreator(void)
{
    return new dtkVtHTransformFilter();
}

//
// dtkVtHTransformFilter.h ends here
