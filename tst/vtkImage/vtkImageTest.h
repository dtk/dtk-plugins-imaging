// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

class vtkImageTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testVtkReader(void);
    void testVtkConverter(void);

private slots:
    virtual void cleanupTestCase(void);
    virtual void cleanup(void);
};


//
// csrSparseMatrixHandlerTest.h ends here
