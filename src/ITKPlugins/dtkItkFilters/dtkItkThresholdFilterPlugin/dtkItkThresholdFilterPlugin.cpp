#include "dtkItkThresholdFilter.h"
#include "dtkItkThresholdFilterPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkThresholdFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkThresholdFilterPlugin::initialize(void)
{
    dtkImaging::filters::threshold::pluginFactory().record("itkThresholdImageFilter", dtkItkThresholdFilterCreator);
}

void dtkItkThresholdFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkThresholdFilter)

