// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkImagingWidgets/dtkAbstractImageViewer.h>

class dtkVtkImageViewerPlugin : public dtkAbstractImageViewerPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractImageViewerPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtkImageViewerPlugin" FILE "dtkVtkImageViewerPlugin.json")


public:
     dtkVtkImageViewerPlugin(void) {}
    ~dtkVtkImageViewerPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkImageViewerPlugin.h ends here
