// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtkMeshICP.h"
#include "dtkVtkMeshICPPlugin.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// vtkMeshICPPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtkMeshICPPlugin::initialize(void)
{
    dtkImaging::filters::meshICP::pluginFactory().record("vtkMeshICPFilter", dtkVtkMeshICPCreator);
}

void dtkVtkMeshICPPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtkMeshICP)

//
// dtkVtkMeshICPPlugin.cpp ends here
