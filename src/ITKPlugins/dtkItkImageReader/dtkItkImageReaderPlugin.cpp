// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkImageReader.h"
#include "dtkItkImageReaderPlugin.h"

#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkImageReaderPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkImageReaderPlugin::initialize(void)
{
    dtkImaging::reader::pluginFactory().record("dtkItkImageReader", dtkItkImageReaderCreator);
}

void dtkItkImageReaderPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkImageReader)

//
// dtkItkImageReaderPlugin.cpp ends here
