// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingFilters/dtkAbstractMeshingFilter.h>

class dtkVtkMeshingFilterPlugin : public dtkAbstractMeshingFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractMeshingFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkVtkMeshingFilterPlugin" FILE "dtkVtkMeshingFilterPlugin.json")

public:
     dtkVtkMeshingFilterPlugin(void) = default;
    ~dtkVtkMeshingFilterPlugin(void) = default;

public:
    void initialize(void);
    void uninitialize(void);
};

//
// dtkVtkMeshingFilterPlugin.h ends here
