// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtImageWriter.h"
#include "dtkVtImageWriterPlugin.h"

#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkVtImageWriterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtImageWriterPlugin::initialize(void)
{
    dtkImaging::writer::pluginFactory().record("dtkVtImageWriter", dtkVtImageWriterCreator);
}

void dtkVtImageWriterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta Writer
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtImageWriter)

//
// dtkVtImageWriterPlugin.cpp ends here
