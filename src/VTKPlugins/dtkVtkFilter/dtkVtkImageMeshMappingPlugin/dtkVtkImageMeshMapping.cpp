// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtkImageMeshMapping.h"

#include <dtkVtkImageMeshEngine.h>
#include <dtkVtkImageConverter.h>

#include <dtkImage.h>
#include <dtkImageMesh.h>
#include <dtkFilterExecutor.h>

#include <dtkLog>

#include <vtkImageData.h>
#include <vtkImageCast.h>
#include <vtkProbeFilter.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>

// /////////////////////////////////////////////////////////////////
// dtkVtkImageMeshMappingPrivate
// /////////////////////////////////////////////////////////////////

class dtkVtkImageMeshMappingPrivate
{
public:
    dtkImage *image;

    dtkImageMesh *mesh_in;
    dtkImageMesh *mesh_out;

public:
    template < typename ImgT, int dim > void exec(void);
};

// ///////////////////////////////////////////////////////////////////
// dtkVtkImageMeshMapping
// ///////////////////////////////////////////////////////////////////

dtkVtkImageMeshMapping::dtkVtkImageMeshMapping(void) : dtkAbstractImageMeshMapping(), d(new dtkVtkImageMeshMappingPrivate)
{
    d->image = nullptr;
    d->mesh_in = nullptr;

    d->mesh_out = new dtkImageMesh();
}

dtkVtkImageMeshMapping::~dtkVtkImageMeshMapping(void)
{
    delete d;
}

void dtkVtkImageMeshMapping::setImage(dtkImage *image)
{
    d->image = image;
}

void dtkVtkImageMeshMapping::setMesh(dtkImageMesh *mesh)
{
    d->mesh_in = mesh;
}

dtkImageMesh *dtkVtkImageMeshMapping::resMesh(void) const
{
    return d->mesh_out;
}

void dtkVtkImageMeshMapping::run(void)
{
    if (!d->image || !d->mesh_in) {
        dtkWarn() << Q_FUNC_INFO << "Missing inputs";
        return;
    }

    dtkFilterExecutor<dtkVtkImageMeshMappingPrivate>::run(d, d->image);
}

template < typename ImgT, int dim> inline void dtkVtkImageMeshMappingPrivate::exec(void)
{
    typedef dtkVtkImageConverterTemplate<ImgT, dim> Converter;
    vtkImageData *image = vtkImageData::New();

    Converter::convertToNative(this->image, image);

    dtkVtkImageMeshEngine *engine = dynamic_cast<dtkVtkImageMeshEngine *>(this->mesh_in->engine());

    if (image && engine) {

        vtkPolyData *polyData = engine->polyData();

        vtkImageCast *cast = vtkImageCast::New();
        cast->SetInputData(image);
        cast->SetOutputScalarTypeToFloat();
        //cast->Update();

        // Probe magnitude with iso-surface.
        vtkProbeFilter *probe = vtkProbeFilter::New();
        probe->SetInputData(polyData);
        probe->SetSourceConnection(cast->GetOutputPort());
        probe->SpatialMatchOn();
        probe->Update();
        vtkPolyData *resPolyData = probe->GetPolyDataOutput();

        dtkVtkImageMeshEngine *ve = new dtkVtkImageMeshEngine();
        ve->setPolyData(resPolyData);

        this->mesh_out = new dtkImageMesh(ve);
    }
}

//
// dtkVtkImageMeshMapping.cpp ends here
