#pragma once

#include <dtkCore>
#include <dtkAbstractXorFilter.h>

class dtkItkXorFilterPlugin: public dtkAbstractXorFilterPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkAbstractXorFilterPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkItkXorFilterPlugin" FILE "dtkItkXorFilterPlugin.json")

public:
     dtkItkXorFilterPlugin(void) {}
    ~dtkItkXorFilterPlugin(void) {}

public:
    void initialize(void);
    void uninitialize(void);
};
