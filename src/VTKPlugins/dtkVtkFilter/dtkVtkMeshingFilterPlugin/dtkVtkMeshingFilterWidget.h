// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtWidgets>

class dtkVtkMeshingFilterWidget : public QWidget
{
public:
     dtkVtkMeshingFilterWidget(QWidget *parent = 0);
    ~dtkVtkMeshingFilterWidget();

private:
    class dtkVtkMeshingFilterWidgetPrivate *d;
};

inline QWidget *dtkVtkMeshingFilterWidgetCreator(void)
{
    return new dtkVtkMeshingFilterWidget();
}

//
// dtkVtkMeshingFilterWidget.h ends here
