// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtOpenFilter.h"
#include "dtkVtImageConverter.h"

#include <dtkFilterExecutor.h>

#include <vt_image.h>

#include <api-morpho.h>

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtOpenFilterPrivate
{
public:
    dtkImage *image_in;
    dtkImage *image_out;

    double radius;

public:
    template < typename ImgT, int dim > void exec(void);
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkVtOpenFilter::dtkVtOpenFilter(void): dtkAbstractOpenFilter(), d(new dtkVtOpenFilterPrivate)
{
    d->image_in = nullptr;
    d->image_out = new dtkImage();

    d->radius = 0;
}

dtkVtOpenFilter::~dtkVtOpenFilter(void)
{
    delete d;
}

void dtkVtOpenFilter::setRadius(double radius)
{
    d->radius = radius;
}

void dtkVtOpenFilter::setImage(dtkImage *image)
{
    d->image_in = image;
}

dtkImage *dtkVtOpenFilter::filteredImage(void) const
{
    return d->image_out;
}

void dtkVtOpenFilter::run(void)
{
    if (!d->image_in) {
        dtkWarn() << Q_FUNC_INFO << "no image input";
        return;
    }

    dtkFilterExecutor<dtkVtOpenFilterPrivate>::run(d, d->image_in);
}

template < typename ImgT, int dim> inline void dtkVtOpenFilterPrivate::exec(void)
{
    vt_image image;
    char str[100];
    sprintf(str, "-operation opening -R %d", (int)this->radius);

    if (dtkVtImageConverter::convertToNative(this->image_in, &image) != EXIT_FAILURE) {
        dtkError() << Q_FUNC_INFO << "Conversion to VT format failed. Aborting.";

    } else {
        if ( API_morpho( &image, &image, nullptr, str, nullptr) != 1 ) {
            dtkError() << Q_FUNC_INFO << "Computation failed. Aborting.";

        } else {
            dtkVtImageConverter::convertFromNative(&image, this->image_out);
        }
        free( image.array );
        image.array = NULL;
    }
}

//
// dtkVtOpenFilter.cpp ends here
