// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkVtkMeshingFilter.h"
#include "dtkVtkMeshingFilterWidget.h"
#include "dtkVtkMeshingFilterPlugin.h"

// ///////////////////////////////////////////////////////////////////
// dtkVtkMeshingFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkVtkMeshingFilterPlugin::initialize(void)
{
    dtkImaging::filters::meshing::pluginFactory().record("dtkVtkMeshingFilter", dtkVtkMeshingFilterCreator);
    dtkImaging::filters::meshing::pluginFactory().record("dtkVtkMeshingFilter", dtkVtkMeshingFilterWidgetCreator);
}

void dtkVtkMeshingFilterPlugin::uninitialize(void)
{
}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkVtkMeshingFilter)

//
// dtkVtkMeshingFilterPlugin.cpp ends here
