// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkAbstractErosionFilter.h>

class dtkImage;
class dtkVtErosionFilterPrivate;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkVtErosionFilter final : public dtkAbstractErosionFilter
{
public:
     dtkVtErosionFilter(void);
    ~dtkVtErosionFilter(void);

public:
    void setImage(dtkImage *image);
    void setRadius(int radius);
    void setIterationCount(int iteration_count);

public:
    dtkImage *filteredImage(void) const;

public:
    void run(void);

private:
    dtkVtErosionFilterPrivate *d;
};

// ///////////////////////////////////////////////////////////////////

inline dtkAbstractErosionFilter *dtkVtErosionFilterCreator(void)
{
    return new dtkVtErosionFilter();
}

//
// dtkVtErosionFilter.h ends here
