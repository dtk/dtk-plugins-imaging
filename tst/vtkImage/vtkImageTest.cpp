// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "vtkImageTest.h"

#include <vtkImageData.h>
#include <vtkSmartPointer.h>


#include <dtkVtkImageConverter.h>
#include <dtkVtkImageReader.h>
#include <dtkVtkMetaImageWriter.h>

#include <dtkImagingCore>

#include <dtkPluginsImagingTest.h>

void vtkImageTestCase::initTestCase(void)
{

}

void vtkImageTestCase::init(void)
{

}


void vtkImageTestCase::testVtkReader(void)
{
    QString file_vti = QFINDTESTDATA("../resources/img_3d.vti");
    QString file     = QFINDTESTDATA("../resources/img_test.png");

    QVERIFY(!file.isEmpty());
    QVERIFY(!file_vti.isEmpty());

    dtkVtkImageReader reader;

    qDebug() << "read png";
    dtkImage  *img = reader.read(file);

    QVERIFY(img != nullptr);

    qDebug() << img->size();

    qDebug() << "read vti";
    img = reader.read(file_vti);

    qDebug() << img->size();

    QVERIFY(img != nullptr);

    QVERIFY(img->size() == 115 * 101 * 75);

    dtkVtkMetaImageWriter writer;
    writer.setPath("output.vti");
    writer.setImage(img);
    writer.write();
}

void vtkImageTestCase::testVtkConverter(void)
{
    qDebug()<<Q_FUNC_INFO<<__LINE__;
    int size=128*128*26;
    float *tab=new float[size];

    for(int i=0;i<size;i++)
        tab[i]=i;

    qDebug()<<Q_FUNC_INFO<<__LINE__<<tab;

    using PixelType = dtk::imaging::PixelType;

    dtkImage img(128, 128, 26, PixelType::Scalar, tab);

    //dtkVtkImageConverter converter;
    auto converter = dynamic_cast<dtkVtkImageConverter*>(dtkImaging::converter::pluginFactory().create("dtkVtkImageConverter"));

    vtkSmartPointer<vtkImageData> vtkImg=vtkSmartPointer<vtkImageData>::New();
    converter->convertToNative(&img, vtkImg);

    for(int i=0;i<26;i++) {
        for(int j=0;j<128*128;j++) {
            float* pixel=static_cast<float*>(vtkImg->GetScalarPointer(j%128,j/128,i));

            QVERIFY(pixel[0]==tab[j+128*128*i]);
        }
    }
}

void vtkImageTestCase::cleanupTestCase(void)
{

}

void vtkImageTestCase::cleanup(void)
{

}

DTKPLUGINSIMAGINGTEST_MAIN_NOGUI(vtkImageTest, vtkImageTestCase)

//
// vtkImageTest.cpp ends here
