// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkImagingFilters/dtkAbstractXorFilter.h>

class dtkItkXorFilterPrivate;

class dtkItkXorFilter final : public dtkAbstractXorFilter
{

public:
     dtkItkXorFilter(void);
    ~dtkItkXorFilter(void);

public:
    void setLhs(dtkImage*);
    void setRhs(dtkImage*);

public:
    dtkImage *result(void) const;

public:
    void run(void);

public:
    template <typename ImgT, int dim> inline void exec(void);

private:
    dtkItkXorFilterPrivate *d;

};

inline dtkAbstractXorFilter *dtkItkXorFilterCreator(void)
{
    return new dtkItkXorFilter();
}

//
// dtkItkXorFilter.h ends here
