// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkItkConnectedComponentsFilterPlugin.h"

#include "dtkItkConnectedComponentsFilter.h"

#include <dtkCore>
#include <dtkImaging.h>

// ///////////////////////////////////////////////////////////////////
// dtkItkConnectedComponentsFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkConnectedComponentsFilterPlugin::initialize(void)
{
    dtkImaging::filters::connectedComponents::pluginFactory().record("dtkItkConnectedComponentsFilter", dtkItkConnectedComponentsFilterCreator);
}

void dtkItkConnectedComponentsFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkConnectedComponentsFilter)

//
// dtkItkConnectedComponentsFilterPlugin.cpp ends here
