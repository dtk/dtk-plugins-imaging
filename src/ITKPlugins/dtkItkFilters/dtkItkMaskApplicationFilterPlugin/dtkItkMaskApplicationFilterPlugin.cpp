#include "dtkItkMaskApplicationFilter.h"
#include "dtkItkMaskApplicationFilterPlugin.h"

#include <dtkCore>
#include "dtkImaging.h"

// ///////////////////////////////////////////////////////////////////
// dtkItkMaskApplicationFilterPlugin
// ///////////////////////////////////////////////////////////////////

void dtkItkMaskApplicationFilterPlugin::initialize(void)
{
    dtkImaging::filters::maskApplication::pluginFactory().record("dtkItkMaskApplicationFilter", dtkItkMaskApplicationFilterCreator);
}

void dtkItkMaskApplicationFilterPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkItkMaskApplicationFilter)

